package project.spring.boot.shop.online.components.config;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class DBConfig {

    private static JdbcTemplate jdbcTemplate;
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static DBConfig INSTANCE;

    public static DBConfig getInstance() {
        if (INSTANCE == null) INSTANCE = new DBConfig();
        return INSTANCE;
    }

    public JdbcTemplate getConnection() throws Exception {
        if (jdbcTemplate == null)
            jdbcTemplate = new JdbcTemplate(PersistenceJPAConfig.getInstance().dataSource());
        //Dùng @Bean
        //jdbcTemplate = new JdbcTemplate(dataSource);

        return jdbcTemplate;
    }

    public NamedParameterJdbcTemplate getNamedConnection() throws Exception {
        if (namedParameterJdbcTemplate == null)
            namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(PersistenceJPAConfig.getInstance().dataSource());
        //Dùng @Bean
        //namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        return namedParameterJdbcTemplate;
    }
}
