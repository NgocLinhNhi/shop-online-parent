package project.spring.boot.shop.online.components.mapper;

import org.springframework.jdbc.core.RowMapper;
import project.spring.boot.shop.online.components.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();
        user.setSeqUser(rs.getLong("SEQ_USER"));
        user.setLoginName(rs.getString("LOGIN_NAME"));
        user.setPassword(rs.getString("PASSWORD"));
        user.setUserName(rs.getString("USER_NAME"));
        user.setPhone(rs.getString("PHONE"));
        user.setAddress(rs.getString("ADDRESS"));
        user.setIdentityCard(rs.getString("IDENTITY_CARD"));
        user.setCreateDate(rs.getDate("CREATE_DATE"));
        return user;
    }

}