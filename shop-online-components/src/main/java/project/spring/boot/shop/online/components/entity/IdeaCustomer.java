package project.spring.boot.shop.online.components.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IDEACUSTOMER")
@Getter
@Setter
public class IdeaCustomer implements Serializable {

    private static final long serialVersionUID = -593152082331964862L;

    @Id
    @Column(name = "SEQ_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int seqNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    @Fetch(FetchMode.SELECT)
    private User customer;

    @Column(name = "PRODUCT_ID")
    private Integer productId;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CONTENT_IDEA")
    private String contentIdea;

    @Column(name = "SYS_STATUS")
    private int sysStatus;

    public IdeaCustomer(int seqNo,
                        User customer,
                        Integer productId,
                        Date createDate,
                        String title,
                        String contentIdea,
                        int sysStatus) {
        super();
        this.seqNo = seqNo;
        this.customer = customer;
        this.productId = productId;
        this.createDate = createDate;
        this.title = title;
        this.contentIdea = contentIdea;
        this.sysStatus = sysStatus;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}