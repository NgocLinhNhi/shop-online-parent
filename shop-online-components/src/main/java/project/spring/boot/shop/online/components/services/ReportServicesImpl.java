package project.spring.boot.shop.online.components.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.spring.boot.shop.online.components.entity.Product;
import project.spring.boot.shop.online.components.mapper.ProductReportObject;
import project.spring.boot.shop.online.components.mapper.ReportMapper;
import project.spring.boot.shop.online.components.repository.ProductRepository;
import project.spring.boot.shop.online.components.view.ProductFileExcel;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReportServicesImpl implements ReportService {
    @Autowired
    ProductRepository productRepository;

    @Override
    public List<ProductReportObject> exportProductReport() throws Exception {
        return productRepository.exportProductReport();
    }

    @Override
    public void insertProductReportByBatch(List<ProductFileExcel> listProduct) throws Exception {
        List<Product> products = createProducts(listProduct);
        productRepository.insertBatchProduct(products);
    }

    private List<Product> createProducts(List<ProductFileExcel> listProduct) throws Exception {
        List<Product> listProducts = new ArrayList<>();

        long seqNo = getMaxSeqPro();
        Product product;

        for (ProductFileExcel pro : listProduct) {
            product = new Product();
            product.setSeqPro(seqNo);
            product.setCategoryId(pro.getCategory());
            product.setProductName(pro.getProductName());

            listProducts.add(product);
            seqNo++;
        }
        return listProducts;
    }

    private long getMaxSeqPro() throws Exception {
        long selectMaxSeqProduct = productRepository.selectMaxSeqProduct();
        return selectMaxSeqProduct + 1;
    }
}
