package project.spring.boot.shop.online.components.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import project.spring.boot.shop.online.components.entity.User;

import static project.spring.boot.shop.online.components.sql.UserSqlQuery.LOGIN_SPRING_JPA;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    //Để có thể sử  dụng Spring-jpa vơi config multi-module  cần config như dưới :
    //@EnableJpaRepositories("project.spring.boot.shop.online.components.repository")
    //@EntityScan("project.spring.boot.shop.online.components.entity")
    //=> Để nó có thể quét được mặc định entity cho lớp extends.. JpaRepository<User, Long>

    @Query(LOGIN_SPRING_JPA)
    User loginUser(String loginName, String password);

}
