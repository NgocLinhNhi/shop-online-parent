package project.spring.boot.shop.online.components.customer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.spring.boot.shop.online.utils.concurrent.*;
import project.spring.boot.shop.online.components.exception.MaxCapacityException;
import project.spring.boot.shop.online.components.request.CustomerRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.locks.Lock;

public class CustomerServiceClientLocal implements CustomerServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(CustomerServiceClientLocal.class);

    protected final int maxCapacity;
    protected volatile boolean active;
    private ExecutorService executorService;
    private final int threadPoolSize;
    private final NFutureMap futureMap;
    private final NMapLockProvider lockProvider;
    private final CustomerRequestTicketQueue ticketQueue;
    private final Map<String, CustomerRequestHandler> requestHandlers;

    //Debug thi set cao lên tí => bt để tầm 20s thôi = 20000
    private final int DEFAULT_TIMEOUT = 150000;

    public CustomerServiceClientLocal(int maxCapacity) {
        this.threadPoolSize = 16;
        this.maxCapacity = maxCapacity;
        this.requestHandlers = new HashMap<>();
        this.futureMap = new NFutureConcurrentHashMap();
        this.ticketQueue = new CustomerRequestTicketQueue();
        this.lockProvider = new NHashMapLockProxyProvider();
    }

    public void start() throws Exception {
        this.executorService = newExecutorService();
        for (int i = 0; i < threadPoolSize; ++i) {
            executorService.submit(this::loop);
        }
    }

    private ExecutorService newExecutorService() {
        ThreadFactory threadFactory = new NThreadFactory("customer-client");
        ExecutorService instance = Executors.newFixedThreadPool(threadPoolSize, threadFactory);
        Runtime.getRuntime().addShutdownHook(new Thread(instance::shutdown));
        return instance;
    }

    public void stop() {
        this.active = false;
        this.ticketQueue.clear();
        if (executorService != null) {
            this.executorService.shutdown();
        }
        this.executorService = null;
    }

    protected void loop() {
        this.active = true;
        while (active) {
            try {
                CustomerRequest request = ticketQueue.take();
                handleRequest(request);
            } catch (InterruptedException e) {
                logger.error("customer server client has interrupted" + e);
                break;
            } catch (Exception e) {
                logger.error("customer server client error", e);
            }
        }
    }

    public void addRequestHandler(String requestType, CustomerRequestHandler handler) {
        this.requestHandlers.put(requestType, handler);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T call(CustomerRequest request, Class<T> outType) throws Exception {
        NFuture future = futureMap.addFuture(request);
        boolean accepted = ticketQueue.add(request);
        if (accepted) {
            try {
                logger.debug("start process request of: "
                        + request.getLoginName()
                        + ", queue.size: " + ticketQueue.size()
                        + ", futures.size: " + futureMap.size()
                        + ", locks.size: " + lockProvider.size());
                return future.get(DEFAULT_TIMEOUT);
            } finally {
                logger.debug("finish process request of: "
                        + request.getLoginName()
                        + ", queue.size: " + ticketQueue.size()
                        + ", futures.size: " + futureMap.size()
                        + ", locks.size: " + lockProvider.size());
            }
        }
        futureMap.removeFuture(request);
        throw new MaxCapacityException("ticket queue got max capacity");
    }

    @SuppressWarnings("unchecked")
    private void handleRequest(CustomerRequest request) {
        NFuture future = futureMap.removeFuture(request);
        Object result = null;
        Exception exception = null;
        try {
            result = processRequest(request);
        } catch (Exception e) {
            exception = e;
        }
        if (future == null) {
            logger.info("has no future map to request: " + request + "it may timeout");
        } else {
            if (result != null)
                future.setResult(result);
            else
                future.setException(exception);
        }
    }

    @SuppressWarnings("unchecked")
    private Object processRequest(CustomerRequest request) throws Exception {
        String requestType = request.getRequestType();
        CustomerRequestHandler handler = requestHandlers.get(requestType);
        if (handler == null)
            throw new IllegalArgumentException("has no handler for request type: " + requestType);
        String customerId = request.getLoginName();
        Lock lock = lockProvider.provideLock(customerId);
        lock.lock();
        try {
            return handler.handle(request);
        } finally {
            lock.unlock();
            lockProvider.removeLock(customerId);
        }
    }

}
