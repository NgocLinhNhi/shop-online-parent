package project.spring.boot.shop.online.components.repository;

import project.spring.boot.shop.online.components.entity.User;

public interface CustomerRepository {

    User loginUser(User user);

    User getUser(String loginName) throws Exception;

    Integer selectMaxSeq() throws Exception;

    void insertUser(User user) throws Exception;
}
