package project.spring.boot.shop.online.components.exception;

public class MaxCapacityException extends RuntimeException {

    public MaxCapacityException(String msg) {
        super(msg);
    }

}
