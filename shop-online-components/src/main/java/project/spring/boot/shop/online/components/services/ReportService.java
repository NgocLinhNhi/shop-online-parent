package project.spring.boot.shop.online.components.services;


import project.spring.boot.shop.online.components.mapper.ProductReportObject;
import project.spring.boot.shop.online.components.mapper.ReportMapper;
import project.spring.boot.shop.online.components.view.ProductFileExcel;

import java.util.List;

public interface ReportService {
    List<ProductReportObject> exportProductReport() throws Exception;

    void insertProductReportByBatch(List<ProductFileExcel> listProduct) throws Exception;
}
