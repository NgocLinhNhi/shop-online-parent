package project.spring.boot.shop.online.components.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "DIARYADMIN")
@Getter
@Setter
public class Diary implements Serializable {

    private static final long serialVersionUID = -731200869916708312L;

    @Id
    @Column(name = "SEQ_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int seqNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ADMIN_ID", nullable = false)
    @Fetch(FetchMode.SELECT)
    private Admin admin;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Column(name = "CONTENT_DIARY")
    private String contentDiary;

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Diary(int seqNo, Admin admin, Date createDate, String contentDiary) {
        super();
        this.seqNo = seqNo;
        this.admin = admin;
        this.createDate = createDate;
        this.contentDiary = contentDiary;
    }

    public Diary() {
        super();
    }

}
