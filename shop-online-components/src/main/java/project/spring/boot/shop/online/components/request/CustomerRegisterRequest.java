package project.spring.boot.shop.online.components.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerRegisterRequest implements CustomerRequest {

    protected String loginName;
    private String password;
    private String email;
    private String phone;
    private String address;
    private int gender;
    private String userName;
    private String identityCard;

    @Override
    public String getRequestType() {
        return CustomerRequestTypes.REGISTER;
    }
}
