package project.spring.boot.shop.online.components.request;

public interface CustomerRequest {

    String getLoginName();

    String getRequestType();

}
