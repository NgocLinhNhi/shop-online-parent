package project.spring.boot.shop.online.components.services;

import project.spring.boot.shop.online.components.entity.Category;

import java.util.List;

public interface CategoryServices {
    List<Category> loadAllCategory() throws Exception;
}
