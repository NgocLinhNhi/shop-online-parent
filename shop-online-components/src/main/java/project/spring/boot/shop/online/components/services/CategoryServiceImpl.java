package project.spring.boot.shop.online.components.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.spring.boot.shop.online.components.entity.Category;
import project.spring.boot.shop.online.components.repository.CategoryRepository;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryServices {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> loadAllCategory() throws Exception {
        return categoryRepository.loadAllCategory();
    }
}
