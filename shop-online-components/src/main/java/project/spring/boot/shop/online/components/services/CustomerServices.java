package project.spring.boot.shop.online.components.services;

import project.spring.boot.shop.online.components.entity.User;

public interface CustomerServices {

    User login(User user);

    void registerMember(User us) throws Exception;

    long selectMaxSeq() throws Exception;

    User checkExistLoginName(String loginName) throws Exception;

}
