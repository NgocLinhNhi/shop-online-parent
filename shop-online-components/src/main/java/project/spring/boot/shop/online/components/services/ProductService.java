package project.spring.boot.shop.online.components.services;

import project.spring.boot.shop.online.components.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> loadAllProduct() throws Exception;

    Product findProductById(Long seqPro) throws Exception;

    void addNewProduct(Product pro) throws Exception;

    void deleteProduct(Long seqPro) throws Exception;

    void updateProduct(Product pro) throws Exception;

}
