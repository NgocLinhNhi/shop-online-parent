package project.spring.boot.shop.online.components.exception;

public class CustomerDuplicateException extends IllegalArgumentException {

    public CustomerDuplicateException(String message) {
        super(message);
    }

}
