package project.spring.boot.shop.online.components.mapper;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class ProductReportObject implements Serializable {
    private Long seqPro;
    private Long categoryId;
    private String categoryName;
    private String productName;
    private BigDecimal price;
    private BigDecimal numberSales;
    private BigDecimal guarantee;
    private String imageProduct;
    private String createDate;
    private String updateDate;
    private Long sysStatus;

}