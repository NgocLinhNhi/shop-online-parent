package project.spring.boot.shop.online.components.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.spring.boot.shop.online.components.entity.User;
import project.spring.boot.shop.online.components.repository.CustomerRepository;
import project.spring.boot.shop.online.components.repository.UserRepository;

@Service
public class CustomerServiceImpl implements CustomerServices {

    private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public User login(User user) {
        //Login với SpringJpa
        logger.info("Login Process with Spring-JPA");
        return userRepository.loginUser(user.getLoginName(), user.getPassword());
        //Login = JdbcTemplate
        //return customerRepository.loginUser(user);
    }

    @Override
    public void registerMember(User user) throws Exception {
        //Register = SpringJpa
        logger.info("Register Process with Spring-JPA");
        userRepository.save(user);
        //Register = JdbcTemplate
        //  customerRepository.insertUser(user);
    }

    @Override
    public long selectMaxSeq() throws Exception {
        return customerRepository.selectMaxSeq();
    }

    @Override
    public User checkExistLoginName(String loginName) throws Exception {
        return customerRepository.getUser(loginName);
    }

}