package project.spring.boot.shop.online.components.mapper;

import org.springframework.jdbc.core.RowMapper;
import project.spring.boot.shop.online.utils.util.DateUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

import static project.spring.boot.shop.online.utils.util.DateConstant.PATTERN_YYYMMDD;

public class ReportMapper implements RowMapper<ProductReportObject> {

    @Override
    public ProductReportObject mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductReportObject pro = new ProductReportObject();
        pro.setSeqPro(rs.getLong("SEQ_PRO"));
        pro.setCategoryName(rs.getString("CATEGORY_NAME"));
        pro.setProductName(rs.getString("PRODUCT_NAME"));
        pro.setPrice(rs.getBigDecimal("PRICE"));
        pro.setImageProduct(rs.getString("IMAGE_URL"));
        pro.setNumberSales(rs.getBigDecimal("NUMBER_SALES"));
        pro.setSysStatus(rs.getLong("SYS_STATUS"));
        pro.setGuarantee(rs.getBigDecimal("GUARANTEE"));
        pro.setUpdateDate(DateUtil.toString(rs.getDate("UPDATE_DATE"), PATTERN_YYYMMDD));
        pro.setCreateDate(DateUtil.toString(rs.getDate("CREATE_DATE"), PATTERN_YYYMMDD));
        return pro;
    }

}