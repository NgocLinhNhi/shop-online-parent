package project.spring.boot.shop.online.components.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import project.spring.boot.shop.online.utils.io.InputStreams;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@Component
public class PersistenceJPAConfig {
    private static PersistenceJPAConfig INSTANCE;

    public static PersistenceJPAConfig getInstance() {
        if (INSTANCE == null) INSTANCE = new PersistenceJPAConfig();
        return INSTANCE;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws Exception {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("project.spring.boot.shop.online.components.entity");

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    @Bean
    public DataSource dataSource() throws Exception {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(getInstance().loadProperties().getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(getInstance().loadProperties().getProperty("spring.datasource.url"));
        dataSource.setUsername(getInstance().loadProperties().getProperty("spring.datasource.username"));
        dataSource.setPassword(getInstance().loadProperties().getProperty("spring.datasource.password"));
        return dataSource;
    }

    private Properties loadProperties() throws Exception {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(getPropertiesFile()));
        properties.put("spring.datasource.driver-class-name", properties.get("spring.datasource.driver-class-name"));
        properties.put("spring.datasource.url", properties.get("spring.datasource.url"));
        properties.put("spring.datasource.username", properties.get("spring.datasource.username"));
        properties.put("spring.datasource.password", properties.get("spring.datasource.password"));
        return properties;
    }

    private String getPropertiesFile() {
        String file = System.getProperty("jms.properties.file");
        if (file == null)
            file = "application.properties";
        return file;
    }
    //load File Properties

    @Bean
    public PlatformTransactionManager transactionManager() throws Exception {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

        return properties;
    }
}
