package project.spring.boot.shop.online.components.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import project.spring.boot.shop.online.components.config.DBConfig;
import project.spring.boot.shop.online.components.config.PersistenceJPAConfig;
import project.spring.boot.shop.online.components.entity.User;
import project.spring.boot.shop.online.components.mapper.UserMapper;
import project.spring.boot.shop.online.components.sql.UserSqlQuery;
import project.spring.boot.shop.online.utils.util.DateUtil;

import javax.sql.DataSource;

import static project.spring.boot.shop.online.components.sql.UserSqlQuery.*;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private static final Logger logger = LoggerFactory.getLogger(CustomerRepositoryImpl.class);

    //Đê trong service thì 2 thằng  nay mới nhận được
    @Autowired
    DataSource dataSource;
    @Autowired
    PersistenceJPAConfig persistenceJPAConfig;

    @SuppressWarnings("unchecked")
    @Override
    public User loginUser(User user) {
        logger.info("START LOGIN PROCESS");
        try {
            DataSource dataSource = persistenceJPAConfig.dataSource();
            NamedParameterJdbcTemplate namedConnection = DBConfig.getInstance().getNamedConnection();
            return (User) namedConnection.queryForObject(
                    LOGIN,
                    new MapSqlParameterSource()
                            .addValue("loginName", user.getLoginName())
                            .addValue("password", user.getPassword()),
                    new UserMapper());
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public User getUser(String loginName) throws Exception {
        logger.info("START CHECK EXIST CUSTOMER");
        try {
            NamedParameterJdbcTemplate namedConnection = DBConfig.getInstance().getNamedConnection();
            return (User) namedConnection.queryForObject(
                    CHECK_EXIST_USER_NAME,
                    new MapSqlParameterSource().addValue("loginName", loginName),
                    new UserMapper());
        } catch (DataAccessException e) {
            return null;
        }
    }

    @Override
    public Integer selectMaxSeq() throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.queryForObject(SELECT_MAX_SEQ, Integer.class);
    }

    @Override
    public void insertUser(User user) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        jdbcTemplate.update(
                UserSqlQuery.insertNewUser().toString(),
                user.getSeqUser(),
                user.getLoginName(),
                user.getPassword(),
                user.getEmail(),
                user.getPhone(),
                user.getAddress(),
                user.getGender(),
                user.getUserName(),
                user.getRoleUser(),
                user.getIsDelete(),
                user.getIdentityCard(),
                DateUtil.convertDateUtilToDateSQL(user.getCreateDate()),
                null
        );
    }

}
