package project.spring.boot.shop.online.components.customer;

import project.spring.boot.shop.online.components.request.CustomerRequest;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class CustomerRequestTicketQueue {

    protected final BlockingQueue<CustomerRequest> queue;

    public CustomerRequestTicketQueue() {
        //Số request đáp ứng của hệ thống thường là 5k thôi
        this(10000);
    }

    public CustomerRequestTicketQueue(int capacity) {
        this.queue = new LinkedBlockingQueue<>(capacity);
    }

    public boolean add(CustomerRequest request) {
        return queue.offer(request);
    }

    public CustomerRequest take() throws Exception {
        return queue.take();
    }

    public void clear() {
        queue.clear();
    }

    public int size() {
        return queue.size();
    }

}
