package project.spring.boot.shop.online.components.sql;

public class ProductSqlQuery {
    public static final String FIND_PRODUCT_BY_ID = "Select * From Product where SEQ_PRO =:seqPro";
    public static final String MAX_SEQ_PRODUCT = "select max(SEQ_PRO) from PRODUCT";
    public static final String UPDATE_PRODUCT = "update Product pro set pro.SYS_STATUS = 0 where pro.SEQ_PRO =:seqPro";
    public static final String DELETE_PRODUCT = "Delete from PRODUCT where SEQ_PRO =:seqPro";

    public static StringBuilder loadAllProductQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT pro.* ,                                  ")
                .append("    (SELECT                                ")
                .append("       CATEGORY_NAME                       ")
                .append("     FROM  CATEGORY cate                   ")
                .append("     WHERE                                 ")
                .append("      pro.CATEGORY_ID = cate.CATEGORY_ID)  ")
                .append("  as CATEGORY_NAME                         ")
                .append("    From Product pro                       ");
        return sql;
    }

    public static StringBuilder findProductBySeqQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT pro.* ,                                  ")
                .append("    (SELECT                                ")
                .append("       CATEGORY_NAME                       ")
                .append("     FROM  CATEGORY cate                   ")
                .append("     WHERE                                 ")
                .append("      pro.CATEGORY_ID = cate.CATEGORY_ID)  ")
                .append("  as CATEGORY_NAME                         ")
                .append("    From Product pro                       ")
                .append("  Where                                    ")
                .append("    pro.SEQ_PRO =:seqPro                   ");
        return sql;
    }

    public static StringBuilder insertProductQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO PRODUCT                       ")
                .append("    (SEQ_PRO,                        ")
                .append("    PRODUCT_NAME,                    ")
                .append("    CATEGORY_ID,                     ")
                .append("    GUARANTEE,                       ")
                .append("    NUMBER_SALES,                    ")
                .append("    PRICE,                           ")
                .append("    IMAGE_URL,                       ")
                .append("    IS_USING,                        ")
                .append("    SYS_STATUS,                      ")
                .append("    CREATE_DATE,                     ")
                .append("    UPDATE_DATE)                     ")
                .append("    VALUES(?,?,?,?,?,?,?,?,?,?,?)    ");
        return sql;
    }

    public static StringBuilder updateProductQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append("Update  PRODUCT  SET                ")
                .append("    PRODUCT_NAME =? ,          ")
                .append("    CATEGORY_ID =? ,           ")
                .append("    GUARANTEE =? ,             ")
                .append("    NUMBER_SALES =? ,          ")
                .append("    PRICE =? ,                 ")
                .append("    IMAGE_URL =? ,             ")
                .append("    IS_USING =? ,              ")
                .append("    SYS_STATUS =? ,            ")
                .append("    CREATE_DATE =?,            ")
                .append("    UPDATE_DATE =?             ")
                .append("    WHERE                      ")
                .append("       SEQ_PRO = ?             ");
        return sql;
    }


    public static StringBuilder insertProductByExcelQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO PRODUCT                  ")
                .append("    (SEQ_PRO,                   ")
                .append("    PRODUCT_NAME,               ")
                .append("    CATEGORY_ID)                ")
                .append("    VALUES(?,?,?)                 ");
        return sql;
    }
}
