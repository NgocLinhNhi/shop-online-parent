package project.spring.boot.shop.online.components.sql;

public class UserSqlQuery {
    public static final String LOGIN_SPRING_JPA = "select us from User us where us.loginName = ?1 and us.password = ?2 and isDelete=1";
    public static final String LOGIN = "select * from User_CUSTOMER us where us.login_Name =:loginName and us.password =:password and is_Delete=1";
    public static final String SELECT_MAX_SEQ = "select max(SEQ_USER) from USER_CUSTOMER";
    public static final String CHECK_EXIST_USER_NAME = "select * from User_CUSTOMER where login_Name=:loginName and is_Delete=1";

    public static StringBuilder checkExistLoginName(String loginName) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM User_CUSTOMER              ")
                .append("    where                           ")
                .append("    login_Name = '")
                .append(loginName)
                .append("' ")
                .append("    and is_Delete=1                 ");
        return sql;
    }


    public static StringBuilder insertNewUser() {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO User_CUSTOMER                   ")
                .append("    (SEQ_USER,                         ")
                .append("    LOGIN_NAME,                        ")
                .append("    PASSWORD,                          ")
                .append("    EMAIL,                             ")
                .append("    PHONE,                             ")
                .append("    ADDRESS,                           ")
                .append("    GENDER,                            ")
                .append("    USER_NAME,                         ")
                .append("    ROLE_USER,                         ")
                .append("    IS_DELETE,                         ")
                .append("    IDENTITY_CARD,                     ")
                .append("    CREATE_DATE,                       ")
                .append("    LINK_BILL)                         ")
                .append("    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)  ");
        return sql;
    }

}
