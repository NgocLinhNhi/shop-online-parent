package project.spring.boot.shop.online.components.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "ADMIN")
@Getter
@Setter
public class Admin implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SEQ_ADMIN", nullable = false)
    private Long seqUser;

    @Column(name = "LOGIN_ADMIN", nullable = false)
    private String loginName;

    @Column(name = "PASSWORD_ADMIN", nullable = false)
    private String password;

    @Column(name = "EMAIL_ADMIN")
    private String email;

    @Column(name = "ADDRESS_ADMIN")
    private String address;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_OF_BIRTH")
    private Date dateOfBirth;

    @Column(name = "GENDER")
    private int gender;

    @Column(name = "SYS_STATUS")
    private int sysStatus;

    @OneToMany(mappedBy = "admin", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    private Set<Diary> diaryAdmin;

    public Admin(Long seqUser,
                 String loginName,
                 String password,
                 String email,
                 String address,
                 Date dateOfBirth,
                 int gender,
                 int sysStatus,
                 Set<Diary> diaryAdmin) {
        super();
        this.seqUser = seqUser;
        this.loginName = loginName;
        this.password = password;
        this.email = email;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.sysStatus = sysStatus;
        this.diaryAdmin = diaryAdmin;
    }

    public Admin() {
        super();
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
