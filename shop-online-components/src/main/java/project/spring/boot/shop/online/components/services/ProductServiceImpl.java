package project.spring.boot.shop.online.components.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.spring.boot.shop.online.components.entity.Product;
import project.spring.boot.shop.online.components.exception.ProductNotFoundException;
import project.spring.boot.shop.online.components.mapper.ReportMapper;
import project.spring.boot.shop.online.components.repository.ProductRepository;

import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> loadAllProduct() throws Exception {
        return productRepository.loadAllProduct();
    }

    @Override
    public Product findProductById(Long seqPro) throws Exception {
        Product result = productRepository.findProductById(seqPro);
        if (result == null) {
            throw new ProductNotFoundException("Not Found Product");
        }
        return result;
    }

    @Override
    public void addNewProduct(Product pro) throws Exception {
        createProduct(pro);
        productRepository.insertProduct(pro);
    }

    private void createProduct(Product pro) throws Exception {
        long selectMaxSeqProduct = productRepository.selectMaxSeqProduct();
        long seqNo = selectMaxSeqProduct + 1;

        pro.setSeqPro(seqNo);
        pro.setSysStatus(new Long("1"));
        pro.setIsUsing("N");

        Date date = new Date();
        pro.setCreateDate(date);
        pro.setUpdateDate(date);
    }

    @Override
    public void deleteProduct(Long seqPro) throws Exception {
        productRepository.deleteProduct(seqPro);
    }

    @Override
    public void updateProduct(Product pro) throws Exception {
        productRepository.updateProduct(pro);
    }

}
