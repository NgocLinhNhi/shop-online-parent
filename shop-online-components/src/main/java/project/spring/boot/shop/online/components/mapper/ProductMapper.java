package project.spring.boot.shop.online.components.mapper;

import org.springframework.jdbc.core.RowMapper;
import project.spring.boot.shop.online.components.entity.Product;

import java.sql.ResultSet;
import java.sql.SQLException;

//RowMapper<Product> => không kế thừa kiểu này die khi lấy 1 single result
public class ProductMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product pro = new Product();
        pro.setSeqPro(rs.getLong("SEQ_PRO"));
        pro.setCategoryId(rs.getLong("CATEGORY_ID"));
        pro.setCategoryName(rs.getString("CATEGORY_NAME"));
        pro.setProductName(rs.getString("PRODUCT_NAME"));
        pro.setPrice(rs.getBigDecimal("PRICE"));
        pro.setNumberSales(rs.getBigDecimal("NUMBER_SALES"));
        pro.setImageProduct(rs.getString("IMAGE_URL"));
        pro.setSysStatus(rs.getLong("SYS_STATUS"));
        pro.setGuarantee(rs.getBigDecimal("GUARANTEE"));
        pro.setIsUsing(rs.getString("IS_USING"));
        pro.setUpdateDate(rs.getDate("UPDATE_DATE"));
        pro.setCreateDate(rs.getDate("CREATE_DATE"));
        return pro;
    }
}
