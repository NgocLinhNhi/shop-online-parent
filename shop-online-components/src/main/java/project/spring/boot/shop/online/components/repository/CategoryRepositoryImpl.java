package project.spring.boot.shop.online.components.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import project.spring.boot.shop.online.components.config.DBConfig;
import project.spring.boot.shop.online.components.entity.Category;
import project.spring.boot.shop.online.components.mapper.CategoryMapper;

import java.util.List;

import static project.spring.boot.shop.online.components.sql.CategorySqlQuery.GET_ALL_CATEGORY;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    @SuppressWarnings("unchecked")
    @Override
    public List<Category> loadAllCategory() throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(GET_ALL_CATEGORY, new CategoryMapper());
    }
}
