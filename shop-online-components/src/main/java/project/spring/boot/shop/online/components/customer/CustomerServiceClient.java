package project.spring.boot.shop.online.components.customer;

import project.spring.boot.shop.online.components.request.CustomerRequest;

public interface CustomerServiceClient {

    <T> T call(CustomerRequest request, Class<T> outType) throws Exception;

}
