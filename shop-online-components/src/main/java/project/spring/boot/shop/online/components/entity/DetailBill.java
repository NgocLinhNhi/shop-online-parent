package project.spring.boot.shop.online.components.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "DETAILBILL")
@Getter
@Setter
public class DetailBill implements Serializable {

    private static final long serialVersionUID = 3952535318569442430L;

    @EmbeddedId
    private DetailBillPK seq_no;

    @ManyToOne(fetch = FetchType.LAZY)
    // ko set InsertAble va updateAble = false thì chết ngay lỗi này :
	/*
		Initial SessionFactory creation failed.org.hibernate.MappingException: Repeated column in mapping
	 	for entity: com.jp.salesonline.entity.DetailBill column: BILL_ID
	 */
    @JoinColumn(name = "BILL_ID", nullable = false, insertable = false, updatable = false)
    @Fetch(FetchMode.SELECT)
    private Bill bill;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID", nullable = false, insertable = false, updatable = false)
    @Fetch(FetchMode.SELECT)
    private Product product;

    @Column(name = "NUMBER_BILL")
    private Integer numberBill;

    @Column(name = "SINGLE_PRICE")
    private BigDecimal singlePrice;

    @Column(name = "SYS_STATUS_DETAILBILL")
    private int sysStatusDetailBill;

    public DetailBill(DetailBillPK seq_no,
                      Bill bill,
                      Product product,
                      Integer numberBill,
                      BigDecimal singlePrice,
                      int sysStatusDetailBill) {
        super();
        this.seq_no = seq_no;
        this.bill = bill;
        this.product = product;
        this.numberBill = numberBill;
        this.singlePrice = singlePrice;
        this.sysStatusDetailBill = sysStatusDetailBill;
    }

    public DetailBill() {
        super();
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
