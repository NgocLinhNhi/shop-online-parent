package project.spring.boot.shop.online.components.customer;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface CustomerRequestAnnotation {

    //Tạo 1 annotation @
    String value();

}
