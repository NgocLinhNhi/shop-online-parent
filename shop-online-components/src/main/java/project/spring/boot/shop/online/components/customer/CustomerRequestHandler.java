package project.spring.boot.shop.online.components.customer;

import project.spring.boot.shop.online.components.request.CustomerRequest;

public interface CustomerRequestHandler<R extends CustomerRequest> {

    Object handle(R request) throws Exception;

}
