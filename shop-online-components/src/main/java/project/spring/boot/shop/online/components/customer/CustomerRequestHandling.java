package project.spring.boot.shop.online.components.customer;

public final class CustomerRequestHandling {

    private CustomerRequestHandling() {
    }

    public static String getRequestType(CustomerRequestHandler handler) {
        CustomerRequestAnnotation customerRequestAnnotation = handler.getClass().getAnnotation(CustomerRequestAnnotation.class);
        return customerRequestAnnotation.value();
    }

}
