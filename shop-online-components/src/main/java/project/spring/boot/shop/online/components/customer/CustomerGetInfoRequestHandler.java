package project.spring.boot.shop.online.components.customer;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.spring.boot.shop.online.components.entity.User;
import project.spring.boot.shop.online.components.exception.CustomerNotFoundException;
import project.spring.boot.shop.online.components.request.CustomerLoginRequest;
import project.spring.boot.shop.online.components.request.CustomerRequestTypes;
import project.spring.boot.shop.online.components.services.CustomerServices;

@Setter
@Component
@CustomerRequestAnnotation(CustomerRequestTypes.GET)
public class CustomerGetInfoRequestHandler extends CustomerRequestAbstractHandler<CustomerLoginRequest> {

    @Autowired
    CustomerServices customerService;

    @Override
    public Object handle(CustomerLoginRequest request) {
        User checkLogin = customerService.login(createUser(request));
        if (checkLogin == null)
            throw new CustomerNotFoundException("User Login Name: " + request.getLoginName() + " not existed");

        return checkLogin;
    }

    private User createUser(CustomerLoginRequest request) {
        User user = new User();
        user.setLoginName(request.getLoginName());
        user.setPassword(request.getPassword());
        return user;
    }
}
