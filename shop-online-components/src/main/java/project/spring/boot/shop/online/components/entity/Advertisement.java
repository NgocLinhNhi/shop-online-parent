package project.spring.boot.shop.online.components.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ADVERTISEMENT")
@Getter
@Setter
public class Advertisement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SEQ_ADV", nullable = false)
    private Long seqAdv;

    @Column(name = "ADMIN_ID", nullable = false)
    private Long adminId;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "CONTENT_ADV")
    private String contentAdv;

    @Column(name = "VIDEO_URL")
    private String videoUrl;

    @Column(name = "IMAGE_URL")
    private String imageUrl;

    @Temporal(TemporalType.DATE)
    @Column(name = "START_DATE")
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "SYS_STATUS")
    private int sysStatus;

    public Advertisement(Long seqAdv,
                         Long adminId,
                         String title,
                         String contentAdv,
                         String videoUrl,
                         String imageUrl,
                         Date startDate,
                         Date endDate,
                         int sysStatus) {
        super();
        this.seqAdv = seqAdv;
        this.adminId = adminId;
        this.title = title;
        this.contentAdv = contentAdv;
        this.videoUrl = videoUrl;
        this.imageUrl = imageUrl;
        this.startDate = startDate;
        this.endDate = endDate;
        this.sysStatus = sysStatus;
    }

    public Advertisement() {
        super();
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
