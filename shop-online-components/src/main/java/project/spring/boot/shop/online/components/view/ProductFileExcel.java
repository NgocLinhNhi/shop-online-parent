package project.spring.boot.shop.online.components.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductFileExcel {
    private Long category;
    private String productName;
    private String message;

    public ProductFileExcel(Long category, String productName) {
        super();
        this.category = category;
        this.productName = productName;
    }

    public ProductFileExcel(Long category, String productName, String message) {
        super();
        this.category = category;
        this.productName = productName;
        this.message = message;
    }

    public ProductFileExcel() {
        super();
    }

}
