package project.spring.boot.shop.online.components.customer;

import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.spring.boot.shop.online.components.entity.User;
import project.spring.boot.shop.online.components.exception.CustomerDuplicateException;
import project.spring.boot.shop.online.components.request.CustomerRegisterRequest;
import project.spring.boot.shop.online.components.request.CustomerRequestTypes;
import project.spring.boot.shop.online.components.services.CustomerServices;

import java.util.Date;

@Setter
@Component
//Add request vào CustomerRequestTypes = annotation
@CustomerRequestAnnotation(CustomerRequestTypes.REGISTER)
public class CustomerRegisterRequestHandler extends CustomerRequestAbstractHandler<CustomerRegisterRequest> {

    private static final Logger logger = LoggerFactory.getLogger(CustomerRegisterRequestHandler.class);

    @Autowired
    CustomerServices customerService;

    @Override
    public Object handle(CustomerRegisterRequest request) throws Exception {
        checkCustomerExist(request.getLoginName());
        User user = newUser(request);
        customerService.registerMember(newUser(request));
        logger.info("register customer SUCCESS with request: {0}" + request);
        return user;
    }

    private User newUser(CustomerRegisterRequest request) throws Exception {
        User user = new User();
        decorateCustomerByRequest(user, request);
        return user;
    }

    private void decorateCustomerByRequest(User user, CustomerRegisterRequest request) throws Exception {
        long seqNo = customerService.selectMaxSeq() + 1;
        user.setSeqUser(seqNo);
        user.setLoginName(request.getLoginName());
        user.setPassword(request.getPassword());
        user.setEmail(request.getEmail());
        user.setPhone(request.getPhone());
        user.setAddress(request.getAddress());
        user.setGender(request.getGender());
        user.setIdentityCard(request.getIdentityCard());
        user.setUserName(request.getUserName());
        user.setRoleUser(1);
        user.setIsDelete(1);
        Date date = new Date();
        user.setCreateDate(date);
    }

    private void checkCustomerExist(String loginName) throws Exception {
        User user = customerService.checkExistLoginName(loginName);
        if (user != null)
            throw new CustomerDuplicateException("customer: " + loginName + " is existed");
    }

}
