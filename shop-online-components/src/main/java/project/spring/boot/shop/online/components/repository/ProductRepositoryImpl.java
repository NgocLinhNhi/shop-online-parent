package project.spring.boot.shop.online.components.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import project.spring.boot.shop.online.components.config.DBConfig;
import project.spring.boot.shop.online.components.entity.Product;
import project.spring.boot.shop.online.components.mapper.ProductMapper;
import project.spring.boot.shop.online.components.mapper.ProductReportObject;
import project.spring.boot.shop.online.components.mapper.ReportMapper;
import project.spring.boot.shop.online.components.sql.ProductSqlQuery;

import java.util.List;

import static project.spring.boot.shop.online.components.sql.ProductSqlQuery.*;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    @Override
    public List<Product> loadAllProduct() throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(
                ProductSqlQuery.loadAllProductQuery().toString(),
                new ProductMapper());
    }

    @Override
    public List<ProductReportObject> exportProductReport() throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(
                ProductSqlQuery.loadAllProductQuery().toString(),
                new ReportMapper());
    }

    @Override
    public Product findProductById(Long seqPro) throws Exception {
        NamedParameterJdbcTemplate namedConnection = DBConfig.getInstance().getNamedConnection();
        return namedConnection.queryForObject(
                findProductBySeqQuery().toString(),
                new MapSqlParameterSource().addValue("seqPro", seqPro),
                new ProductMapper());
    }

    @Override
    public long selectMaxSeqProduct() throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.queryForObject(MAX_SEQ_PRODUCT, Long.class);
    }


    @Override
    public void insertProduct(Product product) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        jdbcTemplate.update(insertProductQuery().toString(),
                product.getSeqPro(),
                product.getProductName(),
                product.getCategory().getCategoryId(),
                product.getGuarantee(),
                product.getNumberSales(),
                product.getPrice(),
                product.getImageProduct(),
                product.getIsUsing(),
                product.getSysStatus(),
                product.getCreateDate(),
                product.getUpdateDate()
        );
    }

    @Override
    public void updateProduct(Product product) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        jdbcTemplate.update(updateProductQuery().toString(),
                product.getProductName(),
                product.getCategory().getCategoryId(),
                product.getGuarantee(),
                product.getNumberSales(),
                product.getPrice(),
                product.getImageProduct(),
                product.getIsUsing(),
                product.getSysStatus(),
                product.getCreateDate(),
                product.getUpdateDate(),
                product.getSeqPro()
        );
    }

    @Override
    public void deleteProduct(Long id) throws Exception {
        NamedParameterJdbcTemplate namedConnection = DBConfig.getInstance().getNamedConnection();
        namedConnection.update(DELETE_PRODUCT,
                new MapSqlParameterSource()
                        .addValue("seqPro", id)
        );
    }

    @Override
    public void insertBatchProduct(List<Product> products) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        handleBatchInsert(products, 500, jdbcTemplate);
    }

    private void handleBatchInsert(List<Product> products,
                                   int batchSize,
                                   JdbcTemplate jdbcTemplate) {
        jdbcTemplate.batchUpdate(
                insertProductByExcelQuery().toString(),
                products,
                batchSize,
                (preparedStatement, data) -> {
                    preparedStatement.setLong(1, data.getSeqPro());
                    preparedStatement.setString(2, data.getProductName());
                    preparedStatement.setObject(3, data.getCategoryId());
                });
    }
}
