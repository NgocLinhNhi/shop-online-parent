package project.spring.boot.shop.online.components.entity;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "PRODUCT")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "getAllProducts", procedureName = "GET_ALL_PRODUCTS", resultClasses = Product.class)})
@Getter
@Setter
public class Product implements Serializable {
    private static final long serialVersionUID = -5538416125326898258L;

    @Id
    @Column(name = "SEQ_PRO")
    private Long seqPro;

    // name = tên cột trong bảng Product => Không phải bảng cha Category
    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID", nullable = false)
    // cho insertable , updateable = false ở đây là khỏi insert update luôn -,-
    // @JsonIgnore => dung JsonIgnore  để không bị lỗi dataJson khi đưa data có bảng relation lên data
    @JsonBackReference
    private Category category;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "IMAGE_URL")
    private String imageProduct;

    @Column(name = "NUMBER_SALES")
    private BigDecimal numberSales;

    @Column(name = "GUARANTEE")
    private BigDecimal guarantee;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATE_DATE")
    private Date createDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_DATE")
    private Date updateDate;

    @Column(name = "SYS_STATUS")
    private Long sysStatus;

    @Column(name = "IS_USING")
    private String isUsing;

    //Mapped ở đây là tên biến trong Object( Java class ) con DetailBill
    //Set Lazy vì 1 product nhiều Bill => Slow
    @OneToMany(mappedBy = "product")
    private Set<DetailBill> detailBill;

    // Để join column trong table và manyToOne noó ko set được data cho object thì
    // chơi kiểu
    // tạo 1 object riêng ko map voi table trong DB
    // private transient Category categoryTest;

    private transient Long categoryId;

    private transient String categoryName;

    public Product() {
        super();
    }

    public Product(Long seqPro,
                   Category category,
                   String productName,
                   BigDecimal price,
                   String imageProduct,
                   BigDecimal numberSales,
                   BigDecimal guarantee,
                   Date createDate,
                   Date updateDate,
                   Long sysStatus,
                   Set<DetailBill> detailBill) {
        super();
        this.seqPro = seqPro;
        this.category = category;
        this.productName = productName;
        this.price = price;
        this.imageProduct = imageProduct;
        this.numberSales = numberSales;
        this.guarantee = guarantee;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.sysStatus = sysStatus;
        this.detailBill = detailBill;
    }

}