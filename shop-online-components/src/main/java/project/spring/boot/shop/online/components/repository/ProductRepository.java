package project.spring.boot.shop.online.components.repository;

import project.spring.boot.shop.online.components.entity.Product;
import project.spring.boot.shop.online.components.mapper.ProductReportObject;
import project.spring.boot.shop.online.components.mapper.ReportMapper;

import java.util.List;

public interface ProductRepository {

    List<Product> loadAllProduct() throws Exception;

    List<ProductReportObject> exportProductReport() throws Exception;

    Product findProductById(Long seqPro) throws Exception;

    long selectMaxSeqProduct() throws Exception;

    void updateProduct(Product product) throws Exception;

    void deleteProduct(Long id) throws Exception;

    void insertBatchProduct(List<Product> products) throws Exception;

    void insertProduct(Product product) throws Exception;
}
