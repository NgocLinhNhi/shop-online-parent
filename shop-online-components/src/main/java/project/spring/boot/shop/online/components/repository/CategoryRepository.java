package project.spring.boot.shop.online.components.repository;

import project.spring.boot.shop.online.components.entity.Category;

import java.util.List;

public interface CategoryRepository {
    List<Category> loadAllCategory() throws Exception;
}
