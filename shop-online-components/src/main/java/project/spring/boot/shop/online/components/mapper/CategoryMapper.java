package project.spring.boot.shop.online.components.mapper;

import org.springframework.jdbc.core.RowMapper;
import project.spring.boot.shop.online.components.entity.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryMapper implements RowMapper {

    @Override
    public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
        Category cate = new Category();
        cate.setCategoryId(rs.getLong("CATEGORY_ID"));
        cate.setCategoryName(rs.getString("CATEGORY_NAME"));
        return cate;
    }

}