﻿-- =============================================
-- Create database template
-- =============================================
USE master
GO

-- Drop the database if it already exists
IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'SalesOnlineCar'
)
DROP DATABASE SalesOnlineCar
GO

CREATE DATABASE SalesOnlineCar
GO

use SalesOnlineCar
go

create table ADMIN(
SEQ_NO int not null identity primary key,--khoa chinh admin
USER_ADMIN nvarchar(50) not null, -- ten dang nhap admin
PASSWORD_ADMIN nvarchar(255), -- password cua admin
EMAIL_ADMIN nvarchar(255),
ADDRESS_ADMIN nvarchar(255),
DATE_OF_BIRTH date, -- ngay sinh cua tai khoan admin
GENDER bit,-- gioi tinh admin
STATUS_ADMIN tinyint -- tinh trang tai khoan admin nay dang hoat dong hay ko
)

create table DIARYADMIN(
SEQ_NO int not null identity primary key,
ADMIN_ID int references admin(SEQ_NO),
CREATE_DATE datetime,
CONTENT_DIARY nvarchar(255)
)

create table ADVERTISEMENTS(
SEQ_NO int not null identity primary key,
ADMIN_ID int,
TITLE nvarchar(255),
CONTENT_AD nvarchar(255),
VIDEO nvarchar(255),
IMAGE_PRO nvarchar(255),
START_DATE_AD datetime,
END_DATE_AD datetime,
STATUS_AD tinyint
)

go
create table PRODUCER(
SEQ_NO int not null identity primary key,
PRODUCER_NAME nvarchar(255),
STATUS_PRODUCER tinyint
)

go
create table CATEGORY(
SEQ_NO int not null identity primary key,
PRODUCER_ID int references producer(SEQ_NO), 
CATEGORY_NAME nvarchar(255),
STATUS_CATEGORY tinyint
)

go
create table PRODUCTS(
SEQ_NO int not null identity primary key,
PRODUCT_NAME nvarchar(255),
CATEGORY_ID int references category(SEQ_NO),
PRICE decimal,
IMAGE_PRODUCT nvarchar(255),
NUMBER_SALES int,
GUARANTEE int,
CREATE_DATE datetime,
UPDATE_DATE datetime,
STATUS_PRODUCT tinyint
)

go
create table CUSTOMER(
SEQ_NO int not null identity primary key,
CUSTOMER_NAME nvarchar(255),
CUSTOMER_PASSWORD nvarchar(255),
EMAIL nvarchar(255),
ADDRESS_CUS nvarchar(255),
DATE_OF_BIRTH date,
PHONE_NUMBER nvarchar(20),
POST_CODE int,
GENDER bit,
STATUS_CUSTOMER tinyint,
TYPE_OF_CUSTOMER tinyint,
 -- dung de lam kieu dang Customer them bill nao thi update chuoi String them bill do
 -- BILL_ID nao bi xoa (STATUS_BILL = 2 ) thi update lai chuoi String xoa bill_ID do trong chuoi di
LINK_BILL nvarchar(255)
)

go
create table IDEACUSTOMER(
SEQ_NO int not null identity primary key,
CUSTOMER_ID int references customer(SEQ_NO),
PRODUCT_ID int,
CREATE_DATE datetime,
TITLE nvarchar(255),
CONTENT_IDEA nvarchar(255),
STATUS_IDEA tinyint
)

go
create table BILL(
SEQ_NO int not null identity primary key,
CUSTOMER_ID int references customer(SEQ_NO),
PHONE_CONTACT nvarchar(20),
ORDER_DATE datetime,
ACCEPT_DATE datetime,
STATUS_BILL tinyint,
ADDRESS_CUSTOMER nvarchar(255),
NAME_CUSTOMER nvarchar(255),
TOTAL_MONEY decimal
)

go
create table DETAILBILL(
BILL_ID int not null references bill(SEQ_NO),
PRODUCT_ID int not null references products(SEQ_NO),
NUMBER int,
SINGLE_PRICE decimal,
STATUS_DETAILBILL tinyint,
primary key(BILL_ID,PRODUCT_ID) 
)


--status 0 :delete 1 :work 2: waitting

-- insert ADMIN
go
insert into ADMIN values(N'Viet','123456',N'mrlytieudao@gmail.com',N'Different Heaven',getdate(),1,1)
insert into ADMIN values(N'Duyen','123456',N'mrlytieudao@gmail.com',N'Different Heaven',getdate(),1,1)

go
--insert CUSTOMER
--CUSTOMER 0 : member , 1: vip member
insert into CUSTOMER values(N'Viet','123456','mrlytieudao@gmail.com',N'Hà Nội','1987/05/16','0967643587','225309509',1,1,1,'1,2')
insert into CUSTOMER values(N'Long','123','LOng@gmail.com',N'Hà Nội','1987/05/30','0967643587','225309509',1,1,1,null)

-- insert PRODUCER
insert into PRODUCER values(N'Toyota',1)
insert into PRODUCER values(N'HonDa',1)
insert into PRODUCER values(N'Lamborgini',1)

go
--insert CATEGORY
insert into CATEGORY values(1,N'Lexus',1)
insert into CATEGORY values(1,N'Vios',1)
insert into CATEGORY values(1,N'Camry',1)
insert into CATEGORY values(1,N'Innova',1)
insert into CATEGORY values(1,N'Altis',1)
insert into CATEGORY values(1,N'Fortuner',0)
insert into CATEGORY values(1,N'Venza',0)
insert into CATEGORY values(1,N'Lancruiser',1)
insert into CATEGORY values(1,N'Yaris',1)
insert into CATEGORY values(1,N'Sienna',1)
insert into CATEGORY values(1,N'Zace',1)
insert into CATEGORY values(1,N'Hilux',1)
insert into CATEGORY values(1,N'Hiace',1)
insert into CATEGORY values(1,N'Crown',1)

go
--insert PRODUCT STATUS_PRODUCT : 1 ton tai 2 : delete
insert into PRODUCTS values(N'TOYOTA CAMRY 2.0',3,30,null,10,36,getdate(),getdate(),1)
insert into PRODUCTS values(N'TOYOTA CAMRY 2.5G',3,10,null,5,36,getdate(),getdate(),1)

go
-- insert DIARYADMIN
insert into DIARYADMIN values(1,getdate(),N'Sida qua di')

go
--insert ADVERTISEMENT STATUS_AD : 1 ok 0 :delete
insert into ADVERTISEMENTS values(1,N'NEW CAR',N'Xe moi ve ko xin ko lay tien day',null,null,getdate(),getdate(),1)

go
--insert BILL STATUS_BILL 1 : da thanh toan --  0 : chua thanh toan -- 2 : da huy 
insert into BILL values(1,'0967643587',getdate(),getdate(),1,N'Ha Noi','Viet',110)
insert into BILL values(2,'0967643587',getdate(),getdate(),1,N'Ha Noi','Long',0)

go
--insert DETAILBILL   1 : Ton tai -- 0 : Da xoa 
insert into DETAILBILL values(1,1,1,10,1)
insert into DETAILBILL values(1,2,5,20,1)

go
--insert IDEACUSTOMER STATUS_IDEA 1: ok 0: Xoa
insert into IDEACUSTOMER values(1,1,getdate(),'SIDA','TReo dau de ban thit cho',1)


go
select * from ADMIN
select * from DIARYADMIN
select * from ADVERTISEMENTS
select * from PRODUCER
select * from CATEGORY
select * from PRODUCTS
select * from CUSTOMER
select * from BILL
select * from DETAILBILL
select * from IDEACUSTOMER

--xp_readerrorlog 0, 1, N'Server is listening on' 