﻿-- =============================================
-- Create database template
-- =============================================
USE master
GO

-- Drop the database if it already exists
IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'SalesOnlineCar'
)
DROP DATABASE SalesOnlineCar
GO

CREATE DATABASE SalesOnlineCar
GO

use SalesOnlineCar
go

create table admin(
seq_no int not null identity primary key,
user_admin nvarchar(50) not null,
password_admin nvarchar(255),
email_admin nvarchar(255),
address_admin nvarchar(255),
date_of_birth date,
gender bit,
status_admin tinyint
)

create table diaryAdmin(
seq_no int not null identity primary key,
admin_id int references admin(seq_no),
create_date datetime,
content nvarchar(255)
)

create table advertisements(
seq_no int not null identity primary key,
admin_id int,
title nvarchar(255),
content_ad nvarchar(255),
video nvarchar(255),
image nvarchar(255),
start_date datetime,
end_date datetime,
status_ad tinyint
)

go
create table producer(
seq_no int not null identity primary key,
producer_Name nvarchar(255),
status_producer tinyint
)

go
create table category(
seq_no int not null identity primary key,
producer_id int references producer(seq_no), 
category_name nvarchar(255),
status_category tinyint
)

go
create table products(
seq_no int not null identity primary key,
productName nvarchar(255),
categoryId int references category(seq_no),
price float,
image nvarchar(255),
number_sales int,
guarantee int,
createDate datetime,
updateDate datetime,
statusProduct tinyint
)

go
create table customer(
seq_no int not null identity primary key,
customer_name nvarchar(255),
customer_password nvarchar(255),
email nvarchar(255),
address nvarchar(255),
date_of_birth date,
phone nvarchar(20),
post_code int,
gender bit,
status_customer tinyint,
type_of_customer tinyint
)

go
create table ideaCustomer(
seq_no int not null primary key,
customer_id int references customer(seq_no),
product_id int,
create_date datetime,
title nvarchar(255),
content nvarchar(255),
status tinyint
)

go
create table bill(
seq_no int not null identity primary key,
customer_id int references customer(seq_no),
phone nvarchar(20),
order_date datetime,
accept_date datetime,
status_bill tinyint,
address_customer nvarchar(255),
name_customer nvarchar(255),
total_money float
)

go
create table detailBill(
bill_id int not null references bill(seq_no),
product_id int not null references products(seq_no),
number int,
single_price float,
primary key(bill_id,product_id)
)


--status 0 :delete 1 :work 2: waitting

-- insert admin
go
insert into admin values(N'Viet','123456','mrlytieudao@gmail.com','Different Heaven',getdate(),1,1)

go
--insert customer
--type_of_customer 0 : member , 1: vip member
insert into customer values(N'Viet','123456','mrlytieudao@gmail.com',N'Hà Nội','1987/05/16','0967643587','225309509',1,1,1)

-- insert producer
insert into producer values(N'Toyota',1)
insert into producer values(N'HonDa',1)
insert into producer values(N'Lamborgini',1)

go
--insert category
insert into category values(1,N'Lexus',1)
insert into category values(1,N'Vios',1)
insert into category values(1,N'Camry',1)
insert into category values(1,N'Innova',1)
insert into category values(1,N'Altis',1)
insert into category values(1,N'Fortuner',1)
insert into category values(1,N'Venza',1)
insert into category values(1,N'Lancruiser',1)
insert into category values(1,N'Yaris',1)
insert into category values(1,N'Sienna',1)
insert into category values(1,N'Zace',1)
insert into category values(1,N'Hilux',1)
insert into category values(1,N'Hiace',1)
insert into category values(1,N'Crown',1)


go
-- insert products


go
select * from admin
select * from diaryAdmin
select * from advertisements
select * from producer
select * from category
select * from products
select * from customer
select * from bill
select * from detailBill
select * from ideaCustomer
