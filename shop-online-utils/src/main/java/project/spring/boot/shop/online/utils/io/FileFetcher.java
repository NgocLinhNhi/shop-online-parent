package project.spring.boot.shop.online.utils.io;

import java.io.File;

public interface FileFetcher {

    File getFile(String filePath);

}
