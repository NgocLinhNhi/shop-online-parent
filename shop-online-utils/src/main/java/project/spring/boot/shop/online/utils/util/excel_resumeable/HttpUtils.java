package project.spring.boot.shop.online.utils.util.excel_resumeable;

class HttpUtils {

    static boolean isEmpty(String value) {
        return value == null || "".equals(value);
    }

    static long toLong(String value, long def) {
        if (isEmpty(value)) {
            return def;
        }
        try {
            return Long.valueOf(value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return def;
        }
    }

    static int toInt(String value, int def) {
        if (isEmpty(value)) {
            return def;
        }
        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return def;
        }
    }
}
