package project.spring.boot.shop.online.utils.util.excel_resumeable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;

@Service
public class CreateResumeAbleFile {

    private static final Logger logger = LoggerFactory.getLogger(CreateResumeAbleFile.class);

    @Value("${file.resources.location}")
    private String termFilePath;

    ResumeAbleInfo createResumeAbleInfo(HttpServletRequest request) throws ServletException {
        //các Param resumableChunkSize ... này không đổi được nó mapping trong file .js sửa cực lắm ..
        int resumeAbleChunkSize = HttpUtils.toInt(request.getParameter("resumableChunkSize"), -1);
        int resumeAbleChunkNumber = HttpUtils.toInt(request.getParameter("resumableChunkNumber"), -1);
        long resumeAbleTotalSize = HttpUtils.toLong(request.getParameter("resumableTotalSize"), -1);
        String resumeAbleIdentifier = request.getParameter("resumableIdentifier");
        String resumeAbleRelativePath = request.getParameter("resumableRelativePath");

        createFolder();
        String resumeAbleFilePath = createFilePath(resumeAbleIdentifier);

        ResumeAbleInfoStorage storage = ResumeAbleInfoStorage.getInstance();
        ResumeAbleInfo info = storage.setInfo(
                resumeAbleChunkSize,
                resumeAbleTotalSize,
                resumeAbleIdentifier,
                resumeAbleIdentifier,
                resumeAbleRelativePath,
                resumeAbleFilePath,
                resumeAbleChunkNumber);

        return checkValidResumeAbleInfo(info, storage);
    }

    private ResumeAbleInfo checkValidResumeAbleInfo(ResumeAbleInfo info,
                                                    ResumeAbleInfoStorage storage) throws ServletException {
        if (!info.valid()) {
            storage.remove(info);
            throw new ServletException("Invalid request params.");
        }
        return info;
    }


    private String createFilePath(String resumeAbleIdentifier) {
        return new File(termFilePath, getCurrentTimeCreateFile() + resumeAbleIdentifier).getAbsolutePath();
    }

    private long getCurrentTimeCreateFile() {
        return new Date().getTime();
    }

    private void createFolder() {
        boolean mkdir = new File(termFilePath).mkdir();
        if (!mkdir) logger.info("Folder existed on server !!!");
        else logger.info("Folder has been created on server !!!");
    }
}
