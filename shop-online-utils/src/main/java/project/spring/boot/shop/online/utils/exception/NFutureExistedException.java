package project.spring.boot.shop.online.utils.exception;

import project.spring.boot.shop.online.utils.concurrent.NFuture;

public class NFutureExistedException extends IllegalArgumentException {
    private static final long serialVersionUID = 4258315197030448654L;

    public NFutureExistedException(Object key, NFuture old) {
        super("future with key: " + key + " existed: " + old);
    }

}
