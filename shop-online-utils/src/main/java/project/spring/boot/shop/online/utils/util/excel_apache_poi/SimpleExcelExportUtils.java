package project.spring.boot.shop.online.utils.util.excel_apache_poi;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.spring.boot.shop.online.utils.io.NFiles;
import project.spring.boot.shop.online.utils.util.StringUtil;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static project.spring.boot.shop.online.utils.util.DateConstant.PATTERN_YY_MM_DD;

public class SimpleExcelExportUtils {
    private Logger logger = LoggerFactory.getLogger(SimpleExcelExportUtils.class);
    private String fileNameExport;
    private String filePath;
    private String excelFileName;

    public SimpleExcelExportUtils(String filePath, String excelFileName) {
        this.filePath = filePath;
        this.excelFileName = excelFileName;
    }

    public XSSFWorkbook createXSSFWorkbook() {
        return new XSSFWorkbook();
    }

    //Dùng cho export file lớn - tránh bị java heap space
    public SXSSFWorkbook createSXSSFWorkbook() {
        return new SXSSFWorkbook();
    }

    public void writeFile(HttpServletResponse response, Workbook workbook) throws IOException {
        long startTime = System.currentTimeMillis();
        logger.info("Start Handle export File Excel process ");

        String fullPathExcel = createFilePath(filePath, excelFileName);
        NFiles.newFile(fullPathExcel);
        writeFileOnServer(workbook, fullPathExcel);
        writeFileResponse(response, workbook);

        workbook.close();
        long endTime = System.currentTimeMillis();
        long timeResult = endTime - startTime;
        logger.info("End Handle export File Excel process : " + timeResult);
    }

    //Export file - return file for front-end api
    private void writeFileResponse(HttpServletResponse response, Workbook workbook) throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileNameExport);
        try (ServletOutputStream outputStream = response.getOutputStream()) {
            workbook.write(outputStream);
            outputStream.flush();
        }
    }

    //Save File on Server => Tùy thích thì save không thì thôi ( Đỡ tốn RAM)
    //Nghĩa là file nặng thì 2 lần ghi = 2 lần xử lý file ..
    private void writeFileOnServer(Workbook workbook, String filePathExcel) throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(filePathExcel)) {
            workbook.write(outputStream);
            outputStream.flush();
        }
    }

    private String createFilePath(String filePath, String exportFileName) {
        //Để như này thì file kết quả import sẽ lưu vào đây => C:\Users\Computer\AppData\Local\Temp
        //return System.getProperty("java.io.tmpdir") + File.separator + "temp";
        fileNameExport = exportFileName + getCurrentTimeCreateFile() + ".xlsx";
        logger.info("Export File Name  : " + fileNameExport);
        String exportFilePath = filePath + File.separator + exportFileName + getCurrentTimeCreateFile() + ".xlsx";
        logger.info("File Path on server  : " + exportFilePath);
        return exportFilePath;
    }

    private long getCurrentTimeCreateFile() {
        return new Date().getTime();
    }

    // Build Header Excel Utils.
    public void writeHeaderLine(XSSFSheet sheet, String headerTitles) {
        createHeader(sheet, parseListTitles(headerTitles));
    }

    private List<String> parseListTitles(String headerTitles) {
        return StringUtil.splitListFile(headerTitles);
    }

    private void createHeader(XSSFSheet sheet, List<String> titles) {
        Row headerRow = sheet.createRow(0);
        AtomicInteger columnCount = new AtomicInteger();
        for (String title : titles) {
            Cell cell = headerRow.createCell(columnCount.getAndIncrement());
            cell.setCellValue(title);
        }
    }

    //Excel Utils handle cellType
    public void formatDateCell(XSSFWorkbook workbook, Cell cell) {
        CellStyle cellStyle = workbook.createCellStyle();
        CreationHelper creationHelper = workbook.getCreationHelper();
        cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat(PATTERN_YY_MM_DD));
        cell.setCellStyle(cellStyle);
    }
}
