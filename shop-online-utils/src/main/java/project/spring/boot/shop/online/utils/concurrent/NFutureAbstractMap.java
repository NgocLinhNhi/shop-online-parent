package project.spring.boot.shop.online.utils.concurrent;

import project.spring.boot.shop.online.utils.exception.NFutureExistedException;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class NFutureAbstractMap<K> implements NFutureMap<K> {

    protected final Map<K, NFuture> map;

    public NFutureAbstractMap() {
        this.map = newFutureMap();
    }

    protected abstract Map<K, NFuture> newFutureMap();

    @Override
    public NFuture addFuture(K key) {
        return addFuture(key, new NFutureTask());
    }

    @Override
    public NFuture addFuture(K key, NFuture future) {
        NFuture old = map.putIfAbsent(key, future);
        return old == null ? future : old;
    }

    @Override
    public NFuture putFuture(K key) {
        AtomicBoolean existed = new AtomicBoolean(true);
        NFuture future = map.computeIfAbsent(key, k -> {
            existed.set(false);
            return new NFutureTask();
        });
        if (existed.get())
            throw new NFutureExistedException(key, future);
        return future;
    }

    @Override
    public NFuture getFuture(K key) {
        return map.get(key);
    }

    @Override
    public NFuture removeFuture(K key) {
        return map.remove(key);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public Map<K, NFuture> clear() {
        Map<K, NFuture> answer = new HashMap<>(map);
        map.clear();
        return answer;
    }

}