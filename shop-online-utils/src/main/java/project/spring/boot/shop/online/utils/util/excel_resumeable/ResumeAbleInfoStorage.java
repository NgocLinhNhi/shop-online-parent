package project.spring.boot.shop.online.utils.util.excel_resumeable;

import java.util.HashMap;

public class ResumeAbleInfoStorage {

    private HashMap<String, ResumeAbleInfo> mMap = new HashMap<>();
    private static ResumeAbleInfoStorage INSTANCE;

    public static synchronized ResumeAbleInfoStorage getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ResumeAbleInfoStorage();
        }
        return INSTANCE;
    }

    private ResumeAbleInfoStorage() {
    }

    synchronized ResumeAbleInfo setInfo(int resumeAbleChunkSize,
                                        long resumeAbleTotalSize,
                                        String resumeAbleIdentifier,
                                        String resumeAbleFilename,
                                        String resumeAbleRelativePath,
                                        String resumeAbleFilePath,
                                        int resumeAbleChunkNumber) {

        ResumeAbleInfo info = mMap.get(resumeAbleIdentifier);

        if (info == null) {
            info = new ResumeAbleInfo();

            info.resumeAbleChunkSize = resumeAbleChunkSize;
            info.resumeAbleTotalSize = resumeAbleTotalSize;
            info.resumeAbleIdentifier = resumeAbleIdentifier;
            info.resumeAbleFilename = resumeAbleFilename;
            info.resumeAbleRelativePath = resumeAbleRelativePath;
            info.resumeAbleFilePath = resumeAbleFilePath;
            info.resumeAbleChunkNumber = resumeAbleChunkNumber;

            mMap.put(resumeAbleIdentifier, info);
        }
        return info;
    }

    public void remove(ResumeAbleInfo info) {
        mMap.remove(info.resumeAbleIdentifier);
    }
}
