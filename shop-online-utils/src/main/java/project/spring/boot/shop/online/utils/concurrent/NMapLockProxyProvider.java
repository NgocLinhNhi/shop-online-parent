package project.spring.boot.shop.online.utils.concurrent;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;

public abstract class NMapLockProxyProvider implements NMapLockProvider {

    protected final Map<Object, NLockProxy> locks;

    public NMapLockProxyProvider() {
        this.locks = newLockMap();
    }

    protected abstract Map<Object, NLockProxy> newLockMap();

    @Override
    public Lock provideLock(Object key) {
        synchronized (locks) {
            NLockProxy lock = locks.get(key);
            if (lock == null) {
                lock = new NLockProxy();
                locks.put(key, lock);
            }
            lock.retain();
            return lock;
        }
    }

    @Override
    public Lock getLock(Object key) {
        synchronized (locks) {
            NLockProxy lock = locks.get(key);
            return lock;
        }
    }

    @Override
    public void removeLock(Object key) {
        synchronized (locks) {
            NLockProxy lock = locks.get(key);
            if (lock != null) {
                lock.release();
                if (lock.isReleasable())
                    locks.remove(key);
            }
        }
    }

    @Override
    public void removeLocks(Set<?> keys) {
        synchronized (locks) {
            for (Object key : keys) {
                NLockProxy lock = locks.get(key);
                if (lock != null) {
                    lock.release();
                    if (lock.isReleasable())
                        locks.remove(key);
                }
            }
        }
    }

    @Override
    public int size() {
        synchronized (locks) {
            return locks.size();
        }
    }
}
