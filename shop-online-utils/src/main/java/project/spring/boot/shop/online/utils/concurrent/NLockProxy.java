package project.spring.boot.shop.online.utils.concurrent;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

public class NLockProxy extends NRef<Lock> implements Lock {

    public static final Supplier<NLockProxy> SUPPLIER = NLockProxy::new;

    public NLockProxy() {
        this(new ReentrantLock());
    }

    public NLockProxy(Lock value) {
        super(value);
    }

    @Override
    public void lock() {
        value.lock();
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        value.lockInterruptibly();
    }

    @Override
    public boolean tryLock() {
        return value.tryLock();
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return value.tryLock(time, unit);
    }

    @Override
    public void unlock() {
        value.unlock();
    }

    @Override
    public Condition newCondition() {
        return value.newCondition();
    }

}