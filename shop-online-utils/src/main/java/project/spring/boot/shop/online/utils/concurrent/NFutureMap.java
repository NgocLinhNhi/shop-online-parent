package project.spring.boot.shop.online.utils.concurrent;

import java.util.Map;

public interface NFutureMap<K> {

    NFuture addFuture(K key);

    NFuture addFuture(K key, NFuture future);

    NFuture putFuture(K key);

    NFuture getFuture(K key);

    NFuture removeFuture(K key);

    int size();

    Map<K, NFuture> clear();

}
