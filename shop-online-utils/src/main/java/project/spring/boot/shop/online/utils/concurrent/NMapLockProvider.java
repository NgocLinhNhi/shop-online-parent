package project.spring.boot.shop.online.utils.concurrent;

import java.util.Set;
import java.util.concurrent.locks.Lock;

public interface NMapLockProvider {

    Lock provideLock(Object key);

    Lock getLock(Object key);

    void removeLock(Object key);

    void removeLocks(Set<?> keys);

    int size();

}
