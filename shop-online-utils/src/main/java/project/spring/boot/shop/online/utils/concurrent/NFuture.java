package project.spring.boot.shop.online.utils.concurrent;

public interface NFuture {

    void setResult(Object result);

    void setException(Exception exception);

    void cancel(String message);

    boolean isDone();

    <V> V get() throws Exception;

    <V> V get(long timeout) throws Exception;

}
