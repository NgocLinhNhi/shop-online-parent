package project.spring.boot.shop.online.utils.concurrent;

public class NRef<T> {

    protected final T value;
    protected volatile int referenceCount;

    public NRef(T value) {
        this.value = value;
    }

    public T get() {
        return value;
    }

    public void retain() {
        ++this.referenceCount;
    }

    public void release() {
        --this.referenceCount;
    }

    public boolean isReleasable() {
        return this.referenceCount == 0;
    }

}
