package project.spring.boot.shop.online.utils.util.excel_resumeable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashSet;

public class ResumeAbleInfo {
    private static final Logger logger = LoggerFactory.getLogger(ResumeAbleInfo.class);

    int resumeAbleChunkSize;
    long resumeAbleTotalSize;
    String resumeAbleIdentifier;
    String resumeAbleFilename;
    String resumeAbleRelativePath;
    String resumeAbleFilePath;
    String finalFilePath;
    int resumeAbleChunkNumber;

    //Chunks uploaded
    HashSet<ResumeAbleChunkNumber> uploadedChunks = new HashSet<>();

    public static class ResumeAbleChunkNumber {
        ResumeAbleChunkNumber(int number) {
            this.number = number;
        }

        public int number;

        @Override
        public boolean equals(Object obj) {
            return obj instanceof ResumeAbleChunkNumber && ((ResumeAbleChunkNumber) obj).number == this.number;
        }

        @Override
        public int hashCode() {
            return number;
        }
    }

    public boolean valid() {
        return resumeAbleChunkSize >= 0 && resumeAbleTotalSize >= 0
                && !HttpUtils.isEmpty(resumeAbleIdentifier)
                && !HttpUtils.isEmpty(resumeAbleFilename)
                && !HttpUtils.isEmpty(resumeAbleRelativePath);
    }

    boolean checkIfUploadFinished() {
        int count = (int) Math.ceil(((double) resumeAbleTotalSize) / ((double) resumeAbleChunkSize));
        for (int i = 1; i < count; i++) {
            if (!uploadedChunks.contains(new ResumeAbleChunkNumber(i))) {
                return false;
            }
        }

        changeFileName();
        return true;
    }

    private void changeFileName() {
        //Upload finished, nối đuôi .xlsx cho file RandomAccessFile đã tạo ra trên server
        File file = new File(resumeAbleFilePath);
        finalFilePath = file.getAbsolutePath().replaceAll("xlsx", ".xlsx");
        boolean result = file.renameTo(new File(finalFilePath));
        if (!result) logger.error("Handle with file excel on server has error ");
    }
}
