package project.spring.boot.shop.online.utils.io;

import java.io.File;
import java.net.URL;

public class AnywayFileFetcher implements FileFetcher {

    @Override
    public File getFile(String filePath) {
        URL resource = getClass().getResource(filePath);
        if(resource == null)
            resource = getClass().getResource("/" + filePath);
        if(resource != null)
            return new File(resource.getFile());
        return new File(filePath);
    }
}
