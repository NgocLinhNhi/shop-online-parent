package project.spring.boot.shop.online.utils.util.excel_apache_poi;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.spring.boot.shop.online.utils.io.NFiles;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class SimpleExcelImportUtils {
    private Logger logger = LoggerFactory.getLogger(SimpleExcelImportUtils.class);
    private String filePath;
    private String excelFileName;
    private String exportFilePath;

    public SimpleExcelImportUtils(String filePath, String excelFileName) {
        this.filePath = filePath;
        this.excelFileName = excelFileName;
    }

    public String writeFile(Boolean validateResult, Workbook workbook) throws IOException {
        if (!validateResult) {
            String fullPathExcel = createFilePath(filePath, excelFileName);
            NFiles.newFile(fullPathExcel);
            writeFileOnServer(workbook, fullPathExcel);
            workbook.close();
            logger.error("Some Error has been occurred when import file");
            return exportFilePath;
        }
        return null;
    }

    //write file error on server
    private void writeFileOnServer(Workbook workbook, String filePathExcel) throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(filePathExcel)) {
            workbook.write(outputStream);
            outputStream.flush();
        }
    }

    //read file error stored on server and write to error file then response for API
    public static void writeFileResponse(HttpServletResponse response,
                                         String filePathError) throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + getFileNameExport(filePathError));
        try (ServletOutputStream outputStream = response.getOutputStream();
             FileInputStream inputStream = new FileInputStream(filePathError)) {
            int ch;
            while ((ch = inputStream.read()) != -1) {
                outputStream.write(ch);
            }
            outputStream.flush();
        }
    }

    private static String getFileNameExport(String filePathError) {
        return filePathError.split("\\\\")[1];
    }

    private long getCurrentTimeCreateFile() {
        return new Date().getTime();
    }

    private String createFilePath(String filePath, String exportFileName) {
        //Để như này thì file kết quả import sẽ lưu vào đây => C:\Users\Computer\AppData\Local\Temp
        //return System.getProperty("java.io.tmpdir") + File.separator + "temp";
        exportFilePath = filePath + File.separator + exportFileName + getCurrentTimeCreateFile() + ".xlsx";
        logger.info("File Path on server  : " + exportFilePath);
        return exportFilePath;
    }

}
