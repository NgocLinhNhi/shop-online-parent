package project.spring.boot.shop.online.utils.concurrent;

import java.util.HashMap;
import java.util.Map;

public class NHashMapLockProxyProvider extends NMapLockProxyProvider {

    @Override
    protected Map<Object, NLockProxy> newLockMap() {
        return new HashMap<>();
    }

}