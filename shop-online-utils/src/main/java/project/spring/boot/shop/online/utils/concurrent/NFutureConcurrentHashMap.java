package project.spring.boot.shop.online.utils.concurrent;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NFutureConcurrentHashMap<K> extends NFutureAbstractMap<K> {

    @Override
    protected Map<K, NFuture> newFutureMap() {
        return new ConcurrentHashMap<>();
    }

}