package project.spring.boot.shop.online.utils.concurrent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class NThreadFactory implements ThreadFactory {
    private static final Logger logger = LoggerFactory.getLogger(NThreadFactory.class);

    private int poolId;
    protected int priority;
    protected String prefix;
    protected boolean daemon;
    protected String poolName;
    protected String threadPrefix;
    protected ThreadGroup threadGroup;
    protected AtomicInteger threadCounter = new AtomicInteger();

    private static final AtomicInteger POOL_COUNTER = new AtomicInteger();

    public NThreadFactory(String threadName) {
        this.poolId = POOL_COUNTER.incrementAndGet();
        this.poolName = threadName;
        this.daemon = false;
        this.prefix = "bo";
        this.priority = Thread.NORM_PRIORITY;
        this.threadGroup = getSystemThreadGroup();
        this.poolName = getFullPoolName();
        this.threadPrefix = poolName + '-' + poolId + '-';
    }

    protected ThreadGroup getSystemThreadGroup() {
        return getSecurityManager() == null ? Thread.currentThread().getThreadGroup() : getSecurityManager().getThreadGroup();
    }

    protected SecurityManager getSecurityManager() {
        return System.getSecurityManager();
    }

    @Override
    public Thread newThread(Runnable runnable) {
        Thread thread = createThread(runnable, getThreadName());
        setUpThread(thread);
        return thread;
    }

    protected Thread createThread(Runnable runnable, String name) {
        return new Thread(runnable, name);
    }

    protected void setUpThread(Thread thread) {
        try {
            trySetUpThread(thread);
        } catch (Exception e) {
            logger.info("can not setup processor {}" + thread.getName() + e);
        }
    }

    protected void trySetUpThread(Thread thread) {
        if (thread.isDaemon()) {
            if (!daemon)
                thread.setDaemon(false);
        } else {
            if (daemon)
                thread.setDaemon(true);
        }
        if (thread.getPriority() != priority)
            thread.setPriority(priority);
    }

    protected String getThreadName() {
        return threadPrefix + threadCounter.incrementAndGet();
    }

    protected String getFullPoolName() {
        return prefix + "-" + poolName;
    }
}
