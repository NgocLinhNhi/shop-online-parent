package project.spring.boot.shop.online.gateway.api;

import org.junit.jupiter.api.Test;
import project.spring.boot.shop.online.components.view.ProductFileExcel;
import project.spring.boot.shop.online.gateway.api.custom_exception.ExcelFileSizeException;
import project.spring.boot.shop.online.gateway.api.validator.ExcelValidator;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class ExcelValidatorTest {

    @Test()
    void validateSizeTestException() {
        ExcelValidator excelValidator = new ExcelValidator();
        Exception exception = assertThrows(ExcelFileSizeException.class, () -> excelValidator.validate(createProducts()));
        assertTrue(exception.getMessage().contains("PageSize dont allow more than 10 row"));
    }

    @Test
    void validateSizeTest() {
        ExcelValidator excelValidator = new ExcelValidator();
        excelValidator.validate(createProductSingle());
    }

    @Test
    void validateSizeTestMockito() {
        ExcelValidator excelValidator = new ExcelValidator();
        excelValidator.validate(createProductSingle());
    }

    @Test
    void validateTestMethod() {
        ProductFileExcel productFileExcel = mock(ProductFileExcel.class);
        List<ProductFileExcel> listResult = new ArrayList<>();
        listResult.add(productFileExcel);

        ExcelValidator excelValidator = new ExcelValidator();
        assertFalse(excelValidator.test(listResult));
    }

    private List<ProductFileExcel> createProductSingle() {
        List<ProductFileExcel> listResult = new ArrayList<>();
        listResult.add(createNewProduct(1L, "Linh"));
        return listResult;
    }

    private List<ProductFileExcel> createProducts() {
        List<ProductFileExcel> listResult = new ArrayList<>();
        listResult.add(createNewProduct(1L, "Linh"));
        listResult.add(createNewProduct(1L, "Ninh ahihi hoho do ngoc alibaba kakaa"));
        listResult.add(createNewProduct(200L, "Viet"));
        listResult.add(createNewProduct(200L, "Viet"));
        listResult.add(createNewProduct(200L, "Viet"));
        listResult.add(createNewProduct(200L, "Viet"));
        listResult.add(createNewProduct(200L, "Viet"));
        listResult.add(createNewProduct(200L, "Viet"));
        listResult.add(createNewProduct(200L, "Viet"));
        listResult.add(createNewProduct(200L, "Viet"));
        listResult.add(createNewProduct(200L, "Viet"));

        return listResult;
    }

    private ProductFileExcel createNewProduct(Long category, String productName) {
        ProductFileExcel pro = new ProductFileExcel();
        pro.setCategory(category);
        pro.setProductName(productName);
        return pro;
    }
}
