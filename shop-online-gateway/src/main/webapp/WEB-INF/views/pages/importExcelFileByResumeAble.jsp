<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thêm mới Product từ File Excel</title>

    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script>
    <!-- for validate form -->
    <script type="text/javascript" src="/jquerry/jquery.dataTables.min.js"></script>

    <!-- de bootstrap import dưới jquery ko bi conflict thu vien goi boostrap của jquerry ko gọi đc modal popup -->
    <link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- validate cho datatable data hien thi -->
    <script src="/DataTables/js/dataTables.bootstrap.min.js"></script>

    <script src="/js/commonAjax.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">

    <!-- hỗ trợ download upload file = resumable -->
    <link rel="stylesheet" type="text/css" href="/resumable/css/style.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Contain  -->
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="box col-md-12">
                    <div class="box-body">
                        <form action="/report/importProductByFileExcel"
                              method="POST"
                              id="fileUploadForm"
                              class="form-horizontal col-md-8 col-md-offset-2"
                              role="form">

                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Thêm mới từ file Excel</legend>

                                <div class="col-md-12 form-group" style="float: left;">
                                    <div class="col-md-3 col-md-offset-2">Tải file mẫu</div>
                                    <div class="col-md-6">
                                        <a href="/download_template_import/TemplateImportProduct.xlsx" target="_self">TemplateImportProduct.xlsx</a>
                                    </div>
                                </div>

                                <div class="col-md-12 form-group" style="float: left;">
                                    <div class="col-md-3 col-md-offset-2">Đường dẫn file (Dung lượng tối đa 1MB)</div>

                                    <div class="col-md-6">
                                        <a href="#" class="resumable-browse">Chọn file cần import</a>
                                        <div id="file-not-found" class="error"></div>
                                    </div>
                                </div>

                                <div class="col-md-12 form-group"></div>
                                <div class="col-md-12 form-group">
                                    <div class="col-md-3 col-md-offset-5">
                                        <input class="btn btn-primary"
                                               type="button"
                                               onclick="uploadFile()"
                                               name="" value="Import">
                                    </div>
                                </div>

                                <div class="col-md-12 resumable-progress">
                                    <table>
                                        <tr>
                                            <td width="100%">
                                                <div class="progress-container">
                                                    <div class="progress-bar"></div>
                                                </div>
                                            </td>
                                            <td class="progress-text" nowrap="nowrap"></td>
                                            <td class="progress-pause" nowrap="nowrap">
                                                <a href="#" onclick="r.upload(); return(false);"
                                                   class="progress-resume-link">
                                                    <i class="fa fa-play"></i>
                                                </a>

                                                <a href="#" onclick="r.pause(); return(false);"
                                                   class="progress-pause-link">
                                                    <i class="fa fa-pause"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- end Contain -->

</div>
<!-- start modal -->
<div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
    <form action="/report/exportExcelFileErrorByResumeAble" method="post">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close dongmodal" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thông báo</h4>
                </div>

                <div class="modal-body">
                    <p id="massagemodal"></p>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn" id="viewDetail">Xem chi tiết</button>
                    <button type="button" class="btn dongmodal" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- endmodal -->

<!-- hỗ trợ download upload file = resumable -->
<script type="text/javascript" src="/resumable/js/resumable.js"></script>

<script type="text/javascript">
    function uploadFile() {
        if (r.files.length === 0) {
            $("#file-not-found").html("<b>Vui lòng chọn file.</b>")
        }

        if (r.files[0].size / 1024 > 1024) {
            $("#file-not-found").html("<b>Vui lòng chọn file < 1MB.</b>");
            r.removeFile(r.files[0]);
            $(".resumable-browse").html("Chọn file cần import");
        }

        if ($("#fileUploadForm").valid() && r.size !== 0 && r.files[0].size / 1024 < 1024) {
            $('.resumable-progress, .resumable-list').show();
            // Show pause, hide resume
            $('.resumable-progress .progress-resume-link').hide();
            $('.resumable-progress .progress-pause-link').show();
            r.upload();
        }
    }

    var r = new Resumable({
        target: 'importProductByFileExcel',
        chunkSize: 1024 * 1024,
        simultaneousUploads: 4,
        testChunks: true,
        throttleProgressCallbacks: 1,
        method: "octet",
        fileType: [".xlsx"],
        fileTypeErrorCallback: function (file, errorCount) {
            $("#viewDetail").hide();
            var message = file.fileName || file.name + " không đúng định dạng .xlsx";
            $('#massagemodal').html(message);
            $('.modal-header').addClass("btn-danger");
            $('.dongmodal').addClass("btn-danger");
            $('#myModal').modal('toggle');
        }
    });

    // Show a place selecting files
    r.assignBrowse($('.resumable-browse')[0]);

    // Handle file add event
    r.on('fileAdded', function (file) {
        // Add the file to the list
        $(".resumable-browse").html(file.fileName);
        $('.resumable-list').html
        ('<li class="resumable-file-' + file.uniqueIdentifier + '">' +
            '<span class="resumable-file-status">Đang tải</span> ' +
            '<span class="resumable-file-name"></span>' +
            '<span class="resumable-file-progress"></span>');
        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-name').html(file.fileName);
        $("#file-not-found").html("")
    });

    r.on('pause', function () {
        // Show resume, hide pause
        $('.resumable-progress .progress-resume-link').show();
        $('.resumable-progress .progress-pause-link').hide();
    });

    r.on('complete', function () {
        // Hide pause/resume when the upload has completed
        $('.resumable-progress .progress-resume-link, .resumable-progress .progress-pause-link').hide();
    });

    r.on('fileSuccess', function (file, message) {
        var modal_header = $('.modal-header');
        var close_modal = $('.dongmodal');

        $("#viewDetail").hide();
        modal_header.removeClass("btn-danger");
        close_modal.removeClass("btn-danger");
        $('#massagemodal').html(message);
        modal_header.addClass("btn-success");
        close_modal.addClass("btn-success");
        $('#myModal').modal('toggle');
        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-status').html('Đã tải lên');
        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress').html('thành công');
        $(".resumable-browse").html("Chọn file cần import");
        r.removeFile(file);
    });

    r.on('fileError', function (file, message) {
        debugger;
        var viewDetail = $("#viewDetail");

        var response = JSON.parse(message);
        viewDetail.hide();
        // Lỗi 400 = validate file thi mới hiện button export file lỗi => hạn chế che đậy khiếm khuyết của ResumeAble
        if (response.responseCode === 400) viewDetail.show();
        $('#massagemodal').html(response.message);

        $('.modal-header').addClass("btn-danger");
        $('.dongmodal').addClass("btn-danger");
        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-status').html('Tải lên');
        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress').html('thất bại');
        $('#myModal').modal('toggle');
    });

    r.on('fileProgress', function (file) {
        // Handle progress for both the file and the overall upload
        $('.resumable-file-' + file.uniqueIdentifier + ' .resumable-file-progress').html(Math.floor(file.progress() * 100) + '%');
        $('.progress-bar').css({width: Math.floor(r.progress() * 100) + '%'});
    });
</script>
</body>
</html>
