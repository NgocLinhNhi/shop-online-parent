<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>Quản lý sản phẩm</title>

    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script>
    <!-- for validate form -->
    <script type="text/javascript" src="/jquerry/jquery.dataTables.min.js"></script>

    <!-- de bootstrap import dưới jquery ko bi conflict thu vien goi boostrap của jquerry ko gọi đc modal popup -->
    <link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- validate cho datatable data hien thi -->
    <script src="/DataTables/js/dataTables.bootstrap.min.js"></script>

    <script src="/js/commonAjax.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">
</head>


<body>
<div class="content-wrapper" style="margin-top: 30px;">

    <div class="box-body" style="margin-left: 570px;margin-bottom: 10px;">
        <div class="footform">
            <a href="/product/addNewProduct"
               class="btn btn-primary"
               id="btn-addProduct"
               style="margin-left: 10px;"
               data-toggle="modal"
               onclick="addAssign()">Add New Product</a>

            <a href="/report/productReport"
               class="btn btn-primary"
               id="btn-exportExcelReport"
               style="margin-left: 20px;"
               data-toggle="modal">Export Excel Report</a>

            <a href="/report/goToImportExcelByResumeAble"
               class="btn btn-primary"
               id="btn-importExcelResumeAble"
               style="margin-left: 20px;"
               data-toggle="modal">Import Excel File By ResumeAble</a>

            <a href="/report/goToImportExcelByApachePoi"
               class="btn btn-primary"
               id="btn-importExcelApachePoi"
               style="margin-left: 20px;"
               data-toggle="modal">Import Excel File By Apache POI</a>
        </div>
    </div>

    <div class="row" style="margin-left: 50px">
        <div class="col-md-11">
            <div class="box box-default">

                <table id="todotable" class="table table-striped">
                    <thead>
                    <tr>
                        <th>SeqPro</th>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>price</th>
                        <th>image Product</th>
                        <th>numberSales</th>
                        <th>guarantee</th>
                        <th>sysStatus</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- Start modal Update -->
<div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- không để javascript:void chết ngay nó đưa về trang đéo nào ấy đòi username string..-->
        <form method="post" action="javascript:void(0);">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header col-md-12">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body col-md-12">
                    <div class="form-group col-md-12">
                        <label class="col-md-4">Mã sản phẩm</label>
                        <div class="col-md-8">
                            <input id="seqPro"
                                   name="seqPro"
                                   type="text"
                                   readonly="readonly"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="col-md-4">Tên product</label>
                        <div class="col-md-8">
                            <input id="productName" name="productName" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="col-md-4">Trạng thái</label>
                        <div class="col-md-8">
                            <select id="sysStatus" class="form-control">
                                <option value="1">Áp dụng</option>
                                <option value="0">Ngừng áp dụng</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="col-md-4 col-md-offset-0">
                        <button type="submit" id="update-product" class="btn btn-primary">Lưu</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End modal Update -->

<!-- start modal Delete-->
<div class="modal fade" id="myModalXoa" role="dialog"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close dongmodal" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p>Bạn có muốn xóa ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="okXoaModal"
                        data-dismiss="modal">Đồng ý
                </button>
                <button type="button" class="btn btn-default dongmodal"
                        data-dismiss="modal">Đóng
                </button>
            </div>
        </div>
    </div>
</div>
<!-- end modal Delete-->

<!-- Modal thông báo message delete-->
<div class="modal fade" id="notifiModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close dongmodal" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p id="massagemodal"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default dongmodal" data-dismiss="modal">Đóng </button>
            </div>
        </div>
    </div>
</div>
<!-- endmodal -->


<script type="text/javascript">
    $(document).ready(function () {
        $('#todotable').DataTable({
            "sAjaxSource": "/product/loadAllProduct",
            "sAjaxDataProp": "",
            "order": [[0, "asc"]],
            "search": true, //mo che do search của Jquery auto
            "columns": [{ //cột nào hiển thị lên datatable mà data = null trong DB là chết ngay
                data: "seqPro"
            }, {
                data: "productName"
            }, {
                data: "categoryName"
            }, {
                data: "price"
            }, {
                data: "imageProduct"
            }, {
                data: "numberSales"
            }, {
                data: "guarantee"  // null la die
            }, {
                data: "sysStatus",
                render: renderSysStatus
                //render tao method rieng cho data nay
            }, {
                data: "seqPro",
                name: "seqPro",
                title: "Action",
                render: renderAction,
                sortable: false
            }]
        });

    });

    function renderSysStatus(data, type, row) {
        if (row.sysStatus === 1) {
            return "Áp dụng";
        } else {
            return "Ngừng áp dụng";
        }
    }

    //render tao action cho button datatable
    function renderAction(data, type, row) {
        return '<button class="btn btn-info btn-sm" onclick="updateAssign(' + row.seqPro + ')">Sửa</button>&nbsp;'
            + '<button class="btnXoa btn btn-danger btn-sm" onclick="deleteAssign(\'' + row.seqPro + '\')">Xóa</button>&nbsp;' +
            '<button class="btn btn-info btn-sm" id="viewDetailPro" onclick="ViewDetail(' + row.seqPro + ')">View Detail</button>'
    }

    function addAssign() {
        //add che do submit cho the a href da co link
        $('#btn-addProduct').submit();
    }

    //Start update
    var idSua;

    function updateAssign(id) {
        idSua = id;
        var myModal = $("#myModal");
        myModal.addClass("fade");
        myModal.modal("toggle");
        $("#myModal  .modal-title").html("Edit Product");
        CommonAjax.get(null, "/product/findProductBySeq/" + idSua, setInfor, null);
        //formValidator.settings.submitHandler = updateHandler;
    }

    var productObject;

    function setInfor(data) {
        productObject = data;
        $("#seqPro").val(data.seqPro);
        $("#productName").val(data.productName);
        $("#sysStatus").val(data.sysStatus);
    }

    function ViewDetail(id) {
        idSua = id;
        //dua href xuong day moi tu convert dc chuoi cua tham so -< nhưng mà không view được data row khác
        //$("#viewDetailPro").prop("href","/product/updateProductBySeq/"+idSua);

        //dùng cách này mới view kick được data row thứ 2 - thay = button cho thẻ a
        //lúc này nó hiểu tham số idSua là dạng String
        window.location.href = "/product/updateProductBySeq/" + idSua;
    }

    $('#update-product').click(function () {
        updateHandler();
    });

    function updateHandler() {
        var seqProInput = $("#seqPro").val().trim();
        var productNameInput = $("#productName").val().trim();
        var sysStatusInput = $("#sysStatus").val().trim();

        var seqPro = seqProInput !== "" ? seqProInput : null;
        var productName = productNameInput !== "" ? productNameInput : null;
        var sysStatus = sysStatusInput !== "" ? sysStatusInput : null;
        var dataa = {
            "seqPro": seqPro,
            "productName": productName,
            "price": productObject.price,
            "numberSales": productObject.numberSales,
            "guarantee": productObject.guarantee,
            "imageProduct": productObject.imageProduct,
            "category": {
                "categoryId": productObject.categoryId
            },
            "sysStatus": sysStatus,
            "createDate": productObject.createDate,
            "updateDate": productObject.updateDate
        };
        CommonAjax.post(dataa, "/product/updateProduct", updateSuccess, updateError);
    }

    function updateSuccess(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-danger");
        modal_header.addClass("btn-success");
        notify.addClass("fade");
        $('#massagemodal').html(data.message);//add message vao message của popup
        notify.modal("toggle"); //hiển thị popup
        notify.on('hidden.bs.modal', function () {
            location.reload();
        });
    }

    function updateError(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-success");
        modal_header.addClass("btn-danger");
        notify.addClass("fade");
        $('#massagemodal').html(data.responseJSON.message);//chuyển đổi message sang dạng Json của thư viện jquery
        notify.modal("toggle");
        notify.on('hidden.bs.modal', function () {
            $(".modal-backdrop").remove();
            $("#notifiModal").removeAttr("style");
        });
    }

    //End Update

    // start Delete
    var deleteId;

    function deleteAssign(id) {
        deleteId = id;
        $('#myModalXoa').modal('toggle');
    }

    $('#okXoaModal').on('click', function () {
        //truyền tham số kiểu không thông qua requestBody
        CommonAjax.post(null, "/product/deleteProduct/" + deleteId, deleteSuccess, deleteError);

    });

    //success delete
    function deleteSuccess(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-danger");
        modal_header.addClass("btn-success");
        $('#massagemodal').html(data.message);
        notify.modal('toggle');
        notify.css({
            "display": "block"
        });
        notify.on('hidden.bs.modal', function () {
            location.reload();
        });
    }

    //error delete
    function deleteError(data) {
        var modal_header = $("#notifiModal .modal-header");
        var notify = $("#notifiModal");

        modal_header.removeClass("btn-success");
        modal_header.addClass("btn-danger");
        $('#massagemodal').html(data.responseJSON.message);
        notify.modal('show');
        notify.css({
            "display": "block"
        });
    }

    // end Delete
</script>
</body>

</html>