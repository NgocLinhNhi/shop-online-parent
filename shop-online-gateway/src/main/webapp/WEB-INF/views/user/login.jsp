<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <link rel="stylesheet" type="text/css" href="/bootstrap/js/bootstrap.min.css">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/jquerry/jquery.min.js"></script>
    <script src="/jquerry/jquery.validate.min.js"></script> <!-- for validate form -->
    <script src="/js/commonAjax.js"></script>
    <script src="/css/ajaxLoading.css"></script>
    <script src="/css/AdminLTE.min.css"></script>
    <link rel="stylesheet" type="text/css" href="/css/ErrorMessage.css">

    <title>Login Page</title>
</head>

<body>

<div class="container" style="margin-top: 180px;">
    <div class="col-sm-10"></div>
    <div class="col-md-3"></div>
    <div class="col-md-6">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Đăng nhập</h3>
            </div>

            <form class="form-horizontal" id="login-form" action="/user/login" method="GET">

                <div class="box-body">
                    <div class="form-group">
                        <label for="username" class="col-sm-3 control-label">Tên đăng nhập</label>

                        <div class="col-sm-9">
                            <input type="text"
                                   class="form-control"
                                   id="username"
                                   name="username"
                                   placeholder="Tên đăng nhập">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">Mật khẩu</label>

                        <div class="col-sm-9">
                            <input type="password"
                                   class="form-control"
                                   id="password"
                                   name="password"
                                   placeholder="Mật khẩu">
                        </div>
                    </div>

                    <label class="col-sm-3"></label>
                    <label class="col-sm-9"
                           id="err-ms"
                           style="color: red">${errorMessage}
                    </label>

                </div>
                <div class="box-footer">
                    <button name="submit" id="btn-login" class="btn btn-info pull-right">Login</button>
                    <div class="col-md-8"></div>
                    <a class="btn btn-primary" href="/goRegister">Register</a>
                </div>
            </form>
        </div>

    </div>
    <div class="col-md-3"></div>
</div>


<script type="text/javascript">
    var formValidator = $("#login-form").validate({
        rules: {
            username: {
                required: true,
                maxlength: 10
            },
            password: {
                required: true
            }
        },
        messages: {
            username: {
                required: "Vui lòng nhập username",
                maxlength: "Độ dài không vượt quá 10 kí tự"
            },
            password: {
                required: "Vui lòng nhập password"
            },
        },
    });


    $("#btn-login").click(function () {
        formValidator.settings.submitHandler;
    });

</script>
</body>
</html>