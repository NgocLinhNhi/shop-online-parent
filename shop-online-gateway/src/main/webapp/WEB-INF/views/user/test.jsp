<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">

<head>
    <title>SHOP SALES ONLINE </title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
</head>


<body>
<table>
    <thead>
        <tr>
            <th>Question</th>
            <th>Answer</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>“Know thyself” was a widely-known aphorism of which country?</td>
        <td>Ancient Greece</td>
    </tr>
    <tr>
        <td>“Man of Peace” is the nickname of which commander?</td>
        <td>Imhotep</td>
    </tr>
    <tr>
        <td>“One hundred forty thousand all disarmed! Among these was there not a single man?” Which Tang-dynasty poet
            wrote
            this couplet?
        </td>
        <td>Madame Huarui</td>
    </tr>
    <tr>
        <td>“Puncture Wounds” is the skill of which character in Ceroli Crisis?</td>
        <td>Astrid</td>
    </tr>
    <tr>
        <td>“The end justifies the means” was a key belief of the author of “The Prince”. What was his name?</td>
        <td>Machiavelli</td>
    </tr>
    <tr>
        <td>“Tongwancheng” was the capital of which ethnic group from ancient China?</td>
        <td>The Xiongnu</td>
    </tr>
    <tr>
        <td>A decrease in which of the following metrics might indicate increasing nearness to a state of vacuum?</td>
        <td>Air pressure</td>
    </tr>
    <tr>
        <td>A human fingernail is made of which material?</td>
        <td>Alpha-keratin</td>
    </tr>
    <tr>
        <td>According to the tale of the She-wolf in Roman mythology, what came of the twins she nurtured, Romulus and
            Remus?
        </td>
        <td>Romulus became the founder and first King of Rome</td>
    </tr>
    <tr>
        <td>Acquiring Commander William I unlocks which of these achievements?</td>
        <td>Path of Glory</td>
    </tr>
    <tr>
        <td>Advantages of Johannes Gutenberg’s printing press include all of the following except what?</td>
        <td>Increase in church power</td>
    </tr>
    <tr>
        <td>After a Venus Flytrap catches an insect, how does it digest and absorb the prey?</td>
        <td>The inner flaps of the leaves secrete a digestive fluid</td>
    </tr>
    <tr>
        <td>After its foundation in the 6th century BCE, which institution of the Roman Republic served as a
            consultative
            parliament?
        </td>
        <td>The Senate</td>
    </tr>
    <tr>
        <td>After the Lost Kingdom opens, what item do you have to spend to teleport to and from the Lost Kingdom?</td>
        <td>Random Teleport</td>
    </tr>
    <tr>
        <td>After the students at the Lyceum of Wisdom finish their lessons, what building in your city do they return
            to?
        </td>
        <td>Academy</td>
    </tr>
    <tr>
        <td>Afterglow is caused by:</td>
        <td>Light scattering in air</td>
    </tr>
    <tr>
        <td>Alliance help can NOT speed up which of the following activities?</td>
        <td>Troop training</td>
    </tr>
    <tr>
        <td>Apart from “Number of Scouts” and “Power”, which of the following Scout Camp parameters can also be
            upgraded?
        </td>
        <td>Scout March Speed and Exploration Range</td>
    </tr>
    <tr>
        <td>Apart from “Times You Can Be Helped” and “Power”, which of the following Alliance Center parameters can also
            be
            upgraded?
        </td>
        <td>Reinforcement Troop Capacity</td>
    </tr>
    <tr>
        <td>As of the 2018 World Cup, how many times has Shakira performed at the World Cup closing ceremony?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>As of the 2018 World Cup, how many World Cups has Brazil won?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>As of the 2018 World Cup, what is the United State’s best ever World Cup result?</td>
        <td>Third place</td>
    </tr>
    <tr>
        <td>As of the 2018 World Cup, which player holds the record for most goals scored in a single World Cup match
            with
            five goals?
        </td>
        <td>Oleg Salenko</td>
    </tr>
    <tr>
        <td>As of the 2018 World Cup, who holds the record for most goals scored in the World Cup?</td>
        <td>Miroslav Klose</td>
    </tr>
    <tr>
        <td>As of the 2018 World Cup, who holds the World Cup record for fastest goal scored at 11 seconds?</td>
        <td>Hakan Şükür</td>
    </tr>
    <tr>
        <td>As of the 2022 World Cup, how many countries have won the World Cup?</td>
        <td>8</td>
    </tr>
    <tr>
        <td>As of the 21st World Cup, who is the only player to have scored a hat-trick in 2 different World Cups?</td>
        <td>Gabriel Batistuta</td>
    </tr>
    <tr>
        <td>Ashoka the Great was the King of which Ancient Indian kingdom?</td>
        <td>Maurya</td>
    </tr>
    <tr>
        <td>Assuming the absence of any other special buffs, which of these commanders has the highest march speed?</td>
        <td>Belisarius</td>
    </tr>
    <tr>
        <td>At what VIP level can you purchase a VIP Special chest that contains enough Hannibal Barca sculptures to
            awaken
            him?
        </td>
        <td>14</td>
    </tr>
    <tr>
        <td>At which VIP level do you permanently unlock a second building queue?</td>
        <td>Lvl. 6</td>
    </tr>
    <tr>
        <td>Black Forest Cake is a famous European dessert. In which country is the Black Forest from which it takes its
            name?
        </td>
        <td>Germany</td>
    </tr>
    <tr>
        <td>Bossa nova is a popular genre of music originating from which country?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>Broccoli is rich in which vitamin?</td>
        <td>Vitamin C</td>
    </tr>
    <tr>
        <td>By what other name are Ceroli Chieftains Ak &amp; Hok also known by?</td>
        <td>The Astral Twins</td>
    </tr>
    <tr>
        <td>By what other name is October also known under Japan’s old calendar?</td>
        <td>Kannazuki</td>
    </tr>
    <tr>
        <td>By what other name is the Ceroli Chieftain Wafura also known?</td>
        <td>Wolfshadow</td>
    </tr>
    <tr>
        <td>Cactus spines are actually a modified form of which other part of the plant?</td>
        <td>Leaves</td>
    </tr>
    <tr>
        <td>Champagne is a region in which country?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Charles Macintosh crafted a very early version of a raincoat from which material?</td>
        <td>Rubber</td>
    </tr>
    <tr>
        <td>China’s “crosstalk” sketch comedy format originates in three places: Tianqiao in Beijing, the Quanye Bazaar
            in
            Tianjin, and where in Nanjing?
        </td>
        <td>Fuzimiao</td>
    </tr>
    <tr>
        <td>Combat Tactics is an important Military Technology – what effect does it have?</td>
        <td>Increases Attack of all units</td>
    </tr>
    <tr>
        <td>Commander Björn Ironside’s “Viking Lord” skill benefits which of the following unit types?</td>
        <td>Infantry</td>
    </tr>
    <tr>
        <td>Darwin’s last book primarily discussed which species’ influence on soil?</td>
        <td>Earthworms</td>
    </tr>
    <tr>
        <td>Diego Velázquez, the painter of the realist work Portrait of Pope Innocent X, was from which country?</td>
        <td>Spain</td>
    </tr>
    <tr>
        <td>During China’s Warring States Period, which state defeated the other six and built the unified Chinese
            empire?
        </td>
        <td>Qin</td>
    </tr>
    <tr>
        <td>During the Renaissance, why was Florence significant?</td>
        <td>Birthplace and symbol of the Renaissance</td>
    </tr>
    <tr>
        <td>During the Second Punic War, in which battle did a vastly outnumbered Hannibal defeat a Roman army?</td>
        <td>The Battle of Cannae</td>
    </tr>
    <tr>
        <td>During the Trojan War, which Greek warrior killed Hector?</td>
        <td>Achilles</td>
    </tr>
    <tr>
        <td>During the Upper and Lower Egypt period, what color was the crown of the pharaoh of Upper Egypt?</td>
        <td>White</td>
    </tr>

    <tr>
        <td>During which emperor’s reign was the Roman Colosseum completed?</td>
        <td>Titus</td>
    </tr>
    <tr>
        <td>During which season are nights relatively short and days relatively long?</td>
        <td>Summer</td>
    </tr>
    <tr>
        <td>Edward VIII became King of England in January 1936. Later in the same year, in which month did he give up
            the
            throne?
        </td>
        <td>December</td>
    </tr>
    <tr>
        <td>Egyptians made paper from which material?</td>
        <td>Papyrus</td>
    </tr>
    <tr>
        <td>Egypt’s economy was primarily based on what?</td>
        <td>Agriculture</td>
    </tr>
    <tr>
        <td>Elbe Day was the meeting of which two armies during World War II?</td>
        <td>USA, USSR</td>
    </tr>
    <tr>
        <td>Equipment with special talent bonuses will have their attributes enhanced by what percentage when equipped
            to a
            commander with the corresponding talent?
        </td>
        <td>30%</td>
    </tr>
    <tr>
        <td>Feijoada is considered a national dish of which country?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>First created in Ancient Greece, to which profession does the Hippocratic Oath apply?</td>
        <td>Doctors</td>
    </tr>
    <tr>
        <td>Fish and chips has its earliest origins in which country?</td>
        <td>England</td>
    </tr>
    <tr>
        <td>Focaccia is a type of pizza-like bread. Which country (and its cooking culture) created the focaccia?</td>
        <td>Italy</td>
    </tr>
    <tr>
        <td>Foie gras is a very nutrient-rich food. Which country produces the most foie gras?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>For how many francs did Charles V ransom Bertrand du Guesclin during the conflict over Brittany?</td>
        <td>100,000</td>
    </tr>
    <tr>
        <td>For which disease did Edward Jenner, known as the father of immunology, develop a vaccine?</td>
        <td>Smallpox</td>
    </tr>
    <tr>
        <td>France and Germany were both once part of what feudal Kingdom?</td>
        <td>Francia</td>
    </tr>
    <tr>
        <td>French National Day is on July 14 in order to coincide with which historical event?</td>
        <td>Storming of the Bastille</td>
    </tr>
    <tr>
        <td>From the 17th to 19th centuries, the Kingdom of Prussia was mostly in which modern country?</td>
        <td>Germany</td>
    </tr>
    <tr>
        <td>From the 8th to the 11th century CE, what did Europeans frequently call the Scandinavian invaders who
            frequently
            ravaged their lands?
        </td>
        <td>Vikings</td>
    </tr>
    <tr>
        <td>Galileo’s Leaning Tower of Pisa experiment overturned a theory of which Ancient Greek scientist?</td>
        <td>Aristotle</td>
    </tr>
    <tr>
        <td>Governors cannot earn Activity Points through which of the following?</td>
        <td>Expeditions</td>
    </tr>
    <tr>
        <td>Halley’s Comet can be observed from Earth every…</td>
        <td>75-76 years</td>
    </tr>
    <tr>
        <td>Held in 1929, how long did the first Academy Awards (“Oscars”) ceremony take?</td>
        <td>15 minutes</td>
    </tr>
    <tr>
        <td>Hieroglyphs were part of which country’s formal writing system?</td>
        <td>Ancient Egypt</td>
    </tr>
    <tr>
        <td>Homer’s epic “The Iliad” focuses on events happening during which war or battle?</td>
        <td>The Trojan War</td>
    </tr>
    <tr>
        <td>How do you get Alliance Technology Credits?</td>
        <td>By donating to research Alliance Technology</td>
    </tr>
    <tr>
        <td>How do you get Alliance Technology Points?</td>
        <td>By donating to research Alliance Technology</td>
    </tr>
    <tr>
        <td>How do you obtain the Friend of the Kingdom achievement?</td>
        <td>Visit 100 tribal villages</td>
    </tr>
    <tr>
        <td>How high in the Sunset Canyon daily ranking do you need to place before you can receive the Golden Key Daily
            Rewards?
        </td>
        <td>In the top 5</td>
    </tr>
    <tr>
        <td>How long did the Hundred Years’ War between England and France last?</td>
        <td>116 years</td>
    </tr>
    <tr>
        <td>How long does the “Basic Army Expansion” boost last?</td>
        <td>4h</td>
    </tr>
    <tr>
        <td>How long does the rune dropped by defeating the Guardians remain on the map?</td>
        <td>8 hours</td>
    </tr>
    <tr>
        <td>How many “Golden Kingdom Equipment Troves” are contained within the Golden Chest earned from stage 16 of the
            “Golden Kingdom” event?
        </td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many arrows are stuck in the shield of commander Lohar?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many beasts can you expect to see fighting alongside Lvl 20 barbarian troops in Rise of Kingdoms</td>
        <td>1 bear and 2 wolves</td>
    </tr>
    <tr>
        <td>How many bones does an adult human normally have?</td>
        <td>206</td>
    </tr>
    <tr>
        <td>How many bowmen are shown practising in the Archery Range when training composite bowmen?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many cards in total are shown in the Mysterious Merchant’s hands?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>How many daily Activity Points do you need in “Lucerne Scrolls” to claim the Daily Gift clue reward?</td>
        <td>50</td>
    </tr>
    <tr>
        <td>How many Easter eggs are featured in the Easter Egg Avatar Frame?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>How many emeralds are featured on the clothing of commander Matilda of Flanders?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>How many flags are featured in front of the Alliance Center for the French civilization?</td>
        <td>6</td>
    </tr>
    <tr>
        <td>How many gears are shown on the icon for the Economic Technology “Engineering”?</td>
        <td>2</td>
    </tr>
    <tr>
        <td>How many goals does a player need to score to earn a hat-trick?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many Golden Keys are awarded to the top finishing Governor in each season of Sunset Canyon?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>How many hurricanes were recorded around the Pacific Ocean in the year 1977?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many individual credits can be earned from selling normal equipment blueprints through the Alliance
            Shop’s
            reclaim feature?
        </td>
        <td>400</td>
    </tr>
    <tr>
        <td>How many individual quests does an Expedition involve?</td>
        <td>90</td>
    </tr>
    <tr>
        <td>How many items of the same quality should I equip to complete “Ready For Battle”?</td>
        <td>6</td>
    </tr>
    <tr>
        <td>How many Key Points are required to unlock the Treasure of Blue Crystal in Alliance Gifts?</td>
        <td>3,000,000</td>
    </tr>
    <tr>
        <td>How many keys are included in the Alliance Gift received for the destruction of a Lvl 5 Barbarian Fort?</td>
        <td>60</td>
    </tr>
    <tr>
        <td>How many keys are included in the Wooden Chest Alliance Gift?</td>
        <td>10</td>
    </tr>
    <tr>
        <td>How many keys does a typical modern piano have?</td>
        <td>88</td>
    </tr>
    <tr>
        <td>How many Legendary Commander Sculptures are awarded to the top finisher by total rankings in the “The
            Mightiest
            Governor” event?
        </td>
        <td>180</td>
    </tr>
    <tr>
        <td>How many letters are used in today’s version of English?</td>
        <td>26</td>
    </tr>
    <tr>
        <td>How many Medals of the Conqueror are needed to purchase a Legendary Commander Sculpture in the Medal Store
            in
            Expedition mode?
        </td>
        <td>1,500</td>
    </tr>
    <tr>
        <td>How many minutes’ worth total of speedups can be obtained from purchasing the “7-Day Speedup Supply” in
            addition
            to the immediate 1,050 Gem reward?
        </td>
        <td>3,150 minutes</td>
    </tr>
    <tr>
        <td>How many Normal Iron Ore do you need to make an Advanced Iron Ore?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many officers (excluding the alliance leader) can an alliance have?</td>
        <td>8</td>
    </tr>
    <tr>
        <td>How many Olympic gold medals did African-American athlete Jesse Owens win in 1936?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many patrolling soldiers roam a governor’s walls?</td>
        <td>7</td>
    </tr>
    <tr>
        <td>How many phases are there in The Mightiest Governor&nbsp;event?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>How many points are earned per 1 point of upgraded Power within the “Stage 4: Power Upgrade” segment of “The
            Mightiest Governor” event?
        </td>
        <td>2</td>
    </tr>
    <tr>
        <td>How many points are gained per Lvl 3 unit severely wounded/killed in the “Stage 5: Enemy Elimination”
            segment of
            the “Mightiest Governor” event?
        </td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many polar bears feature on the Heart of the North Avatar Frame?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many refreshes are available per visit to the Mysterious Merchant?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>How many reward chests are awarded for reaching 60 daily activity points?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many roses feature on the Rose Petals Avatar Frame?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many Scouts can the Scout Camp hold at once?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many sections are featured in the Kingdom Newspaper outside of special calendar events?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>How many separate khanates or empires had the Mongol Empire fractured into by the time of Kublai’s death in
            1294?
        </td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many small tents can you see in a Lvl 3 Barbarian Fort?</td>
        <td>6</td>
    </tr>
    <tr>
        <td>How many stars are featured on the banner displayed behind the commander Minamoto no Yoshitsune?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many students sit in on the lectures held by the scholars of the Lyceum of Wisdom?</td>
        <td>6</td>
    </tr>
    <tr>
        <td>How many teams competed in the 2022 World Cup?</td>
        <td>32</td>
    </tr>
    <tr>
        <td>How many teams took part in the first World Cup?</td>
        <td>13</td>
    </tr>
    <tr>
        <td>How many tents are shown on the icon for the Military Technology “Encampment”?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many tiles does a City Hall occupy within your city?</td>
        <td>9 x 9</td>
    </tr>
    <tr>
        <td>How many times can you receive alliance help if your Alliance Center is level 14?</td>
        <td>18</td>
    </tr>
    <tr>
        <td>How many times can you receive alliance help if your alliance center is level 25?</td>
        <td>30</td>
    </tr>
    <tr>
        <td>How many times has North Korea qualified for the World Cup?</td>
        <td>Two times</td>
    </tr>
    <tr>
        <td>How many times is the character “1” used when you write from number 1 to number 99?</td>
        <td>20</td>
    </tr>
    <tr>
        <td>How many troops are recommended for destroying a Lvl 3 Barbarian Fort?</td>
        <td>480,000 Lvl 3 units</td>
    </tr>
    <tr>
        <td>How many troops can governors send with a level 17 City Hall or above?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many VIP points can you buy with 200 gems?</td>
        <td>200</td>
    </tr>
    <tr>
        <td>How many Watchtowers are included in a Lvl 25 city?</td>
        <td>8</td>
    </tr>
    <tr>
        <td>How many wheels are there on a battering ram?</td>
        <td>6</td>
    </tr>
    <tr>
        <td>How many Women’s World Cups has Megan Rapinoe won?</td>
        <td>2</td>
    </tr>
    <tr>
        <td>How many World Cup titles has Italy won?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>How many World Cup titles has Pelé won?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>How many years are there between each Winter Olympic Games?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>How much capacity does a Lvl 15 Hospital provide (excluding boosts)?</td>
        <td>29,000</td>
    </tr>
    <tr>
        <td>How much does the 12-Hour Enhanced Attack item increase your troops’ Attack by?</td>
        <td>5%</td>
    </tr>
    <tr>
        <td>How much experience does a commander earn from a Lvl 7 Tome of Knowledge?</td>
        <td>50,000</td>
    </tr>
    <tr>
        <td>How much gold does a single issue of the Kingdom Newspaper cost?</td>
        <td>1,000</td>
    </tr>
    <tr>
        <td>How much reinforcement troop capacity does a city receive at Alliance Center Lvl 20?</td>
        <td>575,000</td>
    </tr>
    <tr>
        <td>How much stone will a Lvl 17 Quarry produce every hour (excluding boosts)?</td>
        <td>2,775</td>
    </tr>
    <tr>
        <td>How much total daily contribution is required to place in the daily alliance contribution rankings?</td>
        <td>10,000</td>
    </tr>
    <tr>
        <td>How much will an Enhanced Attack (Advanced) item increase your attack?</td>
        <td>10%</td>
    </tr>
    <tr>
        <td>How much will an Enhanced Gathering item increase your gathering speed?</td>
        <td>50%</td>
    </tr>
    <tr>
        <td>How often does the World Cup take place?</td>
        <td>Every 4 years</td>
    </tr>
    <tr>
        <td>How often is the FIFA World Cup held?</td>
        <td>Every 4 years</td>
    </tr>
    <tr>
        <td>How was the Gutenberg Bible put into mass production?</td>
        <td>Movable type printing press</td>
    </tr>
    <tr>
        <td>If a man with type AB blood and a woman with type B blood have a baby, which of the following statements
            would
            be incorrect?
        </td>
        <td>It could have type O blood</td>
    </tr>
    <tr>
        <td>In 1970, which goalkeeper made the “save of the century” against Pelé?</td>
        <td>Gordon Banks</td>
    </tr>
    <tr>
        <td>In 2002, FIFA published a fan-selected World Cup Dream Team. Who was the goalkeeper?</td>
        <td>Lev Yashin</td>
    </tr>
    <tr>
        <td>In 2010, at age 71, Otto Rehhagel became the oldest person to manage a World Cup team. Which team did he
            manage?
        </td>
        <td>Greece</td>
    </tr>
    <tr>
        <td>In 2018, which country set the record for being the least-populated country to ever reach the World Cup?
        </td>
        <td>Iceland</td>
    </tr>
    <tr>
        <td>In 2026, the US, Mexico, and Canada will co-host the World Cup. How many teams will be participating?</td>
        <td>48</td>
    </tr>
    <tr>
        <td>In Ancient Egypt, which of the following was NOT a privilege of being a ‘Scribe’?</td>
        <td>Exempt from paying medical expenses if sick</td>
    </tr>
    <tr>
        <td>In ancient Greece, the Delian League was led by which city-state?</td>
        <td>Athens</td>
    </tr>
    <tr>
        <td>In Ancient Greece, which philosopher liked to discuss ideas on a street which then got the nickname
            “Peripatetic
            School”?
        </td>
        <td>Aristotle</td>
    </tr>
    <tr>
        <td>In ancient Greek mythology, who was Hermes’ father?</td>
        <td>Zeus</td>
    </tr>
    <tr>
        <td>In ancient Greek mythology, who was the wife of Hades, the king of the underworld?</td>
        <td>Persephone</td>
    </tr>
    <tr>
        <td>In Ark of Osiris, how many teleports does the first alliance to occupy an obelisk earn?</td>
        <td>8</td>
    </tr>
    <tr>
        <td>In Ark of Osiris, what is the requirement for an alliance to participate in the Golden Battlefield?</td>
        <td>Ranked top 20 for alliance power in their kingdom</td>
    </tr>
    <tr>
        <td>In Ark of Osiris, which Speedup can you use?</td>
        <td>Healing Speedup</td>
    </tr>
    <tr>
        <td>In Ark of Osiris, where are you supposed to take the Ark?</td>
        <td>A non-enemy building</td>
    </tr>
    <tr>
        <td>In China’s Han Dynasty, who opened the Silk Road on the orders of Emperor Wu?</td>
        <td>Zhang Qian</td>
    </tr>
    <tr>
        <td>In Chinese mythology, what kind of creature is Jingwei?</td>
        <td>A bird</td>
    </tr>
    <tr>
        <td>In DC comics, what is Batman’s real name?</td>
        <td>Bruce Wayne</td>
    </tr>
    <tr>
        <td>In French novelist Stendhal’s The Red and the Black, what is the name of the ambitious protagonist?</td>
        <td>Julien</td>
    </tr>
    <tr>
        <td>In Golden Kingdom, how many passes do you need to clear to get a Golden Chest?</td>
        <td>4 Passes</td>
    </tr>
    <tr>
        <td>In Greek mythology, to whom did Paris, the Prince of Troy, give a golden apple inscribed with, “To the most
            beautiful goddess”?
        </td>
        <td>Aphrodite</td>
    </tr>
    <tr>
        <td>In Greek mythology, what is the Goddess Eos known as?</td>
        <td>Goddess of the Dawn</td>
    </tr>
    <tr>
        <td>In Greek mythology, what is the relationship between Zeus and Hermes?</td>
        <td>Father and son</td>
    </tr>
    <tr>
        <td>In Greek mythology, what was Achilles’ only weak point?</td>
        <td>His heels</td>
    </tr>
    <tr>
        <td>In Greek mythology, what was the name of Hades’ 3-headed dog which guarded the gates of the Underworld?</td>
        <td>Cerberus</td>
    </tr>
    <tr>
        <td>In Greek mythology, which hero quested for the Golden Fleece?</td>
        <td>Jason</td>
    </tr>
    <tr>
        <td>In Greek mythology, who ran the chariot of the sun off its path, setting the earth aflame?</td>
        <td>Phaethon</td>
    </tr>
    <tr>
        <td>In Morse code, what is the length in dots between two words?</td>
        <td>7 dots</td>
    </tr>
    <tr>
        <td>In most cases, which kind of lion has a mane?</td>
        <td>Adult male</td>
    </tr>
    <tr>
        <td>In mythology, who was hit by the lead arrow of Eros?</td>
        <td>Daphne</td>
    </tr>
    <tr>
        <td>In Norse mythology, what variety of trees was the World Tree?</td>
        <td>Ash</td>
    </tr>
    <tr>
        <td>In Norse mythology, who does the hammer Mjolnir belong to?</td>
        <td>Thor</td>
    </tr>
    <tr>
        <td>In Norse mythology, who wielded the weapon Gungnir?</td>
        <td>Odin</td>
    </tr>
    <tr>
        <td>In RoK, the Cataphract is which civilization’s special unit?</td>
        <td>Byzantium</td>
    </tr>
    <tr>
        <td>In RoK, the Hwarang is which civilization’s special unit?</td>
        <td>Korea</td>
    </tr>
    <tr>
        <td>In RoK, the Janissary is which civilization’s special unit?</td>
        <td>Ottoman Empire</td>
    </tr>
    <tr>
        <td>In RoK, the Throwing Axeman is which civilization’s special unit?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>In RoK, which building can you use to view your kingdom’s history?</td>
        <td>Monument</td>
    </tr>
    <tr>
        <td>In RoK, which of the following is considered a Boost item?</td>
        <td>Peace Shield</td>
    </tr>
    <tr>
        <td>In RoK, which of these commanders is NOT good at leading archer units?</td>
        <td>Saladin</td>
    </tr>
    <tr>
        <td>In Russian history, what nickname was the first Tzar given for their tyranny?</td>
        <td>Ivan the Terrible</td>
    </tr>
    <tr>
        <td>In soccer, which position is allowed to touch the ball with their hands while it is in their team’s penalty
            area?
        </td>
        <td>Goalkeeper</td>
    </tr>
    <tr>
        <td>In Sunset Canyon, how many troops can you deploy at a time?</td>
        <td>5</td>
    </tr>
    <tr>
        <td>In the 1966 World Cup, England manager Alf Ramsey began using a unique formation that led England to the
            World
            Cup title. What was notable about this formation?
        </td>
        <td>No wingers</td>
    </tr>
    <tr>
        <td>In the 2006 World Cup, Switzerland did not concede a single goal in regular play. Who was their
            goalkeeper?
        </td>
        <td>Pascal Zuberbühler</td>
    </tr>
    <tr>
        <td>In the 2006 World Cup, who was the only English player to score during penalties in England’s quarter-final
            match against Portugal?
        </td>
        <td>Owen Hargreaves</td>
    </tr>
    <tr>
        <td>In the 2010 World Cup Final between Spain and the Netherlands, which Spanish player scored the game-winning
            goal?
        </td>
        <td>Andrés Iniesta</td>
    </tr>
    <tr>
        <td>In the 2010 World Cup, which goalkeeper saved two penalty kicks during regular play?</td>
        <td>Brad Friedel</td>
    </tr>
    <tr>
        <td>In the 2014 World Cup Final between Germany and Argentina, which German player scored the game-winning goal?
        </td>
        <td>Mario Götze</td>
    </tr>
    <tr>
        <td>In the 2014 World Cup held in Brazil, how many cities held match venues?</td>
        <td>12</td>
    </tr>
    <tr>
        <td>In the 2014 World Cup held in Brazil, how many Confederation of African Football teams qualified for the
            tournament?
        </td>
        <td>5</td>
    </tr>
    <tr>
        <td>In the 2014 World Cup, which of the following teams was not in the same group as the eventual champions
            Germany?
        </td>
        <td>England</td>
    </tr>
    <tr>
        <td>In the 2018 World Cup, Thibaut Courtois won the Golden Glove award for best goalkeeper. What country is he
            from?
        </td>
        <td>Belgium</td>
    </tr>
    <tr>
        <td>In the 2018 World Cup, what other countries were in England’s Group G?</td>
        <td>Belgium, Panama, Tunisia</td>
    </tr>
    <tr>
        <td>In the 2018 World Cup, which of the following teams was not in the same group as the eventual champions
            France?
        </td>
        <td>Germany</td>
    </tr>
    <tr>
        <td>In the 2022 World Cup, how many teams made it through the group stage to the knockout stage?</td>
        <td>16</td>
    </tr>
    <tr>
        <td>In the 2022 World Cup, which two teams stayed in university accommodations?</td>
        <td>Argentina, Spain</td>
    </tr>
    <tr>
        <td>In the 2022 World Cup, who faced the host country Qatar in the opening match?</td>
        <td>Ecuador</td>
    </tr>
    <tr>
        <td>In the 2022 World Cup, who was the first team to be eliminated?</td>
        <td>Qatar</td>
    </tr>
    <tr>
        <td>In the Book of Exodus, who leads the Jewish people to the Promised Land?</td>
        <td>Moses</td>
    </tr>
    <tr>
        <td>In the COMMANDER VIEW page, what buff do commanders acting as a “Bloodfist Guard” get?</td>
        <td>Troop Health +1%</td>
    </tr>
    <tr>
        <td>In the COMMANDER VIEW page, what buff do commanders acting as a “Drillmaster” get?</td>
        <td>Troop Attack +1%</td>
    </tr>
    <tr>
        <td>In the COMMANDER VIEW page, what buff do commanders acting as a “Iron Guard” get?</td>
        <td>Troop Health +1%</td>
    </tr>
    <tr>
        <td>In the COMMANDER VIEW page, what buff do commanders acting as a “Ranger” get?</td>
        <td>None of these answers are correct</td>
    </tr>
    <tr>
        <td>In the COMMANDER VIEW page, what buff do commanders acting as a “Tax Officer” get?</td>
        <td>Gathering Speed +5%</td>
    </tr>
    <tr>
        <td>In the competition for the Golden Boot, if two players have scored the same number of goals, how is the
            winner
            determined?
        </td>
        <td>Assists</td>
    </tr>
    <tr>
        <td>In the Desire of Victory event, how can players gain rewards?</td>
        <td>Training troops</td>
    </tr>
    <tr>
        <td>In the Lohar’s Trial event, how can players obtain Bone Necklaces?</td>
        <td>Defeating ordinary Barbarians</td>
    </tr>
    <tr>
        <td>In the simplified Maslow’s hierarchy of needs, which need is at the top of the pyramid?</td>
        <td>Self-actualization</td>
    </tr>
    <tr>
        <td>In total, how many goals were scored at the 2018 World Cup?</td>
        <td>169</td>
    </tr>
    <tr>
        <td>In total, how many matches were played at the 2018 World Cup?</td>
        <td>64</td>
    </tr>
    <tr>
        <td>In what century was a settlement established near the mouth of the Amstel, which would eventually become the
            city of Amsterdam?
        </td>
        <td>12th century</td>
    </tr>
    <tr>
        <td>In what city did Henri I de Bourbon, Prince of Condé, pass away?</td>
        <td>Saint-Jean-d’Angély</td>
    </tr>
    <tr>
        <td>In what climate does basil, known in Europe as the “King of Herbs”, largely grow?</td>
        <td>Tropical areas</td>
    </tr>
    <tr>
        <td>In what country did the “Battle of Marengo” take place?</td>
        <td>Italy</td>
    </tr>
    <tr>
        <td>In what era of Chinese history did the “Battle of Fei River” take place where a small number of soldiers
            conquered a vastly superior force?
        </td>
        <td>The Eastern Jin Dynasty</td>
    </tr>
    <tr>
        <td>In what year did Einstein publish his four Annus Mirabilis (“Miracle Papers”)?</td>
        <td>1905</td>
    </tr>
    <tr>
        <td>In what year did Maradona score his “Hand of God” goal?</td>
        <td>1986</td>
    </tr>
    <tr>
        <td>In what year did Mark Zuckerberg create Facebook?</td>
        <td>2004</td>
    </tr>
    <tr>
        <td>In what year did Qatar host the World Cup?</td>
        <td>2022</td>
    </tr>
    <tr>
        <td>In what year did the current World Cup trophy come into use?</td>
        <td>1974</td>
    </tr>
    <tr>
        <td>In what year did the Late Show with David Letterman first premiere?</td>
        <td>1993</td>
    </tr>
    <tr>
        <td>In what year was FIFA founded?</td>
        <td>1904</td>
    </tr>
    <tr>
        <td>In what year was the first Women’s World Cup held?</td>
        <td>1991</td>
    </tr>
    <tr>
        <td>In what year was the first World Cup held?</td>
        <td>1930</td>
    </tr>
    <tr>
        <td>In which century did the classic English dessert sticky toffee pudding originate?</td>
        <td>The mid-20th century</td>
    </tr>
    <tr>
        <td>In which city in the United States has McDonald’s established a Hamburger University?</td>
        <td>Chicago</td>
    </tr>
    <tr>
        <td>In which city is Denmark’s statue of The Little Mermaid located?</td>
        <td>Copenhagen</td>
    </tr>
    <tr>
        <td>In which city is FIFA headquartered?</td>
        <td>Zurich</td>
    </tr>
    <tr>
        <td>In which city is the Obelisk of Thutmose III currently held?</td>
        <td>Istanbul</td>
    </tr>
    <tr>
        <td>In which city was the world’s first international soccer match held?</td>
        <td>Glasgow</td>
    </tr>
    <tr>
        <td>In which commander’s background picture can you see a chicken?</td>
        <td>Queen Tamar of Georgia</td>
    </tr>
    <tr>
        <td>In which commander’s background picture can you see a sakura tree?</td>
        <td>Minamoto no Yoshitsune</td>
    </tr>
    <tr>
        <td>In which country did assam tea, known for its rich aromas, originate?</td>
        <td>India</td>
    </tr>
    <tr>
        <td>In which country did the “Olé, Olé, Olé” chant originate?</td>
        <td>Spain</td>
    </tr>
    <tr>
        <td>In which country did the Carlist Wars take place during the 19th century?</td>
        <td>Spain</td>
    </tr>
    <tr>
        <td>In which country is the Lighthouse of Alexandria, one of the Seven Wonders of the Ancient World, located?
        </td>
        <td>Egypt</td>
    </tr>
    <tr>
        <td>In which country is the mysterious Stonehenge located?</td>
        <td>United Kingdom</td>
    </tr>
    <tr>
        <td>In which country was air conditioning invented?</td>
        <td>USA</td>
    </tr>
    <tr>
        <td>In which country was Botticelli, painter of The Birth of Venus, born?</td>
        <td>Italy</td>
    </tr>
    <tr>
        <td>In which country was Carlsberg Group, a leading brewer, founded in 1847?</td>
        <td>Denmark</td>
    </tr>
    <tr>
        <td>In which country was mozzarella cheese first produced, broadly associated with Italian cooking and pizzas?
        </td>
        <td>Italy</td>
    </tr>
    <tr>
        <td>In which country was the 20th World Cup held?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>In which country was the famed revolutionary and military strategist Simón Bolívar born?</td>
        <td>Venezuela</td>
    </tr>
    <tr>
        <td>In which country was the microscope invented?</td>
        <td>The Netherlands</td>
    </tr>
    <tr>
        <td>In which country was the Somme, the most infamous battlefield in World War I, located?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>In which country were the Wars of the Roses fought?</td>
        <td>England</td>
    </tr>
    <tr>
        <td>In which country were instant noodles first invented?</td>
        <td>Japan</td>
    </tr>
    <tr>
        <td>In which English palace did the “Star Chamber” sit?</td>
        <td>Palace of Westminster</td>
    </tr>
    <tr>
        <td>In which English stadium was the 1966 World Cup Final held?</td>
        <td>Wembley Stadium</td>
    </tr>
    <tr>
        <td>In which era of French history was the Arc de Triomphe built?</td>
        <td>Napoleonic era</td>
    </tr>
    <tr>
        <td>In which era was the very first pyramid of ancient Egypt constructed?</td>
        <td>The Third Dynasty of Egypt</td>
    </tr>
    <tr>
        <td>In which gallery is the Mona Lisa currently held?</td>
        <td>The Louvre</td>
    </tr>
    <tr>
        <td>In which of these campaigns did Thutmose III take part?</td>
        <td>The Battle of Megiddo</td>
    </tr>
    <tr>
        <td>In which region did the popular health food hummus originate?</td>
        <td>Middle East</td>
    </tr>
    <tr>
        <td>In which region was the empire of Srivijaya located?</td>
        <td>Southeast Asia</td>
    </tr>
    <tr>
        <td>In which theater was Beethoven’s Symphony No. 5 in C Minor debuted in 1808?</td>
        <td>The Theater an der Wien</td>
    </tr>
    <tr>
        <td>In which two years was the World Cup suspended due to World War II?</td>
        <td>1942 &amp; 1946</td>
    </tr>
    <tr>
        <td>In which UK city were The Beatles founded in 1960?</td>
        <td>Liverpool</td>
    </tr>
    <tr>
        <td>In which World Cup did England captain Harry Kane score 6 goals, earning him the Golden Boot award?</td>
        <td>2018 Russia World Cup</td>
    </tr>
    <tr>
        <td>In which World Cup did Spain win its first World Cup?</td>
        <td>2010 South Africa World Cup</td>
    </tr>
    <tr>
        <td>In which year did Alexander Fleming discover penicillin?</td>
        <td>1928</td>
    </tr>
    <tr>
        <td>In which year did every World Cup start having a mascot?</td>
        <td>1966</td>
    </tr>
    <tr>
        <td>In which year did Richard the Lionheart and Saladin sign a truce?</td>
        <td>1192</td>
    </tr>
    <tr>
        <td>In which year was the Battle of Guandu?</td>
        <td>200</td>
    </tr>
    <tr>
        <td>In which year was the first edition of “The Diary of Anne Frank” published?</td>
        <td>1947</td>
    </tr>
    <tr>
        <td>In which year was the first Wimbledon Tennis Championship held?</td>
        <td>1877</td>
    </tr>
    <tr>
        <td>In which year was the Napoleonic Code established?</td>
        <td>1804</td>
    </tr>
    <tr>
        <td>In which year was the Soviet Union formally established?</td>
        <td>1922</td>
    </tr>
    <tr>
        <td>In whose workshop did Leonardo da Vinci apprentice?</td>
        <td>Andrea del Verrocchio</td>
    </tr>
    <tr>
        <td>Insufficient intake of which element may cause the thyromegaly?</td>
        <td>Iodine</td>
    </tr>
    <tr>
        <td>Into what kind of deity was Imhotep deified following his death?</td>
        <td>A god of medicine</td>
    </tr>
    <tr>
        <td>Into what shape does commander Yi Seong-Gye’s fan-shaped active skill transform after gaining Expertise?
        </td>
        <td>A circle</td>
    </tr>
    <tr>
        <td>Japanese ivy grows upward on walls and trees in order to capture energy from which source?</td>
        <td>Sunlight</td>
    </tr>
    <tr>
        <td>Johannes Vermeer, the painter of Girl with a Pearl Earring, was from which country?</td>
        <td>The Netherlands</td>
    </tr>
    <tr>
        <td>Keira once defied the world’s expectations about her in a glorious battle. Where did this battle take place?
        </td>
        <td>Sunset Canyon</td>
    </tr>
    <tr>
        <td>Lactose intolerance is caused by a shortage of what?</td>
        <td>Lactase</td>
    </tr>
    <tr>
        <td>Legendary commander of the Joseon Dynasty, Yi Sun-sin, sacrificed his life in which famous battle?</td>
        <td>Legendary commander of the Joseon Dynasty, Yi Sun-sin, sacrificed his life in which famous battle?</td>
    </tr>
    <tr>
        <td>Li Xin, the ancestor of the “Flying General” Li Guang, was a general of which state during the Warring
            States
            period of Chinese history?
        </td>
        <td>Qin</td>
    </tr>
    <tr>
        <td>Messi has used 6 different jersey numbers in his career. What are those 6 numbers?</td>
        <td>10, 14, 15, 18, 19, 30</td>
    </tr>
    <tr>
        <td>Military strategist Belisarius served as a personal bodyguard to which emperor in his youth?</td>
        <td>Justinianus I</td>
    </tr>
    <tr>
        <td>Miroslav Klose holds the record for most goals scored in the World Cup. How many goals has he scored?</td>
        <td>16</td>
    </tr>
    <tr>
        <td>Murasaki is a character in which of the following works?</td>
        <td>Tale of Genji</td>
    </tr>
    <tr>
        <td>Mutations are important because they bring about what?</td>
        <td>Genetic variation needed for a population to evolve</td>
    </tr>
    <tr>
        <td>Northern Humanists like Erasmus were most commonly known for what?</td>
        <td>Interest in religious and social reform</td>
    </tr>
    <tr>
        <td>Noted Danish archaeologist Christian Jürgensen Thomsen proposed the “three ages” system for categorizing
            ancient
            history. Which is the first stage?
        </td>
        <td>Stone Age</td>
    </tr>
    <tr>
        <td>Of the 12 Chinese Zodiac animals, which comes first?</td>
        <td>Rat</td>
    </tr>
    <tr>
        <td>Of the below commanders, which is best suited to lead troops AGAINST archer enemies?</td>
        <td>Cao Cao</td>
    </tr>
    <tr>
        <td>Of the below commanders, which is best suited to lead troops AGAINST cavalry enemies?</td>
        <td>Sun Tzu</td>
    </tr>
    <tr>
        <td>Of the below commanders, which is best suited to lead troops AGAINST infantry enemies?</td>
        <td>Hermann</td>
    </tr>
    <tr>
        <td>Of the below commanders, which is best suited to lead troops when gathering resources?</td>
        <td>Seondeok</td>
    </tr>
    <tr>
        <td>Of the following, which is most likely to have directly led to the Bronze Age?</td>
        <td>Bronze is harder and more durable than other metals available at the time</td>
    </tr>
    <tr>
        <td>On its tragic maiden voyage, what was the Titanic’s intended destination?</td>
        <td>New York</td>
    </tr>
    <tr>
        <td>On which animal was the 2018 Russia World Cup’s mascot based on?</td>
        <td>Wolf</td>
    </tr>
    <tr>
        <td>On which continent did the serval first live?</td>
        <td>Africa</td>
    </tr>
    <tr>
        <td>On which continent is the human race generally thought to have originated?</td>
        <td>Africa</td>
    </tr>
    <tr>
        <td>On which ingredient was the 1986 Mexico World Cup’s mascot based on?</td>
        <td>Pepper</td>
    </tr>
    <tr>
        <td>One “day” on the planet Venus is approximately equal to how many earth days?</td>
        <td>243 days</td>
    </tr>
    <tr>
        <td>One of the benefits of canned food is its long shelf life. Where were the first canned herrings made?</td>
        <td>Northern Europe</td>
    </tr>
    <tr>
        <td>Pandas have 5 clawed toes on as well as a sixth digit. What do pandas use their sixth digit for?</td>
        <td>To hold bamboo while eating</td>
    </tr>
    <tr>
        <td>Passport Pages are used when immigrating to other Kingdoms. What does the number of Passport Pages used
            depend
            on?
        </td>
        <td>Governor’s Individual Power</td>
    </tr>
    <tr>
        <td>Peppers have been farmed for millennia. Which continent can we trace their origins back to?</td>
        <td>The Americas</td>
    </tr>
    <tr>
        <td>Qatar follows Arabia Standard Time. How would that be notated using UTC?</td>
        <td>UTC+3:00</td>
    </tr>
    <tr>
        <td>Queen Guinevere fell in love with which Knight of the Round Table according to Arthurian legend?</td>
        <td>Lancelot</td>
    </tr>
    <tr>
        <td>Queen Victoria of England was part of which royal house?</td>
        <td>The House of Hanover</td>
    </tr>
    <tr>
        <td>Raphael is best known for his creation of which of the following?</td>
        <td>School of Athens</td>
    </tr>
    <tr>
        <td>Santa Claus travels on a sleigh pulled by what animal?</td>
        <td>Reindeer</td>
    </tr>
    <tr>
        <td>Seondeok was Queen of Silla (part of modern-day Korea). What was her birth name?</td>
        <td>Kim Deokman</td>
    </tr>
    <tr>
        <td>Shanghai’s Jin Mao Tower has how many floors?</td>
        <td>88</td>
    </tr>
    <tr>
        <td>Since its founding in 1863, how many times has the International Committee of the Red Cross won the Nobel
            Peace
            Prize?
        </td>
        <td>3</td>
    </tr>
    <tr>
        <td>Some toothpastes contain powdered calcium carbonate. What is its function?</td>
        <td>Acts as an abrasive</td>
    </tr>
    <tr>
        <td>The “Battle of Las Navas de Tolosa” was a major turning point in which conflict?</td>
        <td>The Reconquista</td>
    </tr>
    <tr>
        <td>The “Beef Wellington” involves cooking filet mignon wrapped inside which other ingredient?</td>
        <td>Puff pastry</td>
    </tr>
    <tr>
        <td>The “Day of the Tiles” is regarded by some historians to mark the beginning of the French Revolution. In
            what
            city did it take place?
        </td>
        <td>Grenoble</td>
    </tr>
    <tr>
        <td>The “Dragonfly Cutter” was the weapon of choice of which legendary warrior?</td>
        <td>Honda Tadakatsu</td>
    </tr>
    <tr>
        <td>The “El Nino” phenomenon is named after which of these festivals?</td>
        <td>Christmas</td>
    </tr>
    <tr>
        <td>The “Independence March” is the national anthem of which country?</td>
        <td>Turkey</td>
    </tr>
    <tr>
        <td>The “Three Great Philosophers” of Ancient Greece were Plato, Aristotle, and who?</td>
        <td>Socrates</td>
    </tr>
    <tr>
        <td>The “Windsor knot” was made popular by which king of England?</td>
        <td>Edward VIII</td>
    </tr>
    <tr>
        <td>The 1,000 point achievement chest includes:</td>
        <td>Epic Commander Sculpture</td>
    </tr>
    <tr>
        <td>The 2022 World Cup will be the first men’s world cup with female referees. How many will there by?</td>
        <td>3</td>
    </tr>
    <tr>
        <td>The 24 “solar terms” are a traditional Chinese calendar system used for what purpose?</td>
        <td>Agriculture</td>
    </tr>
    <tr>
        <td>The air often feels fresher after a thunderstorm because it has been enriched with which gas?</td>
        <td>Ozone</td>
    </tr>
    <tr>
        <td>The Alliance Technology “Territory Guardian” increases which attribute of troops when battling in Alliance
            territory?
        </td>
        <td>Attack</td>
    </tr>
    <tr>
        <td>The Bastille, a prison once located in the center of Paris, France, originally served what purpose?</td>
        <td>Defensive Fortress</td>
    </tr>
    <tr>
        <td>The blueprint fragments of which of the following accessories CANNOT be redeemed from the Season of Conquest
            Shop?
        </td>
        <td>Pendant of Eternal Night</td>
    </tr>
    <tr>
        <td>The borders of which of these countries extend across two continents?</td>
        <td>Egypt</td>
    </tr>
    <tr>
        <td>The cathedral of Notre-Dame de Paris was built in which style?</td>
        <td>Gothic</td>
    </tr>
    <tr>
        <td>The coffee bean is in fact a type of…?</td>
        <td>Fruit</td>
    </tr>
    <tr>
        <td>The Colossus of Rhodes depicts which god of the sun?</td>
        <td>Helios</td>
    </tr>
    <tr>
        <td>The concept of ‘separation of powers’ comes from which work by Montesquieu?</td>
        <td>The Spirit of Law</td>
    </tr>
    <tr>
        <td>The cone-shaped winds of tornadoes are notable for doing what?</td>
        <td>Spinning</td>
    </tr>
    <tr>
        <td>The epic “Poem of El Cid” originated from which country?</td>
        <td>Spain</td>
    </tr>
    <tr>
        <td>The famous Renaissance sculpture Pietà was the work of which artist?</td>
        <td>Michelangelo</td>
    </tr>
    <tr>
        <td>The flag of which country holds the world record of being the oldest continuously used national flag?</td>
        <td>Denmark</td>
    </tr>
    <tr>
        <td>The four biggest musicals in the world are Cats, Les Miserables, Miss Saigon, and…?</td>
        <td>The Phantom of the Opera</td>
    </tr>
    <tr>
        <td>The four major varieties of Guzheng in Ancient China were the Haozhong, Luqi, Jiaowei, and the…?</td>
        <td>Raoliang</td>
    </tr>
    <tr>
        <td>The Gallic rooster is an official national symbol of which country?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>The general theory of relativity was published by which physicist?</td>
        <td>Einstein</td>
    </tr>
    <tr>
        <td>The Golden Bear Award is given out at which film festival?</td>
        <td>Berlin Film Festival</td>
    </tr>
    <tr>
        <td>The Golden Glove/Lev Yashin award is given to the best goalkeeper in the World Cup. In which World Cup was
            it
            first awarded?
        </td>
        <td>15th</td>
    </tr>
    <tr>
        <td>The Greek alphabet originated from which other writing system?</td>
        <td>Phoenician</td>
    </tr>
    <tr>
        <td>The Greek epic The Odyssey records the return of the hero Odysseus from war. Where was his homeland?</td>
        <td>Ithaca</td>
    </tr>
    <tr>
        <td>The kingdom title ‘Queen’ increases which of the following?</td>
        <td>Gathering Speed</td>
    </tr>
    <tr>
        <td>The Leaning Tower of Pisa is located in which of these countries?</td>
        <td>Italy</td>
    </tr>
    <tr>
        <td>The Luger pistol, manufactured for the German army during WW2, uses which type of ammo?</td>
        <td>9mm Parabellum</td>
    </tr>
    <tr>
        <td>The mascot of the 2010 South Africa World Cup was an anthropomorphic leopard with green hair. What was his
            name?
        </td>
        <td>Zakumi</td>
    </tr>
    <tr>
        <td>The military leader Charles Martel was NOT known for which of the following?</td>
        <td>Leading cavalry units</td>
    </tr>
    <tr>
        <td>The more genetic variation a population has, the more likely it is that some individual will what?</td>
        <td>Survive</td>
    </tr>
    <tr>
        <td>The Mughal Empire carried Islam to where?</td>
        <td>Most of India</td>
    </tr>
    <tr>
        <td>The Museum of Modern Art is located in which American city?</td>
        <td>New York</td>
    </tr>
    <tr>
        <td>The novel Romance of the Three Kingdoms begins near the end of which Chinese dynasty?</td>
        <td>Han</td>
    </tr>
    <tr>
        <td>The Pantheon has been the burial site of many important artists since the Renaissance. Which of the
            following
            artists were buried there?
        </td>
        <td>Raphael</td>
    </tr>
    <tr>
        <td>The Phoenician civilization originated in which part of the world?</td>
        <td>The Eastern Mediterranean</td>
    </tr>
    <tr>
        <td>The prehistoric humans known as “homo floresiensis” are also commonly referred to as?</td>
        <td>Hobbits</td>
    </tr>
    <tr>
        <td>The process of combining atoms is known as what?</td>
        <td>Fusion</td>
    </tr>
    <tr>
        <td>The process of splitting atoms is known as?</td>
        <td>Fission</td>
    </tr>
    <tr>
        <td>The Punic Wars were fought by Ancient Rome and what other ancient empire?</td>
        <td>Ancient Carthage</td>
    </tr>
    <tr>
        <td>The Qingming Riverside Map captures the daily life and landscape of which ancient Chinese dynasty?</td>
        <td>The Song</td>
    </tr>
    <tr>
        <td>The Renaissance was a “rebirth” of what?</td>
        <td>Classical culture</td>
    </tr>
    <tr>
        <td>The saying “The Empire on which the sun never sets” was originally used in reference to which country’s
            empire?
        </td>
        <td>Spain</td>
    </tr>
    <tr>
        <td>The Scoville scale is often used to rate the spiciness of peppers, a higher value denoting greater
            spiciness.
            Which of these peppers is rated at over 1 million on the Scoville scale?
        </td>
        <td>Soul pepper</td>
    </tr>
    <tr>
        <td>The Taj Mahal was built during which Indian Dynasty?</td>
        <td>The Mughal Dynasty</td>
    </tr>
    <tr>
        <td>The Tang dynasty style of pottery decoration known as “sancai” primarily uses which three colors?</td>
        <td>Yellow, green, white</td>
    </tr>
    <tr>
        <td>The Third Punic War led to the downfall of which state?</td>
        <td>Carthage</td>
    </tr>
    <tr>
        <td>The Three Kingdoms of Korea were Baekje, Silla, and …?</td>
        <td>Goguryeo</td>
    </tr>
    <tr>
        <td>The two capitals of Austria-Hungary were Budapest and which other city?</td>
        <td>Vienna</td>
    </tr>
    <tr>
        <td>The Uruguay national team displays four stars on its emblems. How many World Cups has it won?</td>
        <td>Two</td>
    </tr>
    <tr>
        <td>The writer of ‘A Song of Ice and Fire’ recently contributed to the script of which video game?</td>
        <td>Elden Ring</td>
    </tr>
    <tr>
        <td>The zeppelin was invented by engineers from which country?</td>
        <td>Germany</td>
    </tr>
    <tr>
        <td>To whom did India give the title “Mahatma”?</td>
        <td>Mohandas Karamchand Gandhi</td>
    </tr>
    <tr>
        <td>Tokugawa Ieyasu and who else became the three “Great Unifiers” of Japan?</td>
        <td>Oda Nobunaga and Toyotomi Hideyoshi</td>
    </tr>
    <tr>
        <td>Under normal circumstances, which layer of the Sun can we see with the naked eye?</td>
        <td>Photosphere</td>
    </tr>
    <tr>
        <td>Under which category does the formal writing system of ancient Egypt fall?</td>
        <td>Hieroglyphic</td>
    </tr>
    <tr>
        <td>Under which Pharaoh did ancient Egyptian minister Imhotep serve?</td>
        <td>Zoser</td>
    </tr>
    <tr>
        <td>Wales officially became part of the Kingdom of Great Britain during the reign of which King?</td>
        <td>Henry VIII</td>
    </tr>
    <tr>
        <td>What achievement would spending over 200 Golden Keys in the Tavern in a single transaction unlock?</td>
        <td>All In</td>
    </tr>
    <tr>
        <td>What age did ancient Greece enter after the Mycenaean civilization was destroyed?</td>
        <td>Dark Age</td>
    </tr>
    <tr>
        <td>What ancient Roman ruin located in Algeria was formally assigned World Heritage status in 1982?</td>
        <td>The ruins of Djemila</td>
    </tr>
    <tr>
        <td>What animal is the mascot of the 2022 Winter Olympics based on?</td>
        <td>Panda</td>
    </tr>
    <tr>
        <td>What animal is the “Tigris” named after in ancient Greek?</td>
        <td>Tiger</td>
    </tr>
    <tr>
        <td>What are Arrows of Resistance used for?</td>
        <td>To upgrade the Watchtower</td>
    </tr>
    <tr>
        <td>What are Books of Covenant used for?</td>
        <td>To upgrade the Castle</td>
    </tr>
    <tr>
        <td>What are the dimensions of the fog cleared by a Kingdom Map item?</td>
        <td>10×10</td>
    </tr>
    <tr>
        <td>What benefit does researching “Mathematics” in the Academy provide?</td>
        <td>Increased research speed</td>
    </tr>
    <tr>
        <td>What can “Masonry” technology help you to improve?</td>
        <td>The building speed of buildings in your city</td>
    </tr>
    <tr>
        <td>What caused the death of the outstanding statemen and military strategist, Pericles?</td>
        <td>A plague</td>
    </tr>
    <tr>
        <td>What caused the deaths of Ceroli Chieftain Dekar’s parents?</td>
        <td>The discovery of their witchcraft studies</td>
    </tr>
    <tr>
        <td>What causes lunar eclipses?</td>
        <td>The earth moves directly between the sun and the moon</td>
    </tr>
    <tr>
        <td>What civilization developed between the Tigris and Euphrates rivers?</td>
        <td>The Mesopotamians</td>
    </tr>
    <tr>
        <td>What color are the fresh flowers in the Lyceum of Wisdom for the French civilization?</td>
        <td>Red</td>
    </tr>
    <tr>
        <td>What color are the wings on the Pegasus decoration?</td>
        <td>Gold</td>
    </tr>
    <tr>
        <td>What color gemstones are engraved upon the crown of the commander El Cid?</td>
        <td>Red</td>
    </tr>
    <tr>
        <td>What color is a polar bear’s skin?</td>
        <td>Black</td>
    </tr>
    <tr>
        <td>What color is used on the roofs of buildings in the Japanese civilization?</td>
        <td>Black</td>
    </tr>
    <tr>
        <td>What color paint was favored by ancient Egyptian families for the walls of their homes?</td>
        <td>White</td>
    </tr>
    <tr>
        <td>What color was the match ball used in the 1966 World Cup Final?</td>
        <td>Orange</td>
    </tr>
    <tr>
        <td>What country did the infamous pirate Blackbeard come from?</td>
        <td>England</td>
    </tr>
    <tr>
        <td>What country is not in Europe?</td>
        <td>Egypt</td>
    </tr>
    <tr>
        <td>What date is World Environment Day held each year?</td>
        <td>June 5th</td>
    </tr>
    <tr>
        <td>What did Che Guevara study in college?</td>
        <td>Medicine</td>
    </tr>
    <tr>
        <td>What did Spartacus do as a slave before leading the biggest slave rebellion of Ancient Rome?</td>
        <td>A gladiator</td>
    </tr>
    <tr>
        <td>What did the name for ancient Egypt’s Luxor Temple also mean?</td>
        <td>The southern sanctuary</td>
    </tr>
    <tr>
        <td>What did the word “marathon” originally refer to?</td>
        <td>A place name</td>
    </tr>
    <tr>
        <td>What did Theodorus van Gogh, the younger brother of famed Dutch painter Vincent Van Gogh, do for a living?
        </td>
        <td>Art Dealer</td>
    </tr>
    <tr>
        <td>What discovery made the deciphering of hieroglyphics possible?</td>
        <td>The Rosetta Stone</td>
    </tr>
    <tr>
        <td>What do Dragon Lancers carry in their left hand?</td>
        <td>A tobacco pipe</td>
    </tr>
    <tr>
        <td>What do you have to do to earn the Witness Protection achievement?</td>
        <td>Change your nickname 10 times</td>
    </tr>
    <tr>
        <td>What does a “hat-trick” mean in soccer?</td>
        <td>One player scoring 3 goals for their team</td>
    </tr>
    <tr>
        <td>What does the ankh character represent in ancient Egyptian hieroglyphics?</td>
        <td>Life</td>
    </tr>
    <tr>
        <td>What does the word “Formula” in “Formula 1 Racing” refer to?</td>
        <td>The rules which participating cars must follow</td>
    </tr>
    <tr>
        <td>What four-word saying did Oda Nobunaga live by?</td>
        <td>Rule With absolute force</td>
    </tr>
    <tr>
        <td>What honorary rank was the founder of Kentucky Fried Chicken (KFC) given?</td>
        <td>Colonel</td>
    </tr>
    <tr>
        <td>What image is engraved on the Hammer of the Sun and Moon weapon?</td>
        <td>The sun</td>
    </tr>
    <tr>
        <td>What information does the item Deceptive Troops falsify for enemy Scouts?</td>
        <td>Your City’s unit count</td>
    </tr>
    <tr>
        <td>What ingredients are used to make “zongzi”, a traditional food eaten in China during the Dragon Boat
            Festival?
        </td>
        <td>Glutinous rice</td>
    </tr>
    <tr>
        <td>What instrument was officially banned by FIFA after the 2010 South Africa World Cup?</td>
        <td>Vuvuzela</td>
    </tr>
    <tr>
        <td>What is a “light-year” a measure of?</td>
        <td>Distance</td>
    </tr>
    <tr>
        <td>What is Belisarius’ nickname?</td>
        <td>Last of the Romans</td>
    </tr>
    <tr>
        <td>What is China’s earliest known book of military strategy?</td>
        <td>Sun Tzu’s “The Art of War”</td>
    </tr>
    <tr>
        <td>What is Commander Baibars good at?</td>
        <td>Leading cavalry units</td>
    </tr>
    <tr>
        <td>What is Commander Cao Cao’s expertise skill called?</td>
        <td>Emperor Wu of Wei</td>
    </tr>
    <tr>
        <td>What is Commander Eulji Mundeok good at?</td>
        <td>Leading infantry units</td>
    </tr>
    <tr>
        <td>What is Commander Hermann good at?</td>
        <td>Leading archer units</td>
    </tr>
    <tr>
        <td>What is Commander Keira good at?</td>
        <td>Leading archer units</td>
    </tr>
    <tr>
        <td>What is Commander Lancelot good at?</td>
        <td>Leading cavalry units</td>
    </tr>
    <tr>
        <td>What is Commander Osman I good at?</td>
        <td>Attacking cities</td>
    </tr>
    <tr>
        <td>What is Commander Sun Tzu good at?</td>
        <td>Leading city garrisons</td>
    </tr>
    <tr>
        <td>What is Julius Caesar’s nickname?</td>
        <td>The Uncrowned King</td>
    </tr>
    <tr>
        <td>What is Karaku’s Fire Shaman called?</td>
        <td>Denko</td>
    </tr>
    <tr>
        <td>What is the 2nd-largest constituent nation of the United Kingdom?</td>
        <td>Scotland</td>
    </tr>
    <tr>
        <td>What is the alliance rally capacity provided by a Lvl 15 Fort?</td>
        <td>775,000</td>
    </tr>
    <tr>
        <td>What is the alternative name of Shakespeare’s play Twelfth Night?</td>
        <td>What You Will</td>
    </tr>
    <tr>
        <td>What is the capital of the Republic of Chile?</td>
        <td>Santiago</td>
    </tr>
    <tr>
        <td>What is the center of a hurricane called?</td>
        <td>Eye</td>
    </tr>
    <tr>
        <td>What is the champion Keira’s nickname?</td>
        <td>The Red Chameleon</td>
    </tr>
    <tr>
        <td>What is the color of the floral headdress worn by commander Šárka in Rise of Kingdoms?</td>
        <td>Red</td>
    </tr>
    <tr>
        <td>What is the color of the jade pendant around the commander Cao Cao’s waist?</td>
        <td>Green</td>
    </tr>
    <tr>
        <td>What is the Commander Hannibal Barca good at?</td>
        <td>Attacking cities</td>
    </tr>
    <tr>
        <td>What is the conversion ratio of amperes (A) to milliamperes (mA)?</td>
        <td>1:1000</td>
    </tr>
    <tr>
        <td>What is the default rallied troop capacity when your castle is level 25?</td>
        <td>2 million</td>
    </tr>
    <tr>
        <td>What is the default transportation capacity if your trading post is level 25?</td>
        <td>5 million</td>
    </tr>
    <tr>
        <td>What is the duration of the Builder Recruitment item?</td>
        <td>2 days</td>
    </tr>
    <tr>
        <td>What is the Expertise Skill of the commander Alexander the Great called?</td>
        <td>Son of Amun</td>
    </tr>
    <tr>
        <td>What is the Gem cost of the Human Sculpture decoration?</td>
        <td>2,000</td>
    </tr>
    <tr>
        <td>What is the hardest part of the human body?</td>
        <td>Tooth Enamel</td>
    </tr>
    <tr>
        <td>What is the highest level a City Hall can reach in RoK?</td>
        <td>25</td>
    </tr>
    <tr>
        <td>What is the highest mountain in Africa?</td>
        <td>Mount Kilimanjaro</td>
    </tr>
    <tr>
        <td>What is the in-game nickname of Cleopatra VII?</td>
        <td>Queen of Egypt</td>
    </tr>
    <tr>
        <td>What is the largest airport in the world by area?</td>
        <td>King Fahd International Airport</td>
    </tr>
    <tr>
        <td>What is the largest animal on the planet?</td>
        <td>Blue whale</td>
    </tr>
    <tr>
        <td>What is the largest city in the UK by area?</td>
        <td>London</td>
    </tr>
    <tr>
        <td>What is the largest country in South America?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>What is the largest freshwater lake in the world by area?</td>
        <td>Lake Superior</td>
    </tr>
    <tr>
        <td>What is the largest lake on the South American continent?</td>
        <td>Lake Maracaibo</td>
    </tr>
    <tr>
        <td>What is the largest living species of bird in the world?</td>
        <td>Ostrich</td>
    </tr>
    <tr>
        <td>What is the length of a marathon in kilometers?</td>
        <td>42.2 Km</td>
    </tr>
    <tr>
        <td>What is the main ingredient in salt?</td>
        <td>Sodium chloride</td>
    </tr>
    <tr>
        <td>What is the main ingredient of popcorn?</td>
        <td>Corn</td>
    </tr>
    <tr>
        <td>What is the main purpose of white blood cells in the human body?</td>
        <td>Fighting infection and disease</td>
    </tr>
    <tr>
        <td>What is the maximum number of King’s Road decorative buildings a governor can construct?</td>
        <td>400</td>
    </tr>
    <tr>
        <td>What is the maximum number of troops you can form in the Champions of Olympia event?</td>
        <td>9</td>
    </tr>
    <tr>
        <td>What is the maximum period of time that alliance resources can accumulate for in the Alliance Territory
            screen?
        </td>
        <td>24h</td>
    </tr>
    <tr>
        <td>What is the minimum number of Silver Keys that can be obtained from the “Treasure of Wisdom” bundle?</td>
        <td>2</td>
    </tr>
    <tr>
        <td>What is the most abundant element in earth’s crust?</td>
        <td>Oxygen</td>
    </tr>
    <tr>
        <td>What is the name for electricity produced by water power using large dams in a river?</td>
        <td>Hydroelectric energy</td>
    </tr>
    <tr>
        <td>What is the name for renewable energy derived from burning organic materials such as wood and alcohol?</td>
        <td>Biomass</td>
    </tr>
    <tr>
        <td>What is the name for the phenomenon by which organisms that are better adapted to the environment survive to
            pass traits to their offspring?
        </td>
        <td>Natural selection</td>
    </tr>
    <tr>
        <td>What is the name of the 2022 Qatar World Cup mascot?</td>
        <td>La’eeb</td>
    </tr>
    <tr>
        <td>What is the name of the achievement obtained by acquiring 3 permanent city themes?</td>
        <td>Technicolor Themes</td>
    </tr>
    <tr>
        <td>What is the name of the fourth bracket in the “Champions of Olympia” event?</td>
        <td>Brilliant Silver</td>
    </tr>
    <tr>
        <td>What is the name of the revolution in 1688 which established the British constitutional monarchy?</td>
        <td>The Glorious Revolution</td>
    </tr>
    <tr>
        <td>What is the name of the swindler who can also appear at the same time as the Ceroli Chieftain Bloodfist
            Bargha?
        </td>
        <td>Dhalruk</td>
    </tr>
    <tr>
        <td>What is the name of the Turkish straits that divide Europe from Asia?</td>
        <td>The Bosphorus</td>
    </tr>
    <tr>
        <td>What is the nationality of Pierre de Fermat, who proposed Fermat’s Last Theorem?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>What is the only country to have won both the Men’s and Women’s World Cup?</td>
        <td>Germany</td>
    </tr>
    <tr>
        <td>What is the only known short-period comet that is regularly visible to the naked eye from Earth?</td>
        <td>Halley’s Comet</td>
    </tr>
    <tr>
        <td>What is the primary cause of karst landforms?</td>
        <td>The dissolution of soluble rocks</td>
    </tr>
    <tr>
        <td>What is the root cause of altitude sickness?</td>
        <td>Rapid exposure to low amounts of oxygen at high elevation</td>
    </tr>
    <tr>
        <td>What is the softest mineral currently known to mankind?</td>
        <td>Talc</td>
    </tr>
    <tr>
        <td>What is the tallest peak in the world?</td>
        <td>Mount Everest</td>
    </tr>
    <tr>
        <td>What is the total Lvl 25 Hospital Capacity for severely wounded troops (excluding boosts)?</td>
        <td>300,000</td>
    </tr>
    <tr>
        <td>What is the unlock condition for Nightmare difficulty in the “Ian’s Ballads” event?</td>
        <td>Reach City Hall Lvl 23 and 20 million Power</td>
    </tr>
    <tr>
        <td>What is the VIP Level required to purchase a Legendary Commander Sculpture on a weekly discount from the VIP
            Shop?
        </td>
        <td>Lvl 13</td>
    </tr>
    <tr>
        <td>What is the world’s oldest film festival?</td>
        <td>Venice Film Festival</td>
    </tr>
    <tr>
        <td>What item summons Dauntless Lohar?</td>
        <td>Lohar’s Buckler</td>
    </tr>
    <tr>
        <td>What kind of animal was the mascot of the 1998 World Cup in France?</td>
        <td>Cockerel</td>
    </tr>
    <tr>
        <td>What kind of boost to allied forces does commander Joan of Arc’s active skill provide?</td>
        <td>Rage</td>
    </tr>
    <tr>
        <td>What kind of combat blessing do Archers receive in Champions of Olympia?</td>
        <td>The Blessing of Ares</td>
    </tr>
    <tr>
        <td>What kind of combat blessing do Cavalry receive in Champions of Olympia?</td>
        <td>The Blessing of Hades</td>
    </tr>
    <tr>
        <td>What kind of combat blessing do Infantry receive in Champions of Olympia?</td>
        <td>The Blessing of Athena</td>
    </tr>
    <tr>
        <td>What kind of combat blessing do mixed units receive in Champions of Olympia?</td>
        <td>The Blessing of Apollo</td>
    </tr>
    <tr>
        <td>What kind of commander becomes the temporary Garrison Commander in the event a Governor has not nominated a
            commander or the nominated commander is not present?
        </td>
        <td>The highest level commander currently present</td>
    </tr>
    <tr>
        <td>What letter is shown on the Scolas’ Lucky Coin decoration?</td>
        <td>L</td>
    </tr>
    <tr>
        <td>What level does your City Hall have to be to enter the Iron Age?</td>
        <td>Lvl 10</td>
    </tr>
    <tr>
        <td>What material did the craftsmen of ancient Egypt favor for sculpting statues and murals?</td>
        <td>Stone</td>
    </tr>
    <tr>
        <td>What number World Cup will the 2022 World Cup be?</td>
        <td>22nd</td>
    </tr>
    <tr>
        <td>What occupation buff is provided by the Flame Altar?</td>
        <td>Troop Attack +3%</td>
    </tr>
    <tr>
        <td>What outstanding contribution to science won biologist Karl Landsteiner the Nobel Prize?</td>
        <td>Discovering the human blood groups</td>
    </tr>
    <tr>
        <td>What process is responsible for the red hues of the eponymous Red Sea?</td>
        <td>Unusual algae</td>
    </tr>
    <tr>
        <td>What provided the major economic support for the Renaissance?</td>
        <td>Wealthy patrons</td>
    </tr>
    <tr>
        <td>What race of creatures, mythical or otherwise, lived on Asgard according to Norse myth?</td>
        <td>Aesir</td>
    </tr>
    <tr>
        <td>What reward may be given to governors who do NOT qualify for the Ark of Osiris but whose fellow alliance
            members
            do qualify and are defeated?
        </td>
        <td>Gems</td>
    </tr>
    <tr>
        <td>What tier of crystal treasure is found in the Lvl 26 Alliance Gift?</td>
        <td>Treasure of Blue Crystal</td>
    </tr>
    <tr>
        <td>What two countries together made up the “Iberian Union”?</td>
        <td>Portugal and Spain</td>
    </tr>
    <tr>
        <td>What two major works are attributed to Homer?</td>
        <td>The Odyssey and The Iliad</td>
    </tr>
    <tr>
        <td>What useful tool did famed carpenter Lu Ban invent?</td>
        <td>The saw</td>
    </tr>
    <tr>
        <td>What was another usage of gin, aside from being a popular drink, in the 18th century?</td>
        <td>Medicine</td>
    </tr>
    <tr>
        <td>What was the country of origin of the architect who designed the Museum of Egyptian Antiquities located in
            Cairo?
        </td>
        <td>France</td>
    </tr>
    <tr>
        <td>What was the final score of the 2014 World Cup final between Germany and Argentina?</td>
        <td>1 – 0</td>
    </tr>
    <tr>
        <td>What was the first men’s World Cup to have a female referee?</td>
        <td>2022 Qatar World Cup</td>
    </tr>
    <tr>
        <td>What was the first World Cup trophy called?</td>
        <td>Jules Rimet Trophy</td>
    </tr>
    <tr>
        <td>What was the homeland of Odysseus, the subject of Homer’s Odyssey?</td>
        <td>Ithaca</td>
    </tr>
    <tr>
        <td>What was the name of the alliance led by Athens in the Peloponnesian War?</td>
        <td>Delian League</td>
    </tr>
    <tr>
        <td>What was the name of the creature in Greek mythology that lured sailors with its heavenly singing voice?
        </td>
        <td>Siren</td>
    </tr>
    <tr>
        <td>What was the name of the official match ball of the 2018 World Cup?</td>
        <td>Telstar</td>
    </tr>
    <tr>
        <td>What was the native language of Cleopatra VII, otherwise known as Cleopatra the Queen of Egypt?</td>
        <td>Greek</td>
    </tr>
    <tr>
        <td>What was the original purpose of the Great Pyramid of Giza?</td>
        <td>Pharaoh Khufu’s tomb</td>
    </tr>
    <tr>
        <td>What was the regnal name of the last emperor of China’s Qing Dynasty?</td>
        <td>Xuantong</td>
    </tr>
    <tr>
        <td>What was the relationship between the famed generals Tolui and Genghis Khan?</td>
        <td>Father and son</td>
    </tr>
    <tr>
        <td>What was the venue for the final match of the 2018 World Cup held in Russia?</td>
        <td>Luzhniki Stadium, Moscow</td>
    </tr>
    <tr>
        <td>What weapon does the commander Richard I wield?</td>
        <td>An axe and shield</td>
    </tr>
    <tr>
        <td>What weapons do City Keepers wield?</td>
        <td>An axe and shield</td>
    </tr>
    <tr>
        <td>What weapons does the commander City Keeper wield?</td>
        <td>An axe and shield</td>
    </tr>
    <tr>
        <td>What were the men and women hiding from on the outskirts of Florence in the novella collection “The
            Decameron”?
        </td>
        <td>The Black Plague</td>
    </tr>
    <tr>
        <td>What’s does the “Fully Loaded” alliance skill do when used?</td>
        <td>Increases alliance members’ gathering speed for one type of resource</td>
    </tr>
    <tr>
        <td>What’s does the “Nature’s Gift” alliance skill do when used?</td>
        <td>Summon resource points in a selected area</td>
    </tr>
    <tr>
        <td>What’s does the “Skillful Craftsman” alliance skill do when used?</td>
        <td>Greatly increases alliance flag construction speed</td>
    </tr>
    <tr>
        <td>What’s the primary cause of the formation of the Himalayas?</td>
        <td>Collision between plates</td>
    </tr>
    <tr>
        <td>When a governor’s city hall reaches level 25, how many farms can they build?</td>
        <td>4</td>
    </tr>
    <tr>
        <td>When Achilles refuses to fight, Patroclus puts on Achilles armor, fights the Trojans, and dies. Who is
            Patroclus
            killed by?
        </td>
        <td>Hector</td>
    </tr>
    <tr>
        <td>When alliance members gather resources in alliance territory, how much is their Gathering Speed increased
            by?
        </td>
        <td>25%</td>
    </tr>
    <tr>
        <td>When did the “hard-boiled” or “noir” literary genre of fiction first fluorish?</td>
        <td>1920s</td>
    </tr>
    <tr>
        <td>When did the War of the Austrian Succession take place?</td>
        <td>1740-1748</td>
    </tr>
    <tr>
        <td>When moist air encounters an obstacle such as a mountain range and is forced upward while air temperature
            drops,
            resulting in rain, this is known as:
        </td>
        <td>Orographic precipitation</td>
    </tr>
    <tr>
        <td>When not affected by buffs, how much resource capacity does a Level 25 Storehouse have for each type of
            resource?
        </td>
        <td>2,500,000</td>
    </tr>
    <tr>
        <td>When was Coca-Cola invented?</td>
        <td>The 19th century</td>
    </tr>
    <tr>
        <td>When was Napoleon Bonaparte born?</td>
        <td>1769</td>
    </tr>
    <tr>
        <td>When was the Declaration of Independence written?</td>
        <td>1776</td>
    </tr>
    <tr>
        <td>When was the first Venice Film Festival held?</td>
        <td>1932</td>
    </tr>
    <tr>
        <td>When was the theremin, one of the first electronic musical instruments, invented?</td>
        <td>1919</td>
    </tr>
    <tr>
        <td>When were bicycles introduced to Europe?</td>
        <td>The 19th century</td>
    </tr>
    <tr>
        <td>Where are the world’s largest proven oil reserves located?</td>
        <td>Ghawar Field</td>
    </tr>
    <tr>
        <td>Where did Hippocrates, the “Father of Medicine”, come from?</td>
        <td>Ancient Greece</td>
    </tr>
    <tr>
        <td>Where did King Louis XIV of France move the royal Court to in 1682?</td>
        <td>The Palace of Versailles</td>
    </tr>
    <tr>
        <td>Where did reggae music originate?</td>
        <td>Jamaica</td>
    </tr>
    <tr>
        <td>Where did the final battle between King Arthur and Mordred take place according to Arthurian legend?</td>
        <td>Camlann</td>
    </tr>
    <tr>
        <td>Where did the Renaissance begin?</td>
        <td>Italy</td>
    </tr>
    <tr>
        <td>Where do adaptations come from?</td>
        <td>Both mutations and genetic recombination</td>
    </tr>
    <tr>
        <td>Where do most hurricanes start?</td>
        <td>On the open seas</td>
    </tr>
    <tr>
        <td>Where do most of the Ainu people live?</td>
        <td>Hokkaido</td>
    </tr>
    <tr>
        <td>Where in Egypt is the Sphinx located?</td>
        <td>The Giza Plateau</td>
    </tr>
    <tr>
        <td>Where in Italy is Vatican City located?</td>
        <td>Rome</td>
    </tr>
    <tr>
        <td>Where is the Capital of Scotland?</td>
        <td>Edinburgh</td>
    </tr>
    <tr>
        <td>Where is the capital of the Germany?</td>
        <td>Berlin</td>
    </tr>
    <tr>
        <td>Where is the commander Gaius Marius from?</td>
        <td>Ancient Rome</td>
    </tr>
    <tr>
        <td>Where is the largest pyramid in the world?</td>
        <td>The Great Pyramid of Cholula</td>
    </tr>
    <tr>
        <td>Where is the right hand of the famous sculpture known as “The Thinker” resting?</td>
        <td>Under the chin</td>
    </tr>
    <tr>
        <td>Where was Commander Lohar born?</td>
        <td>In a remote barbarian tribe</td>
    </tr>
    <tr>
        <td>Where was famed musician Beethoven born?</td>
        <td>Bonn</td>
    </tr>
    <tr>
        <td>Where was Pablo Picasso born?</td>
        <td>Iberia</td>
    </tr>
    <tr>
        <td>Where was the Ceroli Chieftain Torgny born?</td>
        <td>Citadel of Braves</td>
    </tr>
    <tr>
        <td>Where was the rallying point for the first Crusade?</td>
        <td>Constantinople</td>
    </tr>
    <tr>
        <td>Where was the World Heritage Terracotta Army found?</td>
        <td>In a royal mausoleum</td>
    </tr>
    <tr>
        <td>Which achievement is unlocked from upgrading 2 hospitals to Lvl 25?</td>
        <td>Hale &amp; Hearty</td>
    </tr>
    <tr>
        <td>Which ancient civilization built the “Lost City” of Machu Picchu?</td>
        <td>The Incas</td>
    </tr>
    <tr>
        <td>Which ancient Egyptian deity is represented with the head of a lion?</td>
        <td>Tefnut</td>
    </tr>
    <tr>
        <td>Which ancient Egyptian pharaoh’s tomb did British archaeologist Howard Carter discover in 1922?</td>
        <td>Tutankhamun</td>
    </tr>
    <tr>
        <td>Which Ancient Greek physicist famously discovered the concept of buoyancy while taking a bath?</td>
        <td>Archimedes</td>
    </tr>
    <tr>
        <td>Which ancient Greek scholar laid the foundation for future European mathematics and authored the “Elements”
            of
            geometry?
        </td>
        <td>Euclid</td>
    </tr>
    <tr>
        <td>Which animal is featured on commander Richard I’s background image?</td>
        <td>A dog</td>
    </tr>
    <tr>
        <td>Which art group is known as a “national treasure” of Canada?</td>
        <td>Cirque du Soleil</td>
    </tr>
    <tr>
        <td>Which artist painted the world-famous “Girl with a Pearl Earring”?</td>
        <td>Johannes Vermeer</td>
    </tr>
    <tr>
        <td>Which artist shocked the world by shredding a copy of his own work, Girl with Balloon, at an auction?</td>
        <td>Banksy</td>
    </tr>
    <tr>
        <td>Which astronomer proposed the heliocentric theory in the 16th century?</td>
        <td>Nicolaus Copernicus</td>
    </tr>
    <tr>
        <td>Which beverage was invented by American pharmacist John Pemberton?</td>
        <td>Cola</td>
    </tr>
    <tr>
        <td>Which British star performed at the opening ceremony of the 2018 World Cup?</td>
        <td>Robbie Williams</td>
    </tr>
    <tr>
        <td>Which buff can be acquired from occupying a Sanctum of Courage?</td>
        <td>Increase EXP gained by commanders</td>
    </tr>
    <tr>
        <td>Which building was originally built in London to house the Great Exhibition of the Works of Industry of All
            Nations held in 1851?
        </td>
        <td>The Crystal Palace</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain comes from Horn Bay?</td>
        <td>Keira</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain comes from Sabertooth Valley?</td>
        <td>Ak &amp; Hok</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain dual wields a dagger and bow?</td>
        <td>Wafura</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain dwells in a place known as Death Forest?</td>
        <td>Dekar</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain has been tasked with safeguarding Windrage Cliffs?</td>
        <td>Astrid</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain has power over frost?</td>
        <td>Frida</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain once encircled the Citadel of Braves with their forces?</td>
        <td>Torgny</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain safeguards the Whitestone Tower?</td>
        <td>Frida</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain spent over 20 years garrisoning the Northern Reaches?</td>
        <td>Torgny</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain uses a bow for a weapon?</td>
        <td>Keira</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain wields two weapons at once?</td>
        <td>Astrid</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain works alongside a snake?</td>
        <td>Dekar</td>
    </tr>
    <tr>
        <td>Which Ceroli Chieftain’s tribe has lived in the freezing wastes for generations?</td>
        <td>Wafura</td>
    </tr>
    <tr>
        <td>Which chapter of the Kingdom Chronicles grants the “Explorer for Life” achievement if all mist is explored
            before the chapter’s end?
        </td>
        <td>Long Peace</td>
    </tr>
    <tr>
        <td>Which Chieftain from the Ceroli Crisis event is well-versed in poison magic?</td>
        <td>Dekar</td>
    </tr>
    <tr>
        <td>Which Chieftain in the Ceroli Crisis event mounted a successful coup?</td>
        <td>Torgny</td>
    </tr>
    <tr>
        <td>Which Chinese dynasty did Jiang Ziya help to establish?</td>
        <td>The Zhou Dynasty</td>
    </tr>
    <tr>
        <td>Which city hosted the 2000 Summer Olympics?</td>
        <td>Sydney</td>
    </tr>
    <tr>
        <td>Which city-state defeated Sparta in the “Battle of Leuctra”?</td>
        <td>Thebes</td>
    </tr>
    <tr>
        <td>Which city-state of Ancient Greece was known for its brutal military training and bravery?</td>
        <td>Sparta</td>
    </tr>
    <tr>
        <td>Which civilian airport is the highest above sea level?</td>
        <td>Daocheng Yading Airport</td>
    </tr>
    <tr>
        <td>Which civilization was the first to have public toilets?</td>
        <td>Ancient Rome</td>
    </tr>
    <tr>
        <td>Which club has footballer Lionel Messi played for the longest?</td>
        <td>Barcelona</td>
    </tr>
    <tr>
        <td>Which commander can be said to have the thickest eyebrows in Rise of Kingdoms?</td>
        <td>Cao Cao</td>
    </tr>
    <tr>
        <td>Which commander does NOT receive a skill level boost from a Legendary Commander Sculpture?</td>
        <td>Hannibal Barca</td>
    </tr>
    <tr>
        <td>Which commander features a ship in their background image?</td>
        <td>Pelagius</td>
    </tr>
    <tr>
        <td>Which commander has a crown with gemstones of a different color to the others?</td>
        <td>Queen Tamar of Georgia</td>
    </tr>
    <tr>
        <td>Which commander is commonly regarded as having exemplified the ideal of the “Bushido Spirit”?</td>
        <td>Kusunoki Masashige</td>
    </tr>
    <tr>
        <td>Which commander is known as “The Immortal Hammer”?</td>
        <td>Charles Martel</td>
    </tr>
    <tr>
        <td>Which commander is known as Barbarossa?</td>
        <td>Frederick I</td>
    </tr>
    <tr>
        <td>Which commander is known as Kamakura’s Warlord?</td>
        <td>Minamoto no Yoshitsune</td>
    </tr>
    <tr>
        <td>Which commander is known as the Conqueror of Chaos?</td>
        <td>Cao Cao</td>
    </tr>
    <tr>
        <td>Which commander is known as the Lady of the Mercians?</td>
        <td>Æthelflæd</td>
    </tr>
    <tr>
        <td>Which commander is nicknamed “Bow of Revolution”?</td>
        <td>Yi Seong-Gye</td>
    </tr>
    <tr>
        <td>Which commander is nicknamed Carthage’s Guardian?</td>
        <td>Hannibal Barca</td>
    </tr>
    <tr>
        <td>Which commander is nicknamed the Father of Conquest?</td>
        <td>Baibars</td>
    </tr>
    <tr>
        <td>Which commander is nicknamed the Roaring Barbarian?</td>
        <td>Lohar</td>
    </tr>
    <tr>
        <td>Which commander is reputed to have said, “I came, I saw, I conquered”?</td>
        <td>Julius Caesar</td>
    </tr>
    <tr>
        <td>Which commander role provides a load boost to troops?</td>
        <td>Supply Captain</td>
    </tr>
    <tr>
        <td>Which commander says: “My destiny is determined by faith and opportunity”?</td>
        <td>Matilda of Flanders</td>
    </tr>
    <tr>
        <td>Which commander showed their military genius in the Battle of Tours in the 8th century AD?</td>
        <td>Charles Martel</td>
    </tr>
    <tr>
        <td>Which commander signed a peace treaty with Saladin in 1192?</td>
        <td>Richard I</td>
    </tr>
    <tr>
        <td>Which commander was considered an enemy of Rome from an early age?</td>
        <td>Hannibal Barca</td>
    </tr>
    <tr>
        <td>Which commander was in charge of capturing Constantinople at the age of 21?</td>
        <td>Mehmed II</td>
    </tr>
    <tr>
        <td>Which commander was known as the Celtic Rose?</td>
        <td>Boudica</td>
    </tr>
    <tr>
        <td>Which commander was paired with Minamoto?</td>
        <td>Tomoe Gozen</td>
    </tr>
    <tr>
        <td>Which commander wields the Beauty and Wisdom skill?</td>
        <td>Cleopatra VII</td>
    </tr>
    <tr>
        <td>Which Commander’s active skill can inflict Silence?</td>
        <td>Hermann</td>
    </tr>
    <tr>
        <td>Which composer wrote the opera “The Ring of the Nibelung”?</td>
        <td>Wagner</td>
    </tr>
    <tr>
        <td>Which continent has the lowest average temperatures?</td>
        <td>Antarctica</td>
    </tr>
    <tr>
        <td>Which continent was the birthplace of Mayan civilization?</td>
        <td>The Americas</td>
    </tr>
    <tr>
        <td>Which country did Arjen Robben play for?</td>
        <td>The Netherlands</td>
    </tr>
    <tr>
        <td>Which country did the famous poet Dante Alighieri come from?</td>
        <td>Italy</td>
    </tr>
    <tr>
        <td>Which country did Zarathustra, of the tone poem ‘Thus Spoke Zarathustra’, come from?</td>
        <td>Persia</td>
    </tr>
    <tr>
        <td>Which country gave the Statue of Liberty to the American people as a gift?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Which country has adopted the quetzal as its national bird?</td>
        <td>Guatemala</td>
    </tr>
    <tr>
        <td>Which country has participated in 8 World Cups but has never advanced past the group stage?</td>
        <td>Scotland</td>
    </tr>
    <tr>
        <td>Which country has topped the FIFA world rankings but has never won a World Cup?</td>
        <td>Belgium</td>
    </tr>
    <tr>
        <td>Which country has won the most World Cups?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>Which country has won the World Cup five times?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>Which country hosted and won the 1930 World Cup?</td>
        <td>Uruguay</td>
    </tr>
    <tr>
        <td>Which country hosted and won the 1978 World Cup?</td>
        <td>Argentina</td>
    </tr>
    <tr>
        <td>Which country hosted the 1994 World Cup?</td>
        <td>The United States</td>
    </tr>
    <tr>
        <td>Which country hosted the 1998 World Cup?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Which country hosted the 2022 World Cup?</td>
        <td>Qatar</td>
    </tr>
    <tr>
        <td>Which country hosted the first ever Winter Olympics on January 25, 1924?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Which country hosted the first World Cup?</td>
        <td>Uruguay</td>
    </tr>
    <tr>
        <td>Which country is Çatalhöyük located in?</td>
        <td>Turkey</td>
    </tr>
    <tr>
        <td>Which country is responsible for inventing blue cheese, well-known for its pungent taste?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Which country used an orange as its World Cup mascot?</td>
        <td>Spain</td>
    </tr>
    <tr>
        <td>Which country was allowed to keep the Jules Rimet Trophy permanently?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>Which country was England fighting against in the Battle of Agincourt?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Which country was Marco Polo, a famous explorer and merchant, born in?</td>
        <td>Republic of Venice</td>
    </tr>
    <tr>
        <td>Which country was the first Asian country to reach the semi-final stage of the World Cup?</td>
        <td>Korea</td>
    </tr>
    <tr>
        <td>Which country was the first to invent perfume?</td>
        <td>Cyprus</td>
    </tr>
    <tr>
        <td>Which country won the 2018 World Cup held in Russia?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Which country’s team has played in every World Cup?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>Which creature decoration from Greek mythology can governors construct in their city?</td>
        <td>Pegasus</td>
    </tr>
    <tr>
        <td>Which deity of ancient Egyptian mythology was represented by the obelisk?</td>
        <td>Ra</td>
    </tr>
    <tr>
        <td>Which demigod captures Cerberus, the “three-headed hound of Hades”, according to Greek mythology?</td>
        <td>Hercules</td>
    </tr>
    <tr>
        <td>Which Dutch scientist is known as “the father of microbiology”?</td>
        <td>Antonie van Leeuwenhoek</td>
    </tr>
    <tr>
        <td>Which Egyptian city marked the boundary between Upper Egypt and Lower Egypt during the pre-dynastic
            period?
        </td>
        <td>Memphis</td>
    </tr>
    <tr>
        <td>Which emperor issued the Proclamation of Milan?</td>
        <td>Constantine I</td>
    </tr>
    <tr>
        <td>Which emperor was named the “Khan of Heaven” by various Turkic nomads during the Sui and Tang dynasties?
        </td>
        <td>Emperor Taizong of Tang</td>
    </tr>
    <tr>
        <td>Which entomologist and astronomer from New Zealand first proposed adopting a method of Daylight Saving Time?
        </td>
        <td>George Hudson</td>
    </tr>
    <tr>
        <td>Which ethnic group founded the earliest civilization of Mesopotamia?</td>
        <td>The Sumerians</td>
    </tr>
    <tr>
        <td>Which European country’s king funded Christopher Columbus’ famous expedition?</td>
        <td>Spain</td>
    </tr>
    <tr>
        <td>Which European wrote the first travelogue detailing China’s history, culture, and art?</td>
        <td>Marco Polo</td>
    </tr>
    <tr>
        <td>Which famous and influential family did the famous Renaissance art patron Duke Lorenzo belong to?</td>
        <td>The House of Medici</td>
    </tr>
    <tr>
        <td>Which famous Japanese samurai was called “Ushiwakamaru” as a child?</td>
        <td>Minamoto no Yoshitsune</td>
    </tr>
    <tr>
        <td>Which famous sculpture features a child urinating?</td>
        <td>Manneken Pis</td>
    </tr>
    <tr>
        <td>Which figure of history disguised himself as a Prussian captain and made off with over 4000 gold marks only
            to
            be later regarded as a folk hero and called the “Captain of Köpenick”?
        </td>
        <td>Wilhelm Voigt</td>
    </tr>
    <tr>
        <td>Which flavor of soup can does NOT appear in Andy Warhol’s famous work titled “Campbell’s Soup Cans”?</td>
        <td>Corn</td>
    </tr>
    <tr>
        <td>Which French monarch was known as “Saint Louis”?</td>
        <td>Louis IX</td>
    </tr>
    <tr>
        <td>Which gas makes up more than 90% of Mars’ atmosphere?</td>
        <td>Carbon Dioxide</td>
    </tr>
    <tr>
        <td>Which German psychologist discovered the human brain’s forgetting curve?</td>
        <td>Hermann Ebbinghaus</td>
    </tr>
    <tr>
        <td>Which goalkeeper has scored the most goals in professional soccer?</td>
        <td>Rogério Ceni</td>
    </tr>
    <tr>
        <td>Which god is Taranis in Celtic mythology?</td>
        <td>Thunder God</td>
    </tr>
    <tr>
        <td>Which Greek goddess is portrayed on the Jules Rimet trophy, the original World Cup trophy?</td>
        <td>Nike</td>
    </tr>
    <tr>
        <td>Which inventor built the world’s first electric locomotive in 1837?</td>
        <td>Robert Davidson</td>
    </tr>
    <tr>
        <td>Which is Minamoto no Yoshitsune’s active skill?</td>
        <td>Kyohachiryu</td>
    </tr>
    <tr>
        <td>Which is recognized by the Guinness Book of World Records as having the largest cumulative circulation of
            any
            single-author comic?
        </td>
        <td>One Piece</td>
    </tr>
    <tr>
        <td>Which is the highest plateau in the world?</td>
        <td>The Tibetan Plateau</td>
    </tr>
    <tr>
        <td>Which is the longest continental mountain range on earth?</td>
        <td>The Andes</td>
    </tr>
    <tr>
        <td>Which is the smallest known snake species?</td>
        <td>Barbados threadsnake</td>
    </tr>
    <tr>
        <td>Which is the Viking civilization’s special unit?</td>
        <td>Berserker</td>
    </tr>
    <tr>
        <td>Which is the world’s largest church?</td>
        <td>St. Peter’s Basilica</td>
    </tr>
    <tr>
        <td>Which Japanese haiku poet wrote under the pseudonym “Tosei”?</td>
        <td>Matsuo Basho</td>
    </tr>
    <tr>
        <td>Which kingdom did the commander Boudica reign over?</td>
        <td>Iceni</td>
    </tr>
    <tr>
        <td>Which landmark in Sydney, Australia is shaped like sails?</td>
        <td>Sydney Opera House</td>
    </tr>
    <tr>
        <td>Which mammalian order does Suidae belong to?</td>
        <td>Artiodactyla</td>
    </tr>
    <tr>
        <td>Which manager has managed in six World Cups?</td>
        <td>Carlos Alberto Parreira</td>
    </tr>
    <tr>
        <td>Which manager led his team to a record 11 consecutive World Cup match victories?</td>
        <td>Luiz Felipe Scolari</td>
    </tr>
    <tr>
        <td>Which military commander was known as the “Teutoburg’s Liberator”?</td>
        <td>Hermann</td>
    </tr>
    <tr>
        <td>Which military leader was known as the “Reformer of the Legions”?</td>
        <td>Gaius Marius</td>
    </tr>
    <tr>
        <td>Which modern-day city was Shakespeare’s Globe Theater built in?</td>
        <td>London</td>
    </tr>
    <tr>
        <td>Which monarch built the Hagia Sophia?</td>
        <td>Justinian I</td>
    </tr>
    <tr>
        <td>Which musical instrument is a national emblem of Ireland?</td>
        <td>Harp</td>
    </tr>
    <tr>
        <td>Which musician composed “The Marriage of Figaro”?</td>
        <td>Mozart</td>
    </tr>
    <tr>
        <td>Which Nobel Prize is the most recently established?</td>
        <td>The Nobel Prize in Economics</td>
    </tr>
    <tr>
        <td>Which of the Babylonian Codes of Law was the first full set of written laws in recorded history?</td>
        <td>The Code of Hammurabi</td>
    </tr>
    <tr>
        <td>Which of the below figures were not Knights of King Arthur’s Round Table?</td>
        <td>Merlin</td>
    </tr>
    <tr>
        <td>Which of the below states was not part of the Three Kingdoms period?</td>
        <td>Jin</td>
    </tr>
    <tr>
        <td>Which of the following accurately describes the Arctic Circle?</td>
        <td>Polar Day and Polar Night are phenomenons that occur there</td>
    </tr>
    <tr>
        <td>Which of the following achievements must be completed before the “Special Snowflake” achievement?</td>
        <td>Long-range Strike</td>
    </tr>
    <tr>
        <td>Which of the following actions is NOT available to an alliance leader in Rise of Kingdoms?</td>
        <td>Quit Alliance</td>
    </tr>
    <tr>
        <td>Which of the following animals does NOT lay eggs?</td>
        <td>Bat</td>
    </tr>
    <tr>
        <td>Which of the following are carnivores?</td>
        <td>Dragonflies</td>
    </tr>
    <tr>
        <td>Which of the following are NOT mammals?</td>
        <td>Turtle</td>
    </tr>
    <tr>
        <td>Which of the following awards daily activity points?</td>
        <td>Defeating barbarian troops on the map</td>
    </tr>
    <tr>
        <td>Which of the following best describes a Clownfish?</td>
        <td>Tropical marine fish</td>
    </tr>
    <tr>
        <td>Which of the following buffs can take effect during Sunset Canyon battles?</td>
        <td>Buffs from equipment</td>
    </tr>
    <tr>
        <td>Which of the following buffs comes from levelling up the required structure to level 25?</td>
        <td>Transportation March Speed Bonus</td>
    </tr>
    <tr>
        <td>Which of the following buildings cannot be upgraded?</td>
        <td>Courier Station</td>
    </tr>
    <tr>
        <td>Which of the following can NOT be found in a Silver Chest at the Tavern?</td>
        <td>Lv.3 Tome of Knowledge</td>
    </tr>
    <tr>
        <td>Which of the following can NOT be found in Alliance Shops?</td>
        <td>Golden Key</td>
    </tr>
    <tr>
        <td>Which of the following CANNOT be equipped to a Commander?</td>
        <td>Lohar’s Longbow</td>
    </tr>
    <tr>
        <td>Which of the following cat breeds is known for being hairless?</td>
        <td>Sphynx</td>
    </tr>
    <tr>
        <td>Which of the following characters are NOT present in the “Journey to the West” story?</td>
        <td>Third Sister Hu</td>
    </tr>
    <tr>
        <td>Which of the following cities is the capital of France?</td>
        <td>Paris</td>
    </tr>
    <tr>
        <td>Which of the following cities was the first to have an underground rapid transit system?</td>
        <td>London</td>
    </tr>
    <tr>
        <td>Which of the following city buildings CAN be upgraded?</td>
        <td>Trading Post</td>
    </tr>
    <tr>
        <td>Which of the following civilizations cannot buff their cavalry?</td>
        <td>The Ottomans</td>
    </tr>
    <tr>
        <td>Which of the following civilizations give a 5% attack bonus to cavalry units?</td>
        <td>Germany</td>
    </tr>
    <tr>
        <td>Which of the following civilizations gives a 20% healing speed bonus to hospitals?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Which of the following civilizations increases the damage of active commander skills by 5%?</td>
        <td>Ottomans</td>
    </tr>
    <tr>
        <td>Which of the following civilizations provides a building speed boost?</td>
        <td>China</td>
    </tr>
    <tr>
        <td>Which of the following commanders excels at attacking enemy cities?</td>
        <td>Julius Caesar</td>
    </tr>
    <tr>
        <td>Which of the following commanders excels at gathering resources?</td>
        <td>Cleopatra VII</td>
    </tr>
    <tr>
        <td>Which of the following commanders excels at leading archers?</td>
        <td>Yi Seong-Gye</td>
    </tr>
    <tr>
        <td>Which of the following commanders excels at leading cavalry?</td>
        <td>Minamoto no Yoshitsune</td>
    </tr>
    <tr>
        <td>Which of the following commanders excels at leading infantry?</td>
        <td>Richard I</td>
    </tr>
    <tr>
        <td>Which of the following commanders joined a rebellion against her own husband?</td>
        <td>Constance</td>
    </tr>
    <tr>
        <td>Which of the following commanders was effectively the ruler of their kingdom, but never officially the head
            of
            state?
        </td>
        <td>Charles Martel</td>
    </tr>
    <tr>
        <td>Which of the following commanders was part of the Battle of Red Cliffs?</td>
        <td>Cao Cao</td>
    </tr>
    <tr>
        <td>Which of the following commanders was part of the Hundred Years’ War?</td>
        <td>Joan of Arc</td>
    </tr>
    <tr>
        <td>Which of the following commanders’ sculptures cannot be donated to the “Past Glory” event after the Lost
            Kingdom
            has been opened?
        </td>
        <td>Æthelflæd</td>
    </tr>
    <tr>
        <td>Which of the following countries is located entirely in the Northern Hemisphere?</td>
        <td>None of these answers are correct</td>
    </tr>
    <tr>
        <td>Which of the following countries or regions is NOT part of Northern Europe?</td>
        <td>Argentina</td>
    </tr>
    <tr>
        <td>Which of the following deities were NOT meant to be given offerings at the Abu Simbel Temples?</td>
        <td>Anubis</td>
    </tr>
    <tr>
        <td>Which of the following describes a monotheist?</td>
        <td>Someone who warships 1 god</td>
    </tr>
    <tr>
        <td>Which of the following descriptions of microscopes is false?</td>
        <td>They can be used to observe stars</td>
    </tr>
    <tr>
        <td>Which of the following did NOT occur during the Greco-Persian Wars?</td>
        <td>The Battle of Agincourt</td>
    </tr>
    <tr>
        <td>Which of the following doctors was the first to describe completely, and in detail, the systemic circulation
            and
            the function of the heart?
        </td>
        <td>William Harvey</td>
    </tr>
    <tr>
        <td>Which of the following does not award Individual Credits?</td>
        <td>Giving resources to allies</td>
    </tr>
    <tr>
        <td>Which of the following does NOT award Individual Credits?</td>
        <td>Attacking Barbarians</td>
    </tr>
    <tr>
        <td>Which of the following does NOT give Commanders EXP?</td>
        <td>Attacking other players</td>
    </tr>
    <tr>
        <td>Which of the following does NOT increase an Alliance’s member limit?</td>
        <td>Appointing officers</td>
    </tr>
    <tr>
        <td>Which of the following French Kings was known as the Sun King?</td>
        <td>Louis XIV</td>
    </tr>
    <tr>
        <td>Which of the following fruits, know as “wolf peach”, were erroneously thought to be poisonous?</td>
        <td>Tomatoes</td>
    </tr>
    <tr>
        <td>Which of the following illnesses can be caused by a lack of Vitamin D?</td>
        <td>Rickets</td>
    </tr>
    <tr>
        <td>Which of the following inventions did NOT come about during the first industrial Revolution?</td>
        <td>The telephone</td>
    </tr>
    <tr>
        <td>Which of the following is a decorative building?</td>
        <td>Stone Drum</td>
    </tr>
    <tr>
        <td>Which of the following is a fossil fuel formed from marine organisms that is often found in folded or tilted
            layers and used for heating and cooking?
        </td>
        <td>Natural gas</td>
    </tr>
    <tr>
        <td>Which of the following is a liquid fossil fuel formed from marine organisms that is burned to obtain energy
            and
            used to manufacture plastics?
        </td>
        <td>Oil</td>
    </tr>
    <tr>
        <td>Which of the following is a military building?</td>
        <td>Scout Camp</td>
    </tr>
    <tr>
        <td>Which of the following is a name for sedimentary rock formed from decayed plant material in swampy areas?
        </td>
        <td>Coal</td>
    </tr>
    <tr>
        <td>Which of the following is a phase of The Mightiest Governor event?</td>
        <td>Enemy Elimination</td>
    </tr>
    <tr>
        <td>Which of the following is an alternative energy source based on splitting heavy atoms into lighter ones to
            release energy?
        </td>
        <td>Nuclear energy</td>
    </tr>
    <tr>
        <td>Which of the following is an economic building?</td>
        <td>Alliance Center</td>
    </tr>
    <tr>
        <td>Which of the following is an inexhaustible energy resource that relies on hot magma or hot, dry rocks below
            ground?
        </td>
        <td>Geothermal energy</td>
    </tr>
    <tr>
        <td>Which of the following is another name for the “Black Death” that raged across the Europe during the Late
            Middle
            Ages?
        </td>
        <td>Yersinia pestis</td>
    </tr>
    <tr>
        <td>Which of the following is considered a metallic resource?</td>
        <td>Iron</td>
    </tr>
    <tr>
        <td>Which of the following is considered one of Confucius’ disciples?</td>
        <td>Yan Hui</td>
    </tr>
    <tr>
        <td>Which of the following is extinct?</td>
        <td>The Steller’s Sea Cow</td>
    </tr>
    <tr>
        <td>Which of the following is NOT a “low country” of Europe?</td>
        <td>The United Kingdom</td>
    </tr>
    <tr>
        <td>Which of the following is NOT a Champions of Olympia map?</td>
        <td>Treasure Hill</td>
    </tr>
    <tr>
        <td>Which of the following is not a characteristic of Renaissance art?</td>
        <td>Flat/Two-dimensional</td>
    </tr>
    <tr>
        <td>Which of the following is NOT a component ingredient used to forge the Epic weapon “Golden Age”?</td>
        <td>Animal Bone</td>
    </tr>
    <tr>
        <td>Which of the following is NOT a King skill?</td>
        <td>Relief</td>
    </tr>
    <tr>
        <td>Which of the following is not a natural resource?</td>
        <td>Clothing</td>
    </tr>
    <tr>
        <td>Which of the following is NOT a prerequisite for the Military Technology Cartography?</td>
        <td>Ballistics</td>
    </tr>
    <tr>
        <td>Which of the following is not a reason why the Renaissance began in Italy?</td>
        <td>The Black Plague did not hit Italy as a result of the Alps</td>
    </tr>
    <tr>
        <td>Which of the following is NOT a suit in poker?</td>
        <td>Winds</td>
    </tr>
    <tr>
        <td>Which of the following is NOT an accurate description of the “Manila galleons”?</td>
        <td>They were used for enemy reconnaissance</td>
    </tr>
    <tr>
        <td>Which of the following is NOT an accurate description of the ancient Kingdom of Kush?</td>
        <td>It did not possess its own unique language and script</td>
    </tr>
    <tr>
        <td>Which of the following is not an objective of Greek mythology?</td>
        <td>Provide a path for salvation</td>
    </tr>
    <tr>
        <td>Which of the following is NOT considered a single-celled organism?</td>
        <td>Mites</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Belisarius’s skills?</td>
        <td>Thunderous Force</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of El Cid’s skills?</td>
        <td>Chivalry</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Hermann’s skills?</td>
        <td>Irresistible</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Julius Caesar’s skills?</td>
        <td>Military Life</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Keira’s skills?</td>
        <td>Unruly Blood</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Lohar’s skills?</td>
        <td>Utter Annihilation</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Matilda of Flanders’ skills?</td>
        <td>Celtic Blood</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Mulan’s skills?</td>
        <td>Indomitable Army</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Osman I’s skills</td>
        <td>Oblique Tactics</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of Pelagius’ passive skills?</td>
        <td>Charge</td>
    </tr>
    <tr>
        <td>Which of the following is not one of the Fates of Norse mythology?</td>
        <td>Karma</td>
    </tr>
    <tr>
        <td>Which of the following is NOT one of the major greenhouse gases in earth’s atmosphere?</td>
        <td>Nitrogen</td>
    </tr>
    <tr>
        <td>Which of the following is NOT part of Mexico’s flag?</td>
        <td>Camel</td>
    </tr>
    <tr>
        <td>Which of the following is the correct description for the stratosphere?</td>
        <td>It is warm above and cold below</td>
    </tr>
    <tr>
        <td>Which of the following is the world’s first national park?</td>
        <td>Yellowstone National Park</td>
    </tr>
    <tr>
        <td>Which of the following is the world’s largest island?</td>
        <td>Greenland</td>
    </tr>
    <tr>
        <td>Which of the following is Zeus’ elder brother in Greek mythology?</td>
        <td>Poseidon</td>
    </tr>
    <tr>
        <td>Which of the following juice is contained in the classic cocktail “Bloody Mary”?</td>
        <td>Tomato Juice</td>
    </tr>
    <tr>
        <td>Which of the following kingdom title descriptions is incorrect?</td>
        <td>Dukes’ troops have increased attack</td>
    </tr>
    <tr>
        <td>Which of the following kingdom title descriptions is incorrect?</td>
        <td>Prime Ministers’ troops have increased attack</td>
    </tr>
    <tr>
        <td>Which of the following marine species has existed the longest?</td>
        <td>Nautilus</td>
    </tr>
    <tr>
        <td>Which of the following musical instruments has six strings?</td>
        <td>Guitar</td>
    </tr>
    <tr>
        <td>Which of the following nations is the closest to the North Pole?</td>
        <td>Finland</td>
    </tr>
    <tr>
        <td>Which of the following penalties may be applied in response to the use of third-party software?</td>
        <td>Battle restrictions</td>
    </tr>
    <tr>
        <td>Which of the following physicists was born on the death anniversary of Galileo Galilei, and died on the
            birth
            anniversary of Albert Einstein?
        </td>
        <td>Stephen Hawking</td>
    </tr>
    <tr>
        <td>Which of the following plants is visible in Mulan and Cao Cao’s background image on the commander screen?
        </td>
        <td>Bamboo</td>
    </tr>
    <tr>
        <td>Which of the following resources cannot be found in your jeans?</td>
        <td>Lead</td>
    </tr>
    <tr>
        <td>Which of the following rewards CANNOT be obtained from a Barbarian Keep in Rise of Kingdoms?</td>
        <td>Commander Sculptures</td>
    </tr>
    <tr>
        <td>Which of the following serves as the national flower of the Philippines?</td>
        <td>Sampaguita</td>
    </tr>
    <tr>
        <td>Which of the following speeches was made by the Prussian prime Minister Otto von Bismarck in September of
            1862?
        </td>
        <td>Blood and Iron</td>
    </tr>
    <tr>
        <td>Which of the following Speedups are NOT available in Rise of Kingdoms?</td>
        <td>6-Hour Speedup</td>
    </tr>
    <tr>
        <td>Which of the following statements about the Battle of Baideng is untrue?</td>
        <td>It was a battle between the Tang dynasty and the Xiongnu</td>
    </tr>
    <tr>
        <td>Which of the following stories is not one of Grimms’ Fairy Tales?</td>
        <td>The Little Mermaid</td>
    </tr>
    <tr>
        <td>Which of the following technologies precedes “Horsemanship”?</td>
        <td>Military Discipline</td>
    </tr>
    <tr>
        <td>Which of the following vitamins is most helpful for maintaining good vision?</td>
        <td>Vitamin A</td>
    </tr>
    <tr>
        <td>Which of the following was a general during Japan’s Genpei War?</td>
        <td>Tomoe Gozen</td>
    </tr>
    <tr>
        <td>Which of the following was a Knight of the Round Table?</td>
        <td>Lancelot</td>
    </tr>
    <tr>
        <td>Which of the following was a Renaissance intellectual movement in which thinkers studied classical texts and
            focused on human potential and achievements?
        </td>
        <td>Humanism</td>
    </tr>
    <tr>
        <td>Which of the following was also known as the Eastern Roman Empire?</td>
        <td>The Byzantine Empire</td>
    </tr>
    <tr>
        <td>Which of the following was an enemy of Richard the Lionheart?</td>
        <td>Saladin</td>
    </tr>
    <tr>
        <td>Which of the following was built at the order of Shah Jahan in the memory of his wife Mumtaz Mahal?</td>
        <td>The Taj Mahal</td>
    </tr>
    <tr>
        <td>Which of the following was not a pillar of the Renaissance?</td>
        <td>Mannerism</td>
    </tr>
    <tr>
        <td>Which of the following was NOT essential for a Vizier, the Pharaoh’s most senior civil servant, in ancient
            Egypt?
        </td>
        <td>To have royal blood</td>
    </tr>
    <tr>
        <td>Which of the following was praised by his countrymen as Ancient Greece’s best and most honest democratic
            representatives?
        </td>
        <td>Pericles</td>
    </tr>
    <tr>
        <td>Which of the following was the highest-ranked medal in the Korean Empire?</td>
        <td>Order of the Gold Cheok</td>
    </tr>
    <tr>
        <td>Which of the following was written by Mary Shelley?</td>
        <td>Frankenstein</td>
    </tr>
    <tr>
        <td>Which of the following were born in the same year as singer Luciano Pavarotti?</td>
        <td>Elvis Presley</td>
    </tr>
    <tr>
        <td>Which of the following will NOT prevent you from entering an Ark of Osiris Battlefield?</td>
        <td>Activating an Attack Enhancement</td>
    </tr>
    <tr>
        <td>Which of the following works is based on the Battle of Roncevaux Pass, fought by Charlemagne and his
            knights?
        </td>
        <td>“The Song of Roland”</td>
    </tr>
    <tr>
        <td>Which of the following works was NOT written by the musical maestro Andrew Lloyd Webber?</td>
        <td>Les Misérables</td>
    </tr>
    <tr>
        <td>Which of the following works was written by the French playwright Pierre Corneille?</td>
        <td>Le Cid</td>
    </tr>
    <tr>
        <td>Which of the following works was written by Victor Hugo?</td>
        <td>The Hunchback of Notre-Dame</td>
    </tr>
    <tr>
        <td>Which of the scientific bird names was also used to commemorate the wife of Napoleon III, the Empress of
            France,
            Eugénie de Montijo?
        </td>
        <td>The white-headed fruit dove</td>
    </tr>
    <tr>
        <td>Which of the seven wonders of the world were created to cure the new Queen of Babylon’s homesickness?</td>
        <td>The Hanging Gardens</td>
    </tr>
    <tr>
        <td>Which of these achievements is earned from healing 100,000 injured troops in the hospital?</td>
        <td>Rebuilding</td>
    </tr>
    <tr>
        <td>Which of these active skills belongs to Charles Martel?</td>
        <td>Immortal Shield</td>
    </tr>
    <tr>
        <td>Which of these active skills belongs to Charles Martel?</td>
        <td>Shield of Francia</td>
    </tr>
    <tr>
        <td>Which of these airports has runways on a beach?</td>
        <td>Barra Airport</td>
    </tr>
    <tr>
        <td>Which of these alliance officers bestows a Research Speed boost?</td>
        <td>Counselor</td>
    </tr>
    <tr>
        <td>Which of these Alliance Skills increases damage versus barbarians and other neutral units?</td>
        <td>Unbridled Progress</td>
    </tr>
    <tr>
        <td>Which of these ancient Greek philosophers did NOT participate in the discussion on justice in Plato’s
            Republic?
        </td>
        <td>Protagoras</td>
    </tr>
    <tr>
        <td>Which of these animated movies was NOT directed by Hayao Miyazaki?</td>
        <td>Paprika</td>
    </tr>
    <tr>
        <td>Which of these areas contains a waterwheel?</td>
        <td>Alliance Cropland</td>
    </tr>
    <tr>
        <td>Which of these artists was NOT a Baroque painter?</td>
        <td>Vincent van Gogh</td>
    </tr>
    <tr>
        <td>Which of these Avatar Frames are available for purchase?</td>
        <td>Rose Petals</td>
    </tr>
    <tr>
        <td>Which of these Avatar Frames can be obtained from the Ceroli Crisis event?</td>
        <td>Undersea Kingdom</td>
    </tr>
    <tr>
        <td>Which of these Blessings immediately inflicts a 10% loss of troop power on your enemy at the start of battle
            in
            the “Golden Kingdom” event?
        </td>
        <td>Anchor</td>
    </tr>
    <tr>
        <td>Which of these boosts work in Expeditions in Rise of Kingdoms?</td>
        <td>All of them</td>
    </tr>
    <tr>
        <td>Which of these boosts work in Sunset Canyon?</td>
        <td>VIP Level troop capacity</td>
    </tr>
    <tr>
        <td>Which of these buildings is NOT one of the New Seven Wonders of the World?</td>
        <td>The Palace of Versailles</td>
    </tr>
    <tr>
        <td>Which of these Byzantine emperors ruled alongside four other emperors at various times only later to be
            declared
            Rome’s sole emperor after 63 years in power?
        </td>
        <td>Constantine VIII</td>
    </tr>
    <tr>
        <td>Which of these Ceroli Chieftains is also called Stormbringer?</td>
        <td>Torgny</td>
    </tr>
    <tr>
        <td>Which of these Ceroli Chieftains is also called the Disaster Bringer?</td>
        <td>Dekar</td>
    </tr>
    <tr>
        <td>Which of these Chinese ethnic groups is known for drinking kumis, a drink made from fermented mare’s milk?
        </td>
        <td>Mongols</td>
    </tr>
    <tr>
        <td>Which of these circumstances would prevent you from changing civilization?</td>
        <td>Ongoing troop training in your city</td>
    </tr>
    <tr>
        <td>Which of these cities hosted the Olympics in 1900?</td>
        <td>Paris</td>
    </tr>
    <tr>
        <td>Which of these cities in Brazil has hosted the Olympic Games?</td>
        <td>Rio de Janeiro</td>
    </tr>
    <tr>
        <td>Which of these city themes feature balloons?</td>
        <td>Harvest Cottage</td>
    </tr>
    <tr>
        <td>Which of these City Themes offers a special attributes bonus of +5% to Cavalry Attack and -5% to Archer
            Health?
        </td>
        <td>Sower’s Song</td>
    </tr>
    <tr>
        <td>Which of these civilizations can increase Archer Defense?</td>
        <td>Korea</td>
    </tr>
    <tr>
        <td>Which of these civilizations can increase their healing speed?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Which of these civilizations can increase their Hospital Capacity?</td>
        <td>Byzantium</td>
    </tr>
    <tr>
        <td>Which of these commanders carries a bow and arrows as well as a sword?</td>
        <td>Æthelflæd</td>
    </tr>
    <tr>
        <td>Which of these commanders does NOT carry a shield?</td>
        <td>Charlemagne</td>
    </tr>
    <tr>
        <td>Which of these commanders does NOT carry a sword?</td>
        <td>Theodora</td>
    </tr>
    <tr>
        <td>Which of these commanders features snowflakes in their background image?</td>
        <td>Björn Ironside</td>
    </tr>
    <tr>
        <td>Which of these Commanders gets the ability to convert some slain units into severely wounded units when
            attacking a city?
        </td>
        <td>Charlemagne</td>
    </tr>
    <tr>
        <td>Which of these commanders has also been called a “Holy Overlord”?</td>
        <td>El Cid</td>
    </tr>
    <tr>
        <td>Which of these commanders has an Attack branch on their Talent tree?</td>
        <td>Centurion</td>
    </tr>
    <tr>
        <td>Which of these commanders has NO whiskers or other facial hair?</td>
        <td>Alexander the Great</td>
    </tr>
    <tr>
        <td>Which of these commanders has the Enchantment active skill?</td>
        <td>Šárka</td>
    </tr>
    <tr>
        <td>Which of these commanders is bald?</td>
        <td>Ragnar Lodbrok</td>
    </tr>
    <tr>
        <td>Which of these commanders is NOT shown wearing a cloak?</td>
        <td>Mehmed II</td>
    </tr>
    <tr>
        <td>Which of these commanders wears a ring on their hand?</td>
        <td>Queen Tamar of Georgia</td>
    </tr>
    <tr>
        <td>Which of these commanders’ Expertise Skill is the enhanced version of an existing skill?</td>
        <td>Björn Ironside</td>
    </tr>
    <tr>
        <td>Which of these commanders’ sculptures can be obtained from opening a chest in the Tavern in Rise of
            Kingdoms?
        </td>
        <td>Charles Martel</td>
    </tr>
    <tr>
        <td>Which of these consonants is trilled when pronounced in Italian?</td>
        <td>r</td>
    </tr>
    <tr>
        <td>Which of these countries did NOT participate in the Northern Seven Years’ War (1563-1570)?</td>
        <td>The United Kingdom</td>
    </tr>
    <tr>
        <td>Which of these countries does the Prime Meridian pass through?</td>
        <td>Mali</td>
    </tr>
    <tr>
        <td>Which of these countries has had the most influence on the development of Cuban food?</td>
        <td>Spain</td>
    </tr>
    <tr>
        <td>Which of these countries was NOT a founding member of NATO (in 1949)?</td>
        <td>Turkey</td>
    </tr>
    <tr>
        <td>Which of these creatures was also popularly used as a kind of personal protective talisman in ancient Egypt?
        </td>
        <td>Scarabs</td>
    </tr>
    <tr>
        <td>Which of these decorations does NOT cost Gems?</td>
        <td>Sakura Tree</td>
    </tr>
    <tr>
        <td>Which of these decorative buildings is also considered an omen of good luck in Chinese culture?</td>
        <td>Chinese Dragon</td>
    </tr>
    <tr>
        <td>Which of these effects is NOT caused by Æthelflæd’s skills?</td>
        <td>Healing slightly wounded units</td>
    </tr>
    <tr>
        <td>Which of these events is NOT part of the Olympic Games?</td>
        <td>Car racing</td>
    </tr>
    <tr>
        <td>Which of these female commanders has been described as “The Georgian Will”?</td>
        <td>Queen Tamar of Georgia</td>
    </tr>
    <tr>
        <td>Which of these female commanders has flowers in their hair?</td>
        <td>Boudica</td>
    </tr>
    <tr>
        <td>Which of these fields is NOT included in the list of Nobel Prizes?</td>
        <td>Mathematics</td>
    </tr>
    <tr>
        <td>Which of these figures of the Seleucid Empire was taken hostage by Rome, subsequently fled Rome, and then
            returned to seize the throne of their home country?
        </td>
        <td>Demetrius I</td>
    </tr>
    <tr>
        <td>Which of these foods could the Eastern Han statesman Cao Cao not have eaten?</td>
        <td>Corn</td>
    </tr>
    <tr>
        <td>Which of these fruits appears in the background image for the Commander Imhotep?</td>
        <td>Banana</td>
    </tr>
    <tr>
        <td>Which of these gods of ancient Egypt was also a symbol of royal authority?</td>
        <td>Horus</td>
    </tr>
    <tr>
        <td>Which of these historical figures was NOT a botanist?</td>
        <td>Dmitry Mendeleev</td>
    </tr>
    <tr>
        <td>Which of these holy sites grants a march speed bonus?</td>
        <td>Sanctum of Wind</td>
    </tr>
    <tr>
        <td>Which of these ingredients is used to brew soy sauce?</td>
        <td>Soybeans</td>
    </tr>
    <tr>
        <td>Which of these is a line of Tomoe Gozen?</td>
        <td>My power scares even the ghosts.</td>
    </tr>
    <tr>
        <td>Which of these is a passive skill of Commander Thutmose III?</td>
        <td>Conquest of Mitanni</td>
    </tr>
    <tr>
        <td>Which of these is an incorrect description of the “Battle of Poitiers”?</td>
        <td>The campaign took place during World War I</td>
    </tr>
    <tr>
        <td>Which of these is an incorrect description of the “Mare Imbrium” on the moon?</td>
        <td>It contains a certain amount of sedimentary water</td>
    </tr>
    <tr>
        <td>Which of these is considered to be the seminal work of the French playwright Molière?</td>
        <td>Tartuffe</td>
    </tr>
    <tr>
        <td>Which of these is found in the alliance Golden Chest?</td>
        <td>1,000 Individual Credits</td>
    </tr>
    <tr>
        <td>Which of these is NOT a mail report category?</td>
        <td>Gathering</td>
    </tr>
    <tr>
        <td>Which of these is NOT a passive skill of Commander Minamoto no Yoshitsune?</td>
        <td>The Worthy Man</td>
    </tr>
    <tr>
        <td>Which of these is NOT a type of Japanese wagyu</td>
        <td>Ronguhon Wagyu</td>
    </tr>
    <tr>
        <td>Which of these is NOT an Alliance Skill?</td>
        <td>Rapid March</td>
    </tr>
    <tr>
        <td>Which of these is not an ancient civilization of the Americas?</td>
        <td>The Sumerians</td>
    </tr>
    <tr>
        <td>Which of these is not an official song of the 2022 World Cup?</td>
        <td>Tatu Bom de Bola</td>
    </tr>
    <tr>
        <td>Which of these is NOT considered a Shakespearean tragedy?</td>
        <td>Twelfth Night</td>
    </tr>
    <tr>
        <td>Which of these is NOT considered one of the “Five Good Emperors” of the Nerva-Antonine dynasty of ancient
            Rome?
        </td>
        <td>Commodus</td>
    </tr>
    <tr>
        <td>Which of these is NOT considered one of the 8 main styles of contemporary Chinese cooking?</td>
        <td>Huaiyang Cuisine</td>
    </tr>
    <tr>
        <td>Which of these is NOT one of Cao Cao’s skills?</td>
        <td>Battle of Sorrow</td>
    </tr>
    <tr>
        <td>Which of these is NOT one of the skills of Ceroli Chieftain Astrid?</td>
        <td>Flametongue’s Kiss</td>
    </tr>
    <tr>
        <td>Which of these is not one of the Three Gorgon Sisters of Grecian myth?</td>
        <td>Perga</td>
    </tr>
    <tr>
        <td>Which of these is NOT one of the Winning Stratagems listed in the ancient Chinese military textbook, the
            “Thirty-Six Stratagems”?
        </td>
        <td>The “empty fort” strategy</td>
    </tr>
    <tr>
        <td>Which of these is not part of the Holy Trinity in Christianity?</td>
        <td>The Mother</td>
    </tr>
    <tr>
        <td>Which of these is NOT Zeus’ child in Greek mythology?</td>
        <td>Hera</td>
    </tr>
    <tr>
        <td>Which of these is one of Minamoto no Yoshitsune’s “Active Skills”?</td>
        <td>Kyohachiryu</td>
    </tr>
    <tr>
        <td>Which of these is one of the four fundamental elements of nature according to many ancient Greek
            philosophers?
        </td>
        <td>Wind</td>
    </tr>
    <tr>
        <td>Which of these is the most visited art museum in the world in recent years?</td>
        <td>The Louvre</td>
    </tr>
    <tr>
        <td>Which of these is the National Flower of India?</td>
        <td>Lotus</td>
    </tr>
    <tr>
        <td>Which of these is used to rally Alliance troops?</td>
        <td>Castle</td>
    </tr>
    <tr>
        <td>Which of these is Yi Seong-Gye an expert in?</td>
        <td>Garrisoning</td>
    </tr>
    <tr>
        <td>Which of these items can NOT be obtained by defeating barbarians?</td>
        <td>VIP points</td>
    </tr>
    <tr>
        <td>Which of these items can NOT be purchased within the Mysterious Merchant’s Boutique?</td>
        <td>Passport Page</td>
    </tr>
    <tr>
        <td>Which of these items CANNOT be obtained from a Tavern Equipment Chest?</td>
        <td>60-Minute Training Speedup</td>
    </tr>
    <tr>
        <td>Which of these items cannot be obtained from an Equipment Chest?</td>
        <td>Tomes of Knowledge</td>
    </tr>
    <tr>
        <td>Which of these items CANNOT be obtained from the Alliance Stone Chest?</td>
        <td>Silver Keys</td>
    </tr>
    <tr>
        <td>Which of these items contains a helpful record on how an ancient Egyptian can pass into the next world after
            death?
        </td>
        <td>The Book of the Dead</td>
    </tr>
    <tr>
        <td>Which of these items may be selected from the Lvl 3 “Pick One” Resource Chest?</td>
        <td>150,000 Food</td>
    </tr>
    <tr>
        <td>Which of these legendary phrases are NOT attributed to Commander El Cid?</td>
        <td>For honor and empire!</td>
    </tr>
    <tr>
        <td>Which of these managers has managed five World Cup teams?</td>
        <td>Bora Milutinović</td>
    </tr>
    <tr>
        <td>Which of these navigators was the first European to arrive in Brazil?</td>
        <td>Pedro Álvares Cabral</td>
    </tr>
    <tr>
        <td>Which of these Nobel Prizes was awarded to Marie Curie in 1903?</td>
        <td>The Nobel Prize in Physics</td>
    </tr>
    <tr>
        <td>Which of these novels is the first ever Japanese work of literary fiction founded on the legend of King
            Arthur?
        </td>
        <td>Kairo-kō</td>
    </tr>
    <tr>
        <td>Which of these novels was NOT written by Margaret Atwood?</td>
        <td>Flowers for Algernon</td>
    </tr>
    <tr>
        <td>Which of these organizations has its headquarters in Geneva?</td>
        <td>UN High Commissioner for Refugees</td>
    </tr>
    <tr>
        <td>Which of these plants is NOT available as a decorative building?</td>
        <td>Peach Tree</td>
    </tr>
    <tr>
        <td>Which of these Renaissance-era artists was not from Italy?</td>
        <td>Shakespeare</td>
    </tr>
    <tr>
        <td>Which of these rewards will NOT be awarded to the first place season finisher in Sunset Canyon?</td>
        <td>Dazzling Starlight Sculpture</td>
    </tr>
    <tr>
        <td>Which of these rivers flows through Africa?</td>
        <td>The Nile</td>
    </tr>
    <tr>
        <td>Which of these skills belonging to Commander Sun Tzu is enhanced upon Awakening?</td>
        <td>Art of War</td>
    </tr>
    <tr>
        <td>Which of these species of shark is also known as the “Godzilla shark”?</td>
        <td>Dracopristis hoffmanorum</td>
    </tr>
    <tr>
        <td>Which of these stars did not take part in the 2022 World Cup opening ceremony?</td>
        <td>Shakira</td>
    </tr>
    <tr>
        <td>Which of these structures allows scouts and City Defenders to better keep an eye on their surroundings?</td>
        <td>Watchtower</td>
    </tr>
    <tr>
        <td>Which of these technologies can be researched in the Academy to accelerate food gathering on the map?</td>
        <td>Scythe</td>
    </tr>
    <tr>
        <td>Which of these terms was also used by the ancient Egyptians to refer to themselves?</td>
        <td>The cattle of Ra</td>
    </tr>
    <tr>
        <td>Which of these terrain types exerts a slight penalty on troop Defense on the Treasure Hill map in “Champions
            of
            Olympia”?
        </td>
        <td>Whirlwind</td>
    </tr>
    <tr>
        <td>Which of these two were created by Leonardo da Vinci?</td>
        <td>The Last Supper, Mona Lisa</td>
    </tr>
    <tr>
        <td>Which of these unfortunate individuals was the only person ever recorded to suffer from both dwarfism and
            gigantism?
        </td>
        <td>Adam Rainer</td>
    </tr>
    <tr>
        <td>Which of these was NOT a common type of garden in ancient Egypt?</td>
        <td>Public gardens</td>
    </tr>
    <tr>
        <td>Which of these was the only commander to defeat Cyrus the Great in battle?</td>
        <td>Tomyris</td>
    </tr>
    <tr>
        <td>Which of these women is NOT considered one of the ‘Four Beauties’ of ancient China?</td>
        <td>Bao Si</td>
    </tr>
    <tr>
        <td>Which of these works of Japanese Monogatari literature describes the legend of “Kaguya-hime”?</td>
        <td>The Tale of The Bamboo Cutter</td>
    </tr>
    <tr>
        <td>Which of these works was NOT written by the philosopher Friedrich Nietzsche?</td>
        <td>Commentaries on the Gallic War</td>
    </tr>
    <tr>
        <td>Which of these WOULD NOT be a penalty for failing to fully extract all resources from a resource point?</td>
        <td>Suffering a barbarian attack</td>
    </tr>
    <tr>
        <td>Which of these writers is considered representative of absurdism?</td>
        <td>Albert Camus</td>
    </tr>
    <tr>
        <td>Which official measure made it a crime to give Martin Luther food or shelter?</td>
        <td>Edict of Worms</td>
    </tr>
    <tr>
        <td>Which organ of the potato plant do we commonly eat?</td>
        <td>Stem</td>
    </tr>
    <tr>
        <td>Which Paris Saint-Germain player plays Rise of Kingdoms?</td>
        <td>Marquinhos</td>
    </tr>
    <tr>
        <td>Which period of European history, lasting from 1300 to 1600, saw renewed interest in classical culture and
            led
            to far-reaching changes in art, learning, and views of the world?
        </td>
        <td>The Renaissance</td>
    </tr>
    <tr>
        <td>Which Pharaoh constructed the Bent Pyramid?</td>
        <td>Sneferu</td>
    </tr>
    <tr>
        <td>Which Pharaoh issued the edict that appears on the Rosetta Stone?</td>
        <td>Ptolemy V</td>
    </tr>
    <tr>
        <td>Which Pharaoh ordered the construction of the Abu Simbel temples?</td>
        <td>Ramesses II</td>
    </tr>
    <tr>
        <td>Which philosopher said “whereof one cannot speak, thereof one must be silent”?</td>
        <td>Wittgenstein</td>
    </tr>
    <tr>
        <td>Which philosopher wrote “Utopia”?</td>
        <td>Plato</td>
    </tr>
    <tr>
        <td>Which planet in our solar system rotates the fastest?</td>
        <td>Jupiter</td>
    </tr>
    <tr>
        <td>Which poet wrote “Germany. A Winter’s Tale”?</td>
        <td>Heine</td>
    </tr>
    <tr>
        <td>Which Portuguese explorer sailed from the Cape of Good Hope to India?</td>
        <td>Vasco da Gama</td>
    </tr>
    <tr>
        <td>Which Portuguese explorer was the first European to sail to the southern tip of Africa?</td>
        <td>Bartolomeu Dias</td>
    </tr>
    <tr>
        <td>Which privilege is reserved for titled officers (not normal officers)?</td>
        <td>The ability to check alliance members’ online status</td>
    </tr>
    <tr>
        <td>Which prominent statesman served as the first general of the Edo shogunate at the end of Japan’s Warring
            States
            period?
        </td>
        <td>Tokugawa Ieyasu</td>
    </tr>
    <tr>
        <td>Which pyramid is widely considered the only surviving Wonder of the Ancient World still with us today?</td>
        <td>The Pyramid of Khufu</td>
    </tr>
    <tr>
        <td>Which rare animal does the WWF use as its logo?</td>
        <td>Panda</td>
    </tr>
    <tr>
        <td>Which Roman Emperor was named Augustus after ending a civil war?</td>
        <td>Gaius Octavius</td>
    </tr>
    <tr>
        <td>Which school of painting would Georges Seurat be a representative of?</td>
        <td>Pointillism</td>
    </tr>
    <tr>
        <td>Which school of thought was founded by the Chinese philosopher Lao Tzu?</td>
        <td>Taoism</td>
    </tr>
    <tr>
        <td>Which scout attribute CANNOT be enhanced by upgrading a Lvl 11 Scout Camp?</td>
        <td>Number of Scouts</td>
    </tr>
    <tr>
        <td>Which script was the Code of Hammurabi engraved onto stone in?</td>
        <td>Cuneiform</td>
    </tr>
    <tr>
        <td>Which sculpture was once described by Mark Twain as the “the most mournful and moving piece of stone in the
            world”?
        </td>
        <td>The Lion of Lucerne</td>
    </tr>
    <tr>
        <td>Which star played in both the 1994 and 2010 World Cup?</td>
        <td>Rigobert Song</td>
    </tr>
    <tr>
        <td>Which star was the oldest player to score a goal in the World Cup?</td>
        <td>Roger Milla</td>
    </tr>
    <tr>
        <td>Which state created the earliest form of paper money?</td>
        <td>Ancient China</td>
    </tr>
    <tr>
        <td>Which state is home to the Walt Disney Company?</td>
        <td>California</td>
    </tr>
    <tr>
        <td>Which state was the first to invent and use papyrus as a writing material?</td>
        <td>Egypt</td>
    </tr>
    <tr>
        <td>Which team has only played a single match in the World Cup?</td>
        <td>Indonesia</td>
    </tr>
    <tr>
        <td>Which team made their World Cup debut in 2022?</td>
        <td>Qatar</td>
    </tr>
    <tr>
        <td>Which team was the only former World Cup winner to fail to qualify for the 2022 World Cup?</td>
        <td>Italy</td>
    </tr>
    <tr>
        <td>Which team won 4th place in the 2018 World Cup?</td>
        <td>England</td>
    </tr>
    <tr>
        <td>Which technology comes right before Heavy Cavalry?</td>
        <td>Scale Armor</td>
    </tr>
    <tr>
        <td>Which technology comes right before Pavise?</td>
        <td>Bodkin Arrows</td>
    </tr>
    <tr>
        <td>Which technology comes right before Swordsman?</td>
        <td>Iron Working</td>
    </tr>
    <tr>
        <td>Which three countries will host the 2026 World Cup?</td>
        <td>USA, Canada, and Mexico</td>
    </tr>
    <tr>
        <td>Which title did Pierre de Coubertin, founder of the International Olympic Committee, hold?</td>
        <td>Baron</td>
    </tr>
    <tr>
        <td>Which tribe claims ownership of the Takra Forest, which must be traversed to reach the Kingdom of Karaku?
        </td>
        <td>The Bloodfury clan</td>
    </tr>
    <tr>
        <td>Which trio of strikers comes from 3 different countries?</td>
        <td>Messi, Suarez, Neymar</td>
    </tr>
    <tr>
        <td>Which troop type receives a boost from the Hidden Lotus decoration?</td>
        <td>Cavalry</td>
    </tr>
    <tr>
        <td>Which troop types will receive a boost from the Easter Party city theme?</td>
        <td>Archer</td>
    </tr>
    <tr>
        <td>Which two animals symbolically decorated the pschent double crown worn by the Pharaohs?</td>
        <td>The cobra and vulture</td>
    </tr>
    <tr>
        <td>Which two cities does Portugal’s Dom Luís I Bridge connect?</td>
        <td>Porto, Vila Nova de Gaia</td>
    </tr>
    <tr>
        <td>Which two English players scored in the 1966 World Cup Final between England and Germany?</td>
        <td>Martin Peters &amp; Geoff Hurst</td>
    </tr>
    <tr>
        <td>Which two oceans does the Panama Canal connect?</td>
        <td>The Pacific and Atlantic Oceans</td>
    </tr>
    <tr>
        <td>Which two sides fought each other in the Battle of Mikatagahara?</td>
        <td>Takeda Shingen and Tokugawa Ieyasu</td>
    </tr>
    <tr>
        <td>Which two teams made their World Cup debuts in 2018?</td>
        <td>Iceland &amp; Panama</td>
    </tr>
    <tr>
        <td>Which type of holy sites grant troop attack bonuses?</td>
        <td>Flame Altar</td>
    </tr>
    <tr>
        <td>Which type of natural disaster is measured using the Richter scale?</td>
        <td>Earthquake</td>
    </tr>
    <tr>
        <td>Which type of stone did the ancient Egyptians extract mainly from the Nile delta?</td>
        <td>Limestone</td>
    </tr>
    <tr>
        <td>Which unit gains a strong benefit from Sun Tzu’s “Master Strategist” skill?</td>
        <td>Infantry</td>
    </tr>
    <tr>
        <td>Which unit type does Cao Cao excel in leading?</td>
        <td>Cavalry</td>
    </tr>
    <tr>
        <td>Which unit type is strong against Archers?</td>
        <td>Cavalry</td>
    </tr>
    <tr>
        <td>Which unit type is strong against Cavalry?</td>
        <td>Infantry</td>
    </tr>
    <tr>
        <td>Which unit type is strong against Infantry?</td>
        <td>Archers</td>
    </tr>
    <tr>
        <td>Which vitamin deficiency can cause night blindness?</td>
        <td>Vitamin A</td>
    </tr>
    <tr>
        <td>Which war ended the rule of the House of Plantagenet and began the rule of the Tudors in England?</td>
        <td>The Wars of the Roses</td>
    </tr>
    <tr>
        <td>Which war was fought between the British houses of Lancaster and York for the throne of England?</td>
        <td>The Wars of the Roses</td>
    </tr>
    <tr>
        <td>Which was a lighthouse built after Macedonia conquered Egypt and was one of the seven wonders of the ancient
            world?
        </td>
        <td>The Lighthouse of Alexandria</td>
    </tr>
    <tr>
        <td>Which was the actor Charlie Chaplin’s first ever film?</td>
        <td>Making a Living</td>
    </tr>
    <tr>
        <td>Which were the two construction materials most heavily favored by the ancient Egyptian building industry?
        </td>
        <td>Mudbricks and stone</td>
    </tr>
    <tr>
        <td>While Gutenberg introduced the printing press in Europe, his invention was influenced by which country, the
            first to develop a moveable type?
        </td>
        <td>China</td>
    </tr>
    <tr>
        <td>Who “owns” Easter Island?</td>
        <td>Chile</td>
    </tr>
    <tr>
        <td>Who authored the novel “No Longer Human”?</td>
        <td>Osamu Dazai</td>
    </tr>
    <tr>
        <td>Who authored the tragedy “Medea”?</td>
        <td>Euripides</td>
    </tr>
    <tr>
        <td>Who came out the victor of the “Battle of Hastings”?</td>
        <td>William the Conqueror</td>
    </tr>
    <tr>
        <td>Who came up with the Trojan Horse idea? Who won the Trojan War?</td>
        <td>Odysseus; The Greeks</td>
    </tr>
    <tr>
        <td>Who commanded the New Model Army founded by Parliament during the English Civil War?</td>
        <td>Cromwell</td>
    </tr>
    <tr>
        <td>Who compiled the fairytales that eventually featured in the “Grimms’ Fairy Tales”?</td>
        <td>The Brothers Grimm</td>
    </tr>
    <tr>
        <td>Who conquered the “Kingdom of the Vandals and Alans” in the Vandalic War?</td>
        <td>Belisarius</td>
    </tr>
    <tr>
        <td>Who created the famous sculpture “The Thinker”?</td>
        <td>Auguste Rodin</td>
    </tr>
    <tr>
        <td>Who defeated the Sphinx according to Greek legend?</td>
        <td>Oedipus</td>
    </tr>
    <tr>
        <td>Who designed the “Ottoman Cannon”?</td>
        <td>Urban</td>
    </tr>
    <tr>
        <td>Who designed the current World Cup trophy?</td>
        <td>Silvio Gazzaniga</td>
    </tr>
    <tr>
        <td>Who designed the Djoser, the first ancient pyramid featuring a stepped design?</td>
        <td>Imhotep</td>
    </tr>
    <tr>
        <td>Who did Boudica lead the tribes of Britain in a revolt against?</td>
        <td>The Holy Roman Empire</td>
    </tr>
    <tr>
        <td>Who did France play in the quarterfinals of the 2006 World Cup in Germany?</td>
        <td>Brazil</td>
    </tr>
    <tr>
        <td>Who first implemented the Julian Calendar in Rome?</td>
        <td>Julius Caesar</td>
    </tr>
    <tr>
        <td>Who formed the First Triumvirate with Caesar and Pompey?</td>
        <td>Crassus</td>
    </tr>
    <tr>
        <td>Who founded the “Malacca Sultanate”?</td>
        <td>Parameswara</td>
    </tr>
    <tr>
        <td>Who founded the “Spanish Armada”?</td>
        <td>Philip II</td>
    </tr>
    <tr>
        <td>Who founded the First Persian Empire?</td>
        <td>Cyrus the Great</td>
    </tr>
    <tr>
        <td>Who founded the Joseon Dynasty?</td>
        <td>Yi Seong-Gye</td>
    </tr>
    <tr>
        <td>Who founded the Kingdom of Asturias?</td>
        <td>Pelagius</td>
    </tr>
    <tr>
        <td>Who gave the famous “I Have a Dream” speech?</td>
        <td>Martin Luther King Jr.</td>
    </tr>
    <tr>
        <td>Who holds the record for most goals scored in a single World Cup, at 13 goals?</td>
        <td>Just Fontaine</td>
    </tr>
    <tr>
        <td>Who initiated the first Salt March?</td>
        <td>Mahatma Gandhi</td>
    </tr>
    <tr>
        <td>Who invented the printing press?</td>
        <td>Johann Gutenberg</td>
    </tr>
    <tr>
        <td>Who invented the world’s first full-sized automobile?</td>
        <td>Nicolas-Joseph Cugnot</td>
    </tr>
    <tr>
        <td>Who is considered the “Father of the Modern Olympic Games”?</td>
        <td>Pierre de Coubertin</td>
    </tr>
    <tr>
        <td>Who is responsible for ushering valiant warriors into Valhalla upon death in Norse mythology?</td>
        <td>Valkyrie</td>
    </tr>
    <tr>
        <td>Who is the “god of death” in ancient Greek mythology?</td>
        <td>Thanatos</td>
    </tr>
    <tr>
        <td>Who is the “God of War” in ancient Greek mythology?</td>
        <td>Ares</td>
    </tr>
    <tr>
        <td>Who is the ‘god of love’ in Norse mythology?</td>
        <td>Freyja</td>
    </tr>
    <tr>
        <td>Who is the God of Sleep in ancient Greek mythology?</td>
        <td>Hypnos</td>
    </tr>
    <tr>
        <td>Who is the god of thunder in Norse mythology?</td>
        <td>Thor</td>
    </tr>
    <tr>
        <td>Who is the goddess of the earth in ancient Greek mythology?</td>
        <td>Gaea</td>
    </tr>
    <tr>
        <td>Who is the King of the Kingdom?</td>
        <td>Leader of the Alliance that conquers the Lost Temple</td>
    </tr>
    <tr>
        <td>Who is the mortal enemy of the commander Saladin?</td>
        <td>Richard I</td>
    </tr>
    <tr>
        <td>Who is the mother of Cupid, the baby-faced god of love in Roman mythology?</td>
        <td>Venus</td>
    </tr>
    <tr>
        <td>Who is the north stand at Old Trafford named after?</td>
        <td>Sir Alex Ferguson</td>
    </tr>
    <tr>
        <td>Who is the oldest player to have played in a World Cup match?</td>
        <td>Essam El Hadary</td>
    </tr>
    <tr>
        <td>Who is the only manager to win the World Cup twice as a manager?</td>
        <td>Vittorio Pozzo</td>
    </tr>
    <tr>
        <td>Who is the only player to have played in three consecutive World Cup Finals?</td>
        <td>Cafu</td>
    </tr>
    <tr>
        <td>Who is the patron and protector of Athens in ancient Greek mythology?</td>
        <td>Athena</td>
    </tr>
    <tr>
        <td>Who is the south stand at Old Trafford named after?</td>
        <td>Sir Bobby Charlton</td>
    </tr>
    <tr>
        <td>Who is the youngest player in the 2022 World Cup?</td>
        <td>Youssoufa Moukoko</td>
    </tr>
    <tr>
        <td>Who is the youngest player to have played in a World Cup match?</td>
        <td>Norman Whiteside</td>
    </tr>
    <tr>
        <td>Who killed Hector and how does Achilles die?</td>
        <td>Achilles; Arrow though his heel</td>
    </tr>
    <tr>
        <td>Who kills Odin, the king of the gods, during Ragnarok in Norse mythology?</td>
        <td>Fenrir</td>
    </tr>
    <tr>
        <td>Who led the Kingdom of Prussia in the Seven Years’ War?</td>
        <td>Frederick II</td>
    </tr>
    <tr>
        <td>Who ordered the production of the civil manuscript popularly known as the Domesday Book?</td>
        <td>William I</td>
    </tr>
    <tr>
        <td>Who owned everything in Ancient Egyptian kingdoms?</td>
        <td>The Pharoah</td>
    </tr>

    <tr>
        <td>Who painted “The Last Supper”?</td>
        <td>Leonardo da Vinci</td>
    </tr>
    <tr>
        <td>Who painted the Mona Lisa?</td>
        <td>Leonardo da Vinci</td>
    </tr>
    <tr>
        <td>Who proposed the philosophical idea of “I think, therefore I am”?</td>
        <td>Descartes</td>
    </tr>
    <tr>
        <td>Who scored the famous goal known as the “hand of God”?</td>
        <td>Diego Maradona</td>
    </tr>
    <tr>
        <td>Who was believed to be the prototype of King of Hearts in poker?</td>
        <td>Charlemagne</td>
    </tr>
    <tr>
        <td>Who was called “Napoleon of crime” in some of the Sherlock Holmes stories?</td>
        <td>Professor Moriarty</td>
    </tr>
    <tr>
        <td>Who was Plato’s teacher?</td>
        <td>Socrates</td>
    </tr>
    <tr>
        <td>Who was the author of the History of the Peloponnesian War?</td>
        <td>Thucydides</td>
    </tr>
    <tr>
        <td>Who was the Ceroli Chieftain that originally requested help from governors?</td>
        <td>Torgny</td>
    </tr>
    <tr>
        <td>Who was the Emperor of the First French Empire?</td>
        <td>Napoleon Bonaparte</td>
    </tr>
    <tr>
        <td>Who was the first astronaut to enter outer space?</td>
        <td>Yuri Gagarin</td>
    </tr>
    <tr>
        <td>Who was the first consul of the Roman Republic?</td>
        <td>Lucius Junius Brutus</td>
    </tr>
    <tr>
        <td>Who was the first Emperor of Rome?</td>
        <td>Gaius Octavius Augustus</td>
    </tr>
    <tr>
        <td>Who was the first human woman created by Hephaestus in Greek mythology?</td>
        <td>Pandora</td>
    </tr>
    <tr>
        <td>Who was the first Italian to receive the “Pritzker Architecture Prize”?</td>
        <td>Aldo Rossi</td>
    </tr>
    <tr>
        <td>Who was the first Japanese author to receive the Nobel Prize in Literature?</td>
        <td>Yasunari Kawabata</td>
    </tr>
    <tr>
        <td>Who was the first man to win the World Cup as both a captain and manager?</td>
        <td>Franz Beckenbauer</td>
    </tr>
    <tr>
        <td>Who was the first of Henry VIII’s children to rule England?</td>
        <td>Edward VI</td>
    </tr>
    <tr>
        <td>Who was the first woman to become a military pilot anywhere in the world?</td>
        <td>Sabiha Gökçen</td>
    </tr>
    <tr>
        <td>Who was the first woman to win a Nobel Prize?</td>
        <td>Marie Curie</td>
    </tr>
    <tr>
        <td>Who was the first World Cup mascot?</td>
        <td>World Cup Willie</td>
    </tr>
    <tr>
        <td>Who was the founder of the Achaemenid Empire?</td>
        <td>Cyrus the Great</td>
    </tr>
    <tr>
        <td>Who was the founder of the Edo Bakufu of Japan?</td>
        <td>Tokugawa Ieyasu</td>
    </tr>
    <tr>
        <td>Who was the founder of the Mongolian Empire?</td>
        <td>Genghis Khan</td>
    </tr>
    <tr>
        <td>Who was the last Emperor of All Russia?</td>
        <td>Nicholas II</td>
    </tr>
    <tr>
        <td>Who was the last emperor of the Holy Roman Empire?</td>
        <td>Francis II</td>
    </tr>
    <tr>
        <td>Who was the last female Paraoh of the Ptolemy Dynasty?</td>
        <td>Cleopatra VII</td>
    </tr>
    <tr>
        <td>Who was the leader of the Western Army during the Battle of Sekigahara, which took place during Japan’s
            Sengoku
            period?
        </td>
        <td>Ishida Mitsunari</td>
    </tr>
    <tr>
        <td>Who was the manager that led the German team to the title in the 2014 Brazil World Cup?</td>
        <td>Joachim Low</td>
    </tr>
    <tr>
        <td>Who was the manager that led the West German team to the title in the 1990 World Cup?</td>
        <td>Franz Beckenbauer</td>
    </tr>
    <tr>
        <td>Who was the only King of England to be executed?</td>
        <td>Charles I</td>
    </tr>
    <tr>
        <td>Who was the only US President that served more than 2 terms?</td>
        <td>Franklin D. Roosevelt</td>
    </tr>
    <tr>
        <td>Who was the top goalscorer at the 2002 World Cup held in Korea and Japan?</td>
        <td>Ronaldo</td>
    </tr>
    <tr>
        <td>Who was the victor in the Battle of Tours in 732 CE?</td>
        <td>Charles Martel</td>
    </tr>
    <tr>
        <td>Who were the first mountaineer(s) on record to reach the summit of Mt. Everest?</td>
        <td>Tenzing Norgay and Edmund Hilary</td>
    </tr>
    <tr>
        <td>Who won the 1998 FIFA World Cup, held in France?</td>
        <td>France</td>
    </tr>
    <tr>
        <td>Who won the 2019 Women’s World Cup, held in France?</td>
        <td>The United States</td>
    </tr>
    <tr>
        <td>Who won the best young player award at the 2018 World Cup?</td>
        <td>Kylian Mbappé</td>
    </tr>
    <tr>
        <td>Who won the first World Cup?</td>
        <td>Uruguay</td>
    </tr>
    <tr>
        <td>Who won the Golden Ball award in the 2018 World Cup, held in Russia?</td>
        <td>Luka Modric</td>
    </tr>
    <tr>
        <td>Who wrote “Human, All Too Human”?</td>
        <td>Nietzsche</td>
    </tr>
    <tr>
        <td>Who wrote “Les Miserables”?</td>
        <td>Victor Hugo</td>
    </tr>
    <tr>
        <td>Who wrote “The Aeneid”, the first epic poem in Western literature?</td>
        <td>Virgil</td>
    </tr>
    <tr>
        <td>Who wrote “The Symposium”?</td>
        <td>Plato</td>
    </tr>
    <tr>
        <td>Who wrote Alice’s Adventures in Wonderland?</td>
        <td>Lewis Carroll</td>
    </tr>
    <tr>
        <td>Who wrote The Art of War?</td>
        <td>Sun Tzu</td>
    </tr>
    <tr>
        <td>Who wrote the opera Carmen?</td>
        <td>Georges Bizet</td>
    </tr>
    <tr>
        <td>Who wrote the well-known book “A Brief History of Time”?</td>
        <td>Stephen Hawking</td>
    </tr>
    <tr>
        <td>Who wrote the well-known book “Sapiens: A Brief History of Humankind”?</td>
        <td>Yuval Noah Harari</td>
    </tr>

    <tr>
        <td>Who wrote the well-known science fiction series “Dune”?</td>
        <td>Frank Herbert</td>
    </tr>
    <tr>
        <td>Whose autobiography “Living to Tell the Tale” won them the Nobel Prize in Literature?</td>
        <td>Gabriel García Márquez</td>
    </tr>
    <tr>
        <td>Whose beauty is said to have been the cause of the Trojan War?</td>
        <td>Helen</td>
    </tr>
    <tr>
        <td>Whose poem The Waste Land won them the Nobel Prize in Literature?</td>
        <td>T.S. Eliot</td>
    </tr>
    <tr>
        <td>Whose victory was the orchestral work “1812 Overture” composed to commemorate?</td>
        <td>Russian Empire</td>
    </tr>
    <tr>
        <td>Why are advantageous traits more likely to be passed onto offspring?</td>
        <td>Because they are more likely to survive and reproduce</td>
    </tr>
    <tr>
        <td>Why do frogs and other organism produce so many eggs/offspring?</td>
        <td>The more offspring produced the more likely they will survive</td>
    </tr>
    <tr>
        <td>Why does drinking coffee make people feel more awake?</td>
        <td>It contains caffeine</td>
    </tr>
    <tr>
        <td>Why was British nurse Florence Nightingale known as “The Lady with the Lamp”?</td>
        <td>She would inspect patients at night</td>
    </tr>
    <tr>
        <td>Why was scurvy once a common problem for sailors?</td>
        <td>A lack of fresh fruit and vegetables on long voyages</td>
    </tr>
    <tr>
        <td>Why was the Dead Sea given this name?</td>
        <td>The high-saline water prevents plants and animals from thriving near it</td>
    </tr>
    <tr>
        <td>Why were the bow and stern of Viking longboats designed to be symmetrical?</td>
        <td>To reverse direction quickly without a turn around</td>
    </tr>
    <tr>
        <td>With whom did Minamoto no Yoshitsune raise an army to fight Taira no Kiyomori?</td>
        <td>Minamoto no Yoritomo</td>
    </tr>
    <tr>
        <td>Without using buffs, how many Action Points can a Governor have?</td>
        <td>1,000</td>
    </tr>
    <tr>
        <td>Work on which of these cathedrals began in Copenhagen in 1877?</td>
        <td>St. Paul’s Church</td>
    </tr>
    <tr>
        <td>You can help your allies build/upgrade buildings, research technologies, and heal troops. But how much does
            each
            help action fill the progress bar?
        </td>
        <td>0.01</td>
    </tr>
    <tr>
        <td>Zhu Geliang had no involvement in which of the following incidents recounted in the Romance of the Three
            Kingdoms?
        </td>
        <td>Yellow Turban Rebellion</td>
    </tr>
    </tbody>
</table>
</body>
</html>

