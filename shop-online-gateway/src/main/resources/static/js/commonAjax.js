/**
 * Ajax excel_service function
 */
var CommonAjax = {
	get : function(data, path, successCallback, errorCallback){
		$.get({
			url: path,
			data: data,
			success : successCallback,
		}).fail(function(data){
			if(data.status == 401)
			{
				window.location.href = "logout";
			}
		}).fail(errorCallback)
	},
	post : function(data, path, successCallback, errorCallback){
		$.post({
			url: path,
			contentType: "application/json",
			data: JSON.stringify(data),
			success : successCallback,
		}).fail(function(data){
			if(data.status == 401)
			{
				window.location.href = "logout";
			}
		}).fail(errorCallback)
	}
}

//Set time de cho Ajax
var timer;
function showAjaxLoading()
{
	$("button").attr("disabled");
	timer && clearTimeout(timer);
	timer = setTimeout(showScreen, 500);
}

function showScreen() {
	$(".fullscreen").show();
}
function hideAjaxLoading()
{
	$("button").removeAttr("disabled");
	clearTimeout(timer);
	$(".fullscreen").hide();
}
$(document).bind("ajaxStart", function(){
	showAjaxLoading()
}).bind("ajaxStop", function(){
	 hideAjaxLoading();
});
