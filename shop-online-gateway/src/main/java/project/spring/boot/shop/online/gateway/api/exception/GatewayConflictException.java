package project.spring.boot.shop.online.gateway.api.exception;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GatewayConflictException extends RuntimeException {

    @Getter
    protected final List<String> fields = new ArrayList<>();

    public GatewayConflictException(String field) {
        super("duplicate field: " + field);
        this.fields.add(field);
    }

    public GatewayConflictException(String field, Exception e) {
        super("duplicate field: " + field, e);
        this.fields.add(field);
    }

    public GatewayConflictException(String[] fields, Exception e) {
        super("duplicate fields: " + Arrays.toString(fields), e);
        this.fields.addAll(Arrays.asList(fields));
    }

}
