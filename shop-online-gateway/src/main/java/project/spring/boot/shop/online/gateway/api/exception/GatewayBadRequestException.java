package project.spring.boot.shop.online.gateway.api.exception;

import lombok.Getter;
import org.assertj.core.util.Lists;

@Getter
public class GatewayBadRequestException extends RuntimeException {

    private Object data;

    public GatewayBadRequestException(Object data) {
        this("bad request", data);
    }

    public GatewayBadRequestException(Object data, Exception e) {
        this("bad request", data, e);
    }

    public GatewayBadRequestException(String message, Object data) {
        super(message + ", data: " + data);
        this.data = data;
    }

    public GatewayBadRequestException(String message, Object data, Exception e) {
        super(message + ", data: " + data, e);
        this.data = data;
    }

    public static GatewayBadRequestException asList(Object data) {
        return new GatewayBadRequestException(Lists.newArrayList(data));
    }

    public static GatewayBadRequestException asList(Object data, Exception e) {
        return new GatewayBadRequestException(Lists.newArrayList(data), e);
    }

}
