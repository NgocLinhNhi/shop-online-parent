package project.spring.boot.shop.online.gateway.api.config;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import project.spring.boot.shop.online.components.customer.CustomerServiceClient;
import project.spring.boot.shop.online.components.customer.CustomerServiceClientLocal;

@Setter
@Configuration
public class GatewayCustomerServiceConfig {

    @Value("${gateway.customer.service.max-capacity}")
    protected int maxCapacity;

    //Tạo Bean cho CustomerServiceClient và khởi tạo CustomerServiceClientLocal implements  CustomerServiceClient = bean too
    @Bean
    public CustomerServiceClient customerServiceClient() {
        return new CustomerServiceClientLocal(maxCapacity);
    }

}
