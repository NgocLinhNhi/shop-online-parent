package project.spring.boot.shop.online.gateway.api.validator;

import org.springframework.stereotype.Component;
import project.spring.boot.shop.online.components.entity.Product;
import project.spring.boot.shop.online.gateway.api.exception.GatewayBadRequestException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductValidator {

    public void validate(Product addProductRequest) {
        List<String> errors = new ArrayList<>();
        validateNumberSales(errors, addProductRequest.getNumberSales());
        validatePrice(errors, addProductRequest.getPrice());
        validateGuarantee(errors, addProductRequest.getGuarantee());

        if (errors.size() > 0) throw new GatewayBadRequestException(errors);
    }

    private void validateNumberSales(List<String> errors, BigDecimal numberSales) {
        if (numberSales == null) errors.add("number_sales is required ");
    }

    private void validatePrice(List<String> errors, BigDecimal price) {
        if (price == null) errors.add("price is required ");
    }

    private void validateGuarantee(List<String> errors, BigDecimal guarantee) {
        if (guarantee == null) errors.add("guarantee is required ");
    }

}
