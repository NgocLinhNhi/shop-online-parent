package project.spring.boot.shop.online.gateway.api.validator;

import org.springframework.stereotype.Component;
import project.spring.boot.shop.online.components.request.CustomerRegisterRequest;
import project.spring.boot.shop.online.utils.util.StringUtil;
import project.spring.boot.shop.online.gateway.api.exception.GatewayBadRequestException;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserValidator {

    public void validate(CustomerRegisterRequest user) {
        List<String> errors = new ArrayList<>();
        validatePhoneNumber(user.getPhone(), errors);
        validateAddress(user.getAddress(), errors);

        if (errors.size() > 0) throw new GatewayBadRequestException(errors);
    }

    private void validatePhoneNumber(String phoneNumber, List<String> errors) {
        if (StringUtil.isEmpty(phoneNumber)) {
            errors.add("Phone Number is required ");
        }
    }

    private void validateAddress(String address, List<String> errors) {
        if (StringUtil.isEmpty(address)) {
            errors.add("address is required ");
        }
    }
}
