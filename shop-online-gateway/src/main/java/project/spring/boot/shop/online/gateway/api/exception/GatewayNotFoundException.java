package project.spring.boot.shop.online.gateway.api.exception;

public class GatewayNotFoundException extends RuntimeException {

    public GatewayNotFoundException(String message) { super(message);}
    public GatewayNotFoundException(String message, Exception e) {
        super(message, e);
    }

}
