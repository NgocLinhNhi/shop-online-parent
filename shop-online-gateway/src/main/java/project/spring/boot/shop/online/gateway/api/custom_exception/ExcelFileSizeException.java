package project.spring.boot.shop.online.gateway.api.custom_exception;

public class ExcelFileSizeException extends RuntimeException {
    public ExcelFileSizeException(String message) {
        super(message);
    }
}
