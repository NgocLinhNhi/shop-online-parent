package project.spring.boot.shop.online.gateway.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import project.spring.boot.shop.online.components.entity.Product;
import project.spring.boot.shop.online.components.exception.ProductNotFoundException;
import project.spring.boot.shop.online.components.services.ProductService;
import project.spring.boot.shop.online.gateway.api.constant.Constant;
import project.spring.boot.shop.online.gateway.api.exception.GatewayNotFoundException;
import project.spring.boot.shop.online.gateway.api.exception.GatewayRestrictedException;
import project.spring.boot.shop.online.gateway.api.message.Response;
import project.spring.boot.shop.online.gateway.api.validator.ProductValidator;

import java.util.List;

@RestController
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    ProductValidator productValidator;

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @GetMapping(value = "/loadAllProduct")
    public ResponseEntity<List<Product>> loadAllProduct() throws Exception {
        List<Product> loadAllProduct = productService.loadAllProduct();
        return new ResponseEntity<>(loadAllProduct, HttpStatus.OK);
    }

    @GetMapping(value = "/loadAllProductJson")
    public String loadAllProductJson() {
        String result;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Product> loadAllProduct = productService.loadAllProduct();
            result = objectMapper.writeValueAsString(loadAllProduct);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
            throw new GatewayRestrictedException();
        }
        return result;
    }

    // sử dụng trong trường hợp update hay view = modal tại trang hiện tại
    @GetMapping(value = "/findProductBySeq/{seqPro}")
    public ResponseEntity<Product> getProductBySeqPro(@PathVariable long seqPro) throws Exception {
        Product proFind;
        try {
            proFind = productService.findProductById(seqPro);
        } catch (ProductNotFoundException e) {
            throw new GatewayNotFoundException("Can not find Product", e);
        }
        return new ResponseEntity<>(proFind, HttpStatus.OK);
    }

    // trường hợp move sang 1 page jsp khác để update hay view
    @GetMapping(value = "/updateProductBySeq/{seqPro}")
    public ModelAndView updateProductBySeqPro(@PathVariable String seqPro, ModelMap model) throws Exception {
        // do truyền = href no hiểu là String nên để ở tham số là String
        Long seq = Long.parseLong(seqPro);
        Product proFind;
        try {
            proFind = productService.findProductById(seq);
        } catch (ProductNotFoundException e) {
            throw new GatewayNotFoundException("Can not find Product", e);
        }
        model.addAttribute("product", proFind);
        return new ModelAndView("pages/updateProduct");
    }

    @PostMapping(value = "/addNewProduct", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Response> addNewProduct(@RequestBody @Validated Product pro) throws Exception {
        productValidator.validate(pro);
        productService.addNewProduct(pro);
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.OK);
    }

    @PostMapping(value = "/deleteProduct/{seqPro}")
    public ResponseEntity<Response> deleteProduct(@PathVariable long seqPro) throws Exception {
        productService.deleteProduct(seqPro);
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.CREATED);
    }

    @PostMapping(value = "/updateProduct")
    public ResponseEntity<Response> updateProduct(@RequestBody Product pro) throws Exception {
        productService.updateProduct(pro);
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.SUCCESS_MESSAGE),
                HttpStatus.OK);
    }
}
