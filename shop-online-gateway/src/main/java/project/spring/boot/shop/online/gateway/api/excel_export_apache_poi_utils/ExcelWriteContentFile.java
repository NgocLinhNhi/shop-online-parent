package project.spring.boot.shop.online.gateway.api.excel_export_apache_poi_utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import project.spring.boot.shop.online.components.mapper.ProductReportObject;

import java.math.BigDecimal;
import java.util.List;

public class ExcelWriteContentFile {

    public static ExcelWriteContentFile getInstance() {
        return new ExcelWriteContentFile();
    }

    public void writeDataLines(List<ProductReportObject> listData,
                               XSSFSheet sheet) {
        int rowCount = 1; // tạo row data đầu tiên

        for (ProductReportObject data : listData) {
            long seqPro = data.getSeqPro();
            String categoryName = data.getCategoryName();
            String productName = data.getProductName();
            BigDecimal price = data.getPrice();
            BigDecimal numberSales = data.getNumberSales();
            BigDecimal guarantee = data.getGuarantee();
            String image = data.getImageProduct();
            String createDate = data.getCreateDate();
            String updateDate = data.getUpdateDate();

            Row row = sheet.createRow(rowCount++);

            int columnCount = 0;
            Cell cell = row.createCell(columnCount++);
            cell.setCellValue(seqPro);

            cell = row.createCell(columnCount++);
            cell.setCellValue(categoryName);

            cell = row.createCell(columnCount++);
            cell.setCellValue(productName);

            cell = row.createCell(columnCount++);
            cell.setCellValue(String.valueOf(price));

            cell = row.createCell(columnCount++);
            cell.setCellValue(String.valueOf(numberSales));

            cell = row.createCell(columnCount++);
            cell.setCellValue(String.valueOf(guarantee));

            cell = row.createCell(columnCount++);
            cell.setCellValue(image);

            cell = row.createCell(columnCount++);
            cell.setCellValue(createDate);

            cell = row.createCell(columnCount++);
            cell.setCellValue(updateDate);
        }
    }

}
