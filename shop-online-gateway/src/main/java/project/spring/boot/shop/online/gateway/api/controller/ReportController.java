package project.spring.boot.shop.online.gateway.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import project.spring.boot.shop.online.components.mapper.ProductReportObject;
import project.spring.boot.shop.online.components.services.ReportService;
import project.spring.boot.shop.online.gateway.api.constant.Constant;
import project.spring.boot.shop.online.gateway.api.excel_export_apache_poi_utils.ExcelExportService;
import project.spring.boot.shop.online.gateway.api.excel_resumeable_service.ExcelProcessHandler;
import project.spring.boot.shop.online.gateway.api.excel_resumeable_service.ExportExcelReportService;
import project.spring.boot.shop.online.utils.util.excel_apache_poi.SimpleExcelImportUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;


@Controller
@RequestMapping(value = "/report")
public class ReportController {

    private static final Logger logger = LoggerFactory.getLogger(ReportController.class);

    @Autowired
    ReportService reportService;

    @Autowired
    ExportExcelReportService exportReportService;

    @Autowired
    private ExcelProcessHandler excelProcessHandler;

    @RequestMapping(value = "/productReport")
    public ModelAndView goToReport() {
        return new ModelAndView("pages/exportReportExcel");
    }

    @RequestMapping(value = "/goToImportExcelByResumeAble")
    public ModelAndView goAddProductFile() {
        return new ModelAndView("pages/importExcelFileByResumeAble");
    }

    @RequestMapping(value = "/goToImportExcelByApachePoi")
    public ModelAndView goImportExcelByApachePoi() {
        return new ModelAndView("pages/importExcelFileByApachePoi");
    }

    //Export file excel = JxlsHelper normal
    @PostMapping(value = "/exportExcelReportByResumeAble")
    public void exportReportProductByResumeAble(HttpServletResponse response) {
        logger.info("Start Handle export file Excel with ResumeAble");
        Date date = new Date();
        try {
            //Tạo Report normal
            List<ProductReportObject> products = reportService.exportProductReport();
            exportReportService.exportReportProduct(response, date, date, products);
            logger.info("End Handle export file Excel with ResumeAble");
        } catch (IOException e) {
            logger.error(Constant.RESPONSE.FILE_NOT_FOUND_ERROR, e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Export detail File excel error when import has error = ResumeAble
    @PostMapping(value = "/exportExcelFileErrorByResumeAble")
    public void exportDetailFileErrorByResumeAble(HttpServletResponse response) {
        try {
            logger.info("Start Handle export file detail Error with ResumeAble");
            exportReportService.exportFileImportError(response, excelProcessHandler.listProductErrorResult);
            logger.info("End Handle export file detail Error with ResumeAble");
        } catch (IOException e) {
            logger.error(Constant.RESPONSE.FILE_NOT_FOUND_ERROR, e);
        }
    }

    //Export File excel from Database = Apache poi
    @PostMapping(value = "/exportExcelReportByApachePoi")
    public void exportExcelReportByApachePoi(HttpServletResponse response) {
        try {
            logger.info("Start Handle export file Excel with apache poi");
            List<ProductReportObject> products = reportService.exportProductReport();
            ExcelExportService excelService = new ExcelExportService();
            excelService.exportExcelFile(products, response);
            logger.info("End Handle export file Excel with apache poi");
        } catch (Exception e) {
            logger.error(Constant.RESPONSE.INTERNAL_SERVER_ERROR_CODE + "", e);
        }
    }

    //Export detail File excel error when import has error = Apache poi
    @PostMapping(value = "/exportFileResultImport", produces = "application/json")
    public void exportDetailFileErrorByApachePoi(HttpServletResponse response, @RequestBody String fileErrorName) {
        try {
            logger.info("Start Handle export file detail Error with apache poi");
            SimpleExcelImportUtils.writeFileResponse(response, decodeFilePath(fileErrorName));
            logger.info("End Handle export file detail Error with apache poi");
        } catch (IOException e) {
            logger.error(Constant.RESPONSE.FILE_NOT_FOUND_ERROR, e);
        }
    }

    private String decodeFilePath(String fileErrorName) throws UnsupportedEncodingException {
        String decode = URLDecoder.decode(fileErrorName, "UTF-8");
        return decode.split("=")[1];
    }
}
