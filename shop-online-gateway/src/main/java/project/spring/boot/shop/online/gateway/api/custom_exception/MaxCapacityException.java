package project.spring.boot.shop.online.gateway.api.custom_exception;

public class MaxCapacityException extends RuntimeException {

    public MaxCapacityException(String msg) {
        super(msg);
    }

}
