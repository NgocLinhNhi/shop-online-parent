package project.spring.boot.shop.online.gateway.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import project.spring.boot.shop.online.components.view.ProductFileExcel;
import project.spring.boot.shop.online.gateway.api.constant.Constant;
import project.spring.boot.shop.online.gateway.api.custom_exception.ExcelFileSizeException;
import project.spring.boot.shop.online.gateway.api.custom_exception.ExcelValidateCellException;
import project.spring.boot.shop.online.gateway.api.excel_import_apache_poi_utils.ExcelImportService;
import project.spring.boot.shop.online.gateway.api.excel_resumeable_service.ExcelProcessHandler;
import project.spring.boot.shop.online.gateway.api.exception.GatewayBadRequestException;
import project.spring.boot.shop.online.gateway.api.exception.GatewayPayloadTooLargeException;
import project.spring.boot.shop.online.gateway.api.message.Response;
import project.spring.boot.shop.online.utils.util.StringUtil;
import project.spring.boot.shop.online.utils.util.excel_resumeable.ResumeAbleSimpleWriter;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;

@Controller
@RequestMapping(value = "/report")
public class FileUploadController {
    private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    @Autowired
    private ExcelProcessHandler excelProcessHandler;

    @Autowired
    private ResumeAbleSimpleWriter resumeAbleSimpleWriter;

    @Autowired
    private ExcelImportService excelImportService;

    @PostMapping("/importProductByFileExcel")
    public ResponseEntity<String> uploadExcelByResumeAble(HttpServletRequest request) throws Exception {
        logger.info("Start handle import file excel with ResumeAble");
        StringBuilder message = new StringBuilder();
        try {
            File file = resumeAbleSimpleWriter.resumeAbleUpload(request);
            if (file == null) return new ResponseEntity<>(Constant.RESPONSE.FILE_LOADING, HttpStatus.NOT_FOUND);
            List<ProductFileExcel> excelProductResult = excelProcessHandler.processExcelProduct(file);

            logger.info("End handle import file excel with ResumeAble");

            return new ResponseEntity<>(
                    message.append(Constant.RESPONSE.FILE_SUCCESS)
                            .append(excelProductResult.size())
                            .append(Constant.RESPONSE.FILE_TOTAL_DATA).toString(),
                    HttpStatus.OK);

        } catch (ExcelValidateCellException ex) {
            throw new GatewayBadRequestException(ex.getMessage());
        } catch (ExcelFileSizeException et) {
            throw new GatewayPayloadTooLargeException(et.getMessage());
        } catch (Exception e) {
            logger.error("System has error please contact with admin ", e);
            throw new Exception();
        }
    }

    @PostMapping(value = "/importExcelReportByApachePoi")
    public ResponseEntity<Response> importExcelByApachePoi(@RequestParam("file") MultipartFile file) {
        logger.info("Start handle import file excel with apache poi");
        String exportFilePath;
        try {
            InputStream inputStream = new BufferedInputStream(file.getInputStream());
            exportFilePath = excelImportService.handleFileExcelImport(inputStream);

            if (!StringUtil.isEmpty(exportFilePath)) {
                return new ResponseEntity<>(new Response(
                        Constant.RESPONSE.FAILED_STATUS,
                        Constant.RESPONSE.BAD_REQUEST_CODE,
                        Constant.RESPONSE.FILE_EXCEL_VALIDATE_ERROR,
                        exportFilePath),
                        HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new Response(
                    Constant.RESPONSE.FAILED_STATUS,
                    Constant.RESPONSE.INTERNAL_SERVER_ERROR_CODE,
                    Constant.RESPONSE.INTERNAL_SERVER_ERROR),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("End handle import file excel with apache poi");
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.SUCCESS_STATUS,
                Constant.RESPONSE.SUCCESS_CODE,
                Constant.RESPONSE.IMPORT_SUCCESS),
                HttpStatus.OK);
    }
}
