package project.spring.boot.shop.online.gateway.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import project.spring.boot.shop.online.components.customer.CustomerServiceClient;
import project.spring.boot.shop.online.components.entity.User;
import project.spring.boot.shop.online.components.exception.CustomerDuplicateException;
import project.spring.boot.shop.online.components.exception.CustomerNotFoundException;
import project.spring.boot.shop.online.components.request.CustomerLoginRequest;
import project.spring.boot.shop.online.components.request.CustomerRegisterRequest;
import project.spring.boot.shop.online.gateway.api.exception.GatewayConflictException;
import project.spring.boot.shop.online.gateway.api.exception.GatewayRestrictedException;
import project.spring.boot.shop.online.gateway.api.test_performance.TestPerformanceLogin;
import project.spring.boot.shop.online.gateway.api.validator.UserValidator;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private CustomerServiceClient customerServiceClient;

    @Autowired
    UserValidator userValidator;

    @GetMapping(value = "/login")
    public ModelAndView showWelcomePage(ModelMap model,
                                        @RequestParam String username,
                                        @RequestParam String password) throws Exception {
        ModelAndView mv;

        CustomerLoginRequest request = createCustomerLoginRequest(username, password);

        User loginUser;
        try {
            loginUser = customerServiceClient.call(request, User.class);
        } catch (CustomerNotFoundException e) {
            model.put("errorMessage", "Wrong User Name or password!!!");
            mv = new ModelAndView("user/login");
            return mv;
        }

        if (loginUser == null) {
            model.put("errorMessage", "Wrong User Name or password!!!");
            mv = new ModelAndView("user/login");
            return mv;
        }
        logger.info("LOGIN SUCCESS FULL");
        return new ModelAndView("pages/product");
    }

    @GetMapping(value = "/test-performance-login")
    public long testPerformanceLogin(ModelMap model,
                                     @RequestParam String username,
                                     @RequestParam String password) throws Exception {
        long startTime = System.currentTimeMillis();
        TestPerformanceLogin.getInstance(customerServiceClient, username, password).loop();
        long endTime = System.currentTimeMillis();
        long timeResult = endTime - startTime;
        logger.info("elapsed time has been used (ms): " + timeResult);
        System.out.println("elapsed time has been used (ms): " + timeResult);
        //login xử lý 1000x 8 = 8000 request lần chỉ trong 2388 ms = 2.3s
        return timeResult;
    }

    //1 Cách khác trả về kết quả cho API
    //Khác với các API của product đây trả về 1 Object Json cho API
    @PostMapping(value = "/register")
    public Object registerMember(@RequestBody CustomerRegisterRequest request) {
        try {
            userValidator.validate(request);
            //Call BlockingQueue xử lý request theo queue
            return customerServiceClient.call(request, User.class);
        } catch (CustomerDuplicateException e) {
            throw new GatewayConflictException("Login Name :" + request.getLoginName() + " is existed");
        } catch (Exception e) {
            throw new GatewayRestrictedException();
        }
    }

    private CustomerLoginRequest createCustomerLoginRequest(String userName, String password) {
        CustomerLoginRequest request = new CustomerLoginRequest();
        request.setLoginName(userName);
        request.setPassword(password);
        return request;
    }
}
