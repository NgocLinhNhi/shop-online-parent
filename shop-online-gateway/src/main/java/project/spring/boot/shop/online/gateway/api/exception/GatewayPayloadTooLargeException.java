package project.spring.boot.shop.online.gateway.api.exception;

import lombok.Getter;
import org.assertj.core.util.Lists;

@Getter
public class GatewayPayloadTooLargeException extends RuntimeException {

    private Object data;

    public GatewayPayloadTooLargeException(Object data) {
        this("Too Large", data);
    }

    public GatewayPayloadTooLargeException(Object data, Exception e) {
        this("Too Large", data, e);
    }

    public GatewayPayloadTooLargeException(String message, Object data) {
        super(message + ", data: " + data);
        this.data = data;
    }

    public GatewayPayloadTooLargeException(String message, Object data, Exception e) {
        super(message + ", data: " + data, e);
        this.data = data;
    }

    public static GatewayPayloadTooLargeException asList(Object data) {
        return new GatewayPayloadTooLargeException(Lists.newArrayList(data));
    }

    public static GatewayPayloadTooLargeException asList(Object data, Exception e) {
        return new GatewayPayloadTooLargeException(Lists.newArrayList(data), e);
    }

}
