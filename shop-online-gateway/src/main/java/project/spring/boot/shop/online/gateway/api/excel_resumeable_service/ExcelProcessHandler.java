package project.spring.boot.shop.online.gateway.api.excel_resumeable_service;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.jxls.reader.ReaderBuilder;
import org.jxls.reader.XLSReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.spring.boot.shop.online.components.services.ReportService;
import project.spring.boot.shop.online.components.view.ProductFileExcel;
import project.spring.boot.shop.online.gateway.api.custom_exception.ExcelValidateCellException;
import project.spring.boot.shop.online.gateway.api.validator.ExcelValidator;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static project.spring.boot.shop.online.gateway.api.constant.Constant.FILENAME.PRODUCT_EXCEL_IMPORT_TEMPLATE;

@Service
public class ExcelProcessHandler {
    private static Logger logger = LoggerFactory.getLogger(ExcelProcessHandler.class);

    @Autowired
    ReportService reportService;

    @Autowired
    ExcelValidator excelValidator;

    public List<ProductFileExcel> listProductErrorResult;

    public List<ProductFileExcel> processExcelProduct(File file) throws Exception {
        List<ProductFileExcel> productInfos = new ArrayList<>();
        Map<String, Object> beans = new HashMap<>();

        InputStream inputXML = new BufferedInputStream(getClass().getResourceAsStream(PRODUCT_EXCEL_IMPORT_TEMPLATE));
        InputStream inputXLS = new BufferedInputStream(new FileInputStream(file));
        XLSReader mainReader = ReaderBuilder.buildFromXML(inputXML);

        productInfos = putData(beans, productInfos, mainReader, inputXLS);
        clear();
        deleteFileExist(file);
        validateFileExcel(productInfos);
        handleData(productInfos);
        return productInfos;
    }

    @SuppressWarnings("unchecked")
    private List<ProductFileExcel> putData(Map<String, Object> beans,
                                           List<ProductFileExcel> productInfos,
                                           XLSReader mainReader,
                                           InputStream inputXLS) throws IOException, InvalidFormatException {
        beans.put("products", productInfos);
        // put data from file Excel into beans
        mainReader.read(inputXLS, beans);
        logger.info("List Product from file Size : " + productInfos.size());
        return (List<ProductFileExcel>) beans.get("products");
    }

    private void clear() {
        excelValidator.listProductError.clear();
    }

    private void validateFileExcel(List<ProductFileExcel> productInfos) {
        excelValidator.validate(productInfos);
        listProductErrorResult = new ArrayList<>(excelValidator.listProductError);
    }

    private void handleData(List<ProductFileExcel> productInfos) throws Exception {
        if (excelValidator.listProductError.size() == 0) {
            reportService.insertProductReportByBatch(productInfos);
        } else {
            throw new ExcelValidateCellException("Some Error occurred in handling file excel");
        }
    }

    private void deleteFileExist(File file) {
        boolean delete = file.delete();
        if (delete) logger.info("Deleted file successfully on server !!!");
        else logger.info("Deleted file on server has been failed !!!");
    }
}
