package project.spring.boot.shop.online.gateway.api.custom_exception;

public class ExcelValidateCellException extends RuntimeException {
    public ExcelValidateCellException(String message) {
        super(message);
    }
}
