package project.spring.boot.shop.online.gateway.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.spring.boot.shop.online.components.entity.Category;
import project.spring.boot.shop.online.components.services.CategoryServices;

import java.util.List;

@RestController
@RequestMapping("/cate")
public class CategoryController {

    @Autowired
    CategoryServices categoryService;

    @GetMapping(value = "/loadAllCategory")
    public List<Category> loadAllCategory() throws Exception {
        return categoryService.loadAllCategory();
    }
}
