package project.spring.boot.shop.online.gateway.api.test_performance;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.spring.boot.shop.online.components.customer.CustomerServiceClient;
import project.spring.boot.shop.online.components.entity.User;
import project.spring.boot.shop.online.components.request.CustomerLoginRequest;

import java.util.concurrent.Callable;

@Getter
@Setter
public class CustomerLoginCallable implements Callable<String> {

    private static final Logger logger = LoggerFactory.getLogger(CustomerLoginCallable.class);

    private String name;
    private String loginName;
    private String password;
    private CustomerServiceClient customerServiceClient;

    public CustomerLoginCallable(CustomerServiceClient customerServiceClient,
                                 String name,
                                 String loginName,
                                 String password) {
        this.name = name;
        this.loginName = loginName;
        this.password = password;
        this.customerServiceClient = customerServiceClient;
    }

    @Override
    public String call() throws Exception {
        return handleLogin();
    }

    public String handleLogin() {
        CustomerLoginRequest request = createCustomerLoginRequest(loginName, password);
        try {
            customerServiceClient.call(request, User.class);
        } catch (Exception e) {
            logger.error("Exception : " + e);
        }
        return "SUCCESS";
    }

    private CustomerLoginRequest createCustomerLoginRequest(String userName, String password) {
        CustomerLoginRequest request = new CustomerLoginRequest();
        request.setLoginName(userName);
        request.setPassword(password);
        return request;
    }
}
