package project.spring.boot.shop.online.gateway.api.message;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
    private String responseStatus;
    private int responseCode;
    private String message;
    private String filePath;

    public Response(String responseStatus, int responseCode, String message) {
        super();
        this.responseStatus = responseStatus;
        this.responseCode = responseCode;
        this.message = message;
    }

    public Response(String responseStatus, int responseCode, String message, String filePath) {
        super();
        this.responseStatus = responseStatus;
        this.responseCode = responseCode;
        this.message = message;
        this.filePath = filePath;
    }


    public Response() {
        super();
    }

}
