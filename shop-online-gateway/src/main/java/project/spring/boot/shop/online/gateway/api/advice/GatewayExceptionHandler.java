package project.spring.boot.shop.online.gateway.api.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import project.spring.boot.shop.online.gateway.api.constant.Constant;
import project.spring.boot.shop.online.gateway.api.custom_exception.ServerMaintainException;
import project.spring.boot.shop.online.gateway.api.exception.*;
import project.spring.boot.shop.online.gateway.api.message.Response;

import java.util.concurrent.TimeoutException;

@ControllerAdvice
public class GatewayExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GatewayExceptionHandler.class);

    //Nơi xử lý Custom lại các Exception nào trả về error Code nào -> convert lại mã lỗi trả cho API

    //Trả về Object Json thì dùng ResponseEntity => refer project rsbo
//    @ExceptionHandler({GatewayNotFoundException.class})
//    public ResponseEntity handleException(GatewayNotFoundException exception_custom) {
//        logger.info("not found", exception_custom);
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
//    }

    @ExceptionHandler({GatewayBadRequestException.class})
    public ResponseEntity<Response> handleException(GatewayBadRequestException exception) {
        logger.error("bad request", exception);
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.FAILED_STATUS,
                Constant.RESPONSE.BAD_REQUEST_CODE,
                exception.getData().toString()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({GatewayPayloadTooLargeException.class})
    public ResponseEntity<Response> handleException(GatewayPayloadTooLargeException exception) {
        logger.error("Payload too large", exception);
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.FAILED_STATUS,
                Constant.RESPONSE.PAYLOAD_TOO_LARGE,
                exception.getData().toString()),
                HttpStatus.PAYLOAD_TOO_LARGE);
    }


    //@ExceptionHandler Annotation xử lý Exception của Springboot custom lại
    @ExceptionHandler({GatewayNotFoundException.class})
    public ResponseEntity<Response> handleException(GatewayNotFoundException exception) {
        logger.error("not found", exception);
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.FAILED_STATUS,
                Constant.RESPONSE.NOT_FOUND_CODE,
                ""),
                HttpStatus.NOT_FOUND);
    }

    //Trả về HTTP thì dùng ResponseEntity<Response>
    @ExceptionHandler({GatewayConflictException.class})
    public ResponseEntity<Response> handleException(GatewayConflictException exception) {
        logger.error("conflict", exception);
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.FAILED_STATUS,
                Constant.RESPONSE.CONFLICT_CODE,
                exception.getFields().toString()),
                HttpStatus.CONFLICT);
    }

    @ExceptionHandler({GatewayRestrictedException.class})
    public ResponseEntity handleException(GatewayRestrictedException exception) {
        logger.error("restricted", exception);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("restricted");
    }

    //In ra nhiều lỗi ngầm của Program nhò catch @ExceptionHandler của springboot này...
    //Để cái này thì sẽ bắt đc lỗi khó gặm HTTP method not support của import file resumeAble dù success => chưa fix được
    @ExceptionHandler({Exception.class})
    public ResponseEntity<Response> handleException(Exception exception) {
        logger.error("server internal error", exception);
        return new ResponseEntity<>(new Response(
                Constant.RESPONSE.FAILED_STATUS,
                Constant.RESPONSE.INTERNAL_SERVER_ERROR_CODE,
                "Please contact with Admin to check !!!"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ServerMaintainException.class})
    public ResponseEntity handleException(ServerMaintainException exception) {
        logger.error("server maintain", exception);
        return handleException(new GatewayUnavailableException(
                Constant.RESPONSE.SERVER_MAINTAIN, "service maintain"));
    }

    @ExceptionHandler({TimeoutException.class})
    public ResponseEntity handleException(TimeoutException exception) {
        logger.error("service timeout", exception);
        return handleException(new GatewayUnavailableException(
                Constant.RESPONSE.GATEWAY_TIME_OUT, "service timeout"));
    }

}
