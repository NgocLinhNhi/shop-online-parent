package project.spring.boot.shop.online.gateway.api.excel_import_apache_poi_utils;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.spring.boot.shop.online.components.services.ReportService;
import project.spring.boot.shop.online.components.view.ProductFileExcel;
import project.spring.boot.shop.online.utils.util.excel_apache_poi.ExcelParseFormat;
import project.spring.boot.shop.online.utils.util.excel_apache_poi.SimpleExcelImportUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static project.spring.boot.shop.online.gateway.api.constant.Constant.FILENAME.EXCEL_EXPORT_FILE_NAME;
import static project.spring.boot.shop.online.gateway.api.constant.Constant.FILENAME.EXCEL_EXPORT_RESULT_PATH;

@Service
public class ExcelImportService {

    private static final Logger logger = LoggerFactory.getLogger(ExcelImportService.class);

    @Autowired
    ReportService reportService;

    public String handleFileExcelImport(InputStream inputStream) throws Exception {
        logger.info("Start Handle File Excel process ");
        long startTime = System.currentTimeMillis();
        List<ProductFileExcel> listData = new ArrayList<>();
        String exportFilePath;
        Boolean validateResult;

        try (Workbook workbook = WorkbookFactory.create(inputStream)) {
            Iterator<Row> rowIterator = ExcelParseFormat.checkTypeOfWorkBook(workbook);
            validateResult = ExcelImportValidator.getInstance().validateRecords(rowIterator, listData);
            exportFilePath = newSimpleExcelImportUtils().writeFile(validateResult, workbook);
        }

        if (validateResult) {
            reportService.insertProductReportByBatch(listData);
            logger.info("Insert database successfully with size " + listData.size());
        }

        long endTime = System.currentTimeMillis();
        long timeResult = endTime - startTime;
        logger.info("End Handle File Excel process : " + timeResult + "ms (1s=1000ms)");
        return exportFilePath;
    }

    private SimpleExcelImportUtils newSimpleExcelImportUtils() {
        return new SimpleExcelImportUtils(
                EXCEL_EXPORT_RESULT_PATH,
                EXCEL_EXPORT_FILE_NAME);
    }
}
