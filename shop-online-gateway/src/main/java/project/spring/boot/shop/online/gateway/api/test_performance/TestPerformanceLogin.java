package project.spring.boot.shop.online.gateway.api.test_performance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.spring.boot.shop.online.components.customer.CustomerServiceClient;
import project.spring.boot.shop.online.utils.concurrent.NThreadFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class TestPerformanceLogin {

    private static final Logger logger = LoggerFactory.getLogger(TestPerformanceLogin.class);
    private final int threadPoolSize = 8;
    private final CustomerServiceClient customerServiceClient;
    String loginName;
    String password;

    private static TestPerformanceLogin INSTANCE;

    public static TestPerformanceLogin getInstance(CustomerServiceClient customerServiceClient,
                                                   String loginName,
                                                   String password) {
        if (INSTANCE == null) INSTANCE = new TestPerformanceLogin(
                customerServiceClient,
                loginName,
                password);
        return INSTANCE;
    }

    TestPerformanceLogin(CustomerServiceClient customerServiceClient,
                         String loginName,
                         String password) {
        this.customerServiceClient = customerServiceClient;
        this.loginName = loginName;
        this.password = password;
    }

    public void loop() throws Exception {
        //ý nghĩa 1 lần login với 8 thread - lập lại chu trình login 1000 lần
        //ý nghĩa như 1 cái Jmeter code chạy = cơm vậy đó
        for (int i = 0; i < 1000; i++) {
            logger.info("Login count number: " + i);
            start();
        }
    }

    private void start() {
        ExecutorService executorService = newExecutorService();
        for (int i = 0; i < threadPoolSize; ++i) {
            CustomerLoginCallable productCallable = new CustomerLoginCallable(
                    customerServiceClient,
                    "Customer Login number +" + i,
                    loginName,
                    password);
            executorService.submit(productCallable);
        }
    }

    private ExecutorService newExecutorService() {
        ThreadFactory threadFactory = new NThreadFactory("customer-login-test-performance");
        ExecutorService instance = Executors.newFixedThreadPool(threadPoolSize, threadFactory);
        Runtime.getRuntime().addShutdownHook(new Thread(instance::shutdown));
        return instance;
    }

}
