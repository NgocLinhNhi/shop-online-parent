package project.spring.boot.shop.online.gateway.api.constant;

public class Constant {
    public static final String EXCEPTION = "Exception";
    public static final String EMPTY = "";
    public static final String SEND_EMAIL = "Send email has failed!!!";
    public static final String ABSOLUTE_PATH = "/static/report/";

    public static class RESPONSE {
        public static final String SUCCESS_STATUS = "SUCCESS";
        public static final String FAILED_STATUS = "FAILED";

        public static final int SUCCESS_CODE = 200;
        public static final int BAD_REQUEST_CODE = 400;
        public static final int PAYLOAD_TOO_LARGE = 413;
        public static final int NOT_FOUND_CODE = 404;
        public static final int CONFLICT_CODE = 409;
        public static final int GATEWAY_TIME_OUT = 504;
        public static final int SERVER_MAINTAIN = 503;
        public static final int INTERNAL_SERVER_ERROR_CODE = 500;

        public static final String SUCCESS_MESSAGE = "Thực hiện thành công!";
        public static final String IMPORT_SUCCESS = "Import Successfull!";
        public static final String REGISTER_USER_SUCCESS = "Tạo mới tài khoản thành công!";

        //upload file
        public static final String FILE_ERROR = "Đã có lỗi trong quá trình xử lý file, vui lòng tải lại theo đúng mẫu quy định !!!";
        public static final String FILE_EXCEL_VALIDATE_ERROR = "Validate file has error, please download check file error !!!";
        public static final String INTERNAL_SERVER_ERROR = "Some Error has been occurred , please contact with admin !!!";
        public static final String FILE_NOT_FOUND_ERROR = "File not found on serverException";
        public static final String FILE_LOADING = "Đang tải...";
        public static final String FILE_SUCCESS = "Thêm mới thành công ";
        public static final String FILE_TOTAL_DATA = " bản ghi ";

    }

    public static class FILENAME {
        public static final String PRODUCT_EXCEL_IMPORT_TEMPLATE = "/static/report/ImportProductMappingTemplate.xml";
        public static final String PRODUCT_IMPORT_ERROR_RESULT_TEMPLATE = "ImportProductErrorResult.xlsx";
        public static final String PRODUCT_REPORT_EXCEL_TEMPLATE = "ExportProductTemplateExcel.xlsx";

        public static final String PRODUCT_REPORT_EXCEL = "PRODUCT_EXPORT_RESULT_";
        public static final String PRODUCT_IMPORT_ERROR_RESULT = "PRODUCT_IMPORT_ERROR_RESULT_";

        public static final String CONTENT_REPORT = "6 THÁNG CUỐI";

        public static final String EXCEL_EXPORT_RESULT_PATH = "E:/excel-import";
        public static final String EXCEL_EXPORT_FILE_NAME = "export_result_PRODUCT_";
        public static final String SHEET_NAME = "PRODUCT";
        public static final String PRODUCT_HEADER =
                "SEQ_ID," +
                "CATEGORY_NAME," +
                "PRODUCT_NAME," +
                "PRICE,NUMBER_SALES," +
                "GUARANTEE, IMAGE," +
                "CREATE_DATE," +
                "UPDATE_DATE";

    }
}
