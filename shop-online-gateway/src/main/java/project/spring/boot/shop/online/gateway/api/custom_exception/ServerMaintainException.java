package project.spring.boot.shop.online.gateway.api.custom_exception;

import lombok.Getter;

public class ServerMaintainException extends IllegalStateException {

    @Getter
    protected final int responseType;

    public static final int RESPONSE_TYPE_REDIRECT = 1;
    public static final int RESPONSE_TYPE_MESSAGE = 2;

    public ServerMaintainException(int responseType, String msg) {
        super(msg);
        this.responseType = responseType;
    }

}
