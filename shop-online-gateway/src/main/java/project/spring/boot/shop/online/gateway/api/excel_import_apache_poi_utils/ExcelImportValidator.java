package project.spring.boot.shop.online.gateway.api.excel_import_apache_poi_utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.spring.boot.shop.online.components.view.ProductFileExcel;
import project.spring.boot.shop.online.utils.util.excel_apache_poi.ExcelParseFormat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelImportValidator {
    private static Logger logger = LoggerFactory.getLogger(ExcelImportValidator.class);

    public static ExcelImportValidator getInstance() {
        return new ExcelImportValidator();
    }

    Boolean validateRecords(Iterator<Row> rowIter, List<ProductFileExcel> listData) {
        try {
            //pass 1 row - header
            int maxIndex = 0;
            int categoryIdIndex = -1;
            int productNameIndex = -1;

            if (rowIter.hasNext()) {
                Row row = rowIter.next();  // skip the header row
                int index = 0;//read header
                for (Cell cell : row) {
                    CellType cellType = cell.getCellTypeEnum();
                    if (cellType.equals(CellType.STRING)) {
                        String value = cell.getStringCellValue();
                        if (value == null) continue;
                        if ("Category".equalsIgnoreCase(value)) {
                            categoryIdIndex = index;
                        } else if ("ProductName".equalsIgnoreCase(value)) {
                            productNameIndex = index;
                        }
                    }
                    index++;
                }
                //Create cell for report result insert
                Cell cell = row.createCell(index);
                cell.setCellValue("Kết quả");
                maxIndex = index;
            }

            boolean isValidRow = true;
            while (rowIter.hasNext()) {
                ProductFileExcel data = new ProductFileExcel();
                List<String> errors = new ArrayList<>();
                Row row = rowIter.next();

                if (ExcelParseFormat.isRowEmpty(row)) continue;

                isValidRow = validateCategoryId(categoryIdIndex, row, data, errors, isValidRow);
                isValidRow = validateProductName(productNameIndex, row, data, errors, isValidRow);

                createCellErrorResult(row, maxIndex, errors);

                if (isValidRow) listData.add(data);
            }
            return isValidRow;
        } catch (IllegalStateException ex) {
            logger.error("IllegalStateException" + ex);
        }
        return false;
    }

    private void createCellErrorResult(Row row, int maxIndex, List<String> errors) {
        Cell cellResult = row.createCell(maxIndex);
        String error = String.join("", errors);
        cellResult.setCellValue(error);
    }

    private boolean validateCategoryId(int categoryIdIndex,
                                       Row row,
                                       ProductFileExcel data,
                                       List<String> errors,
                                       boolean isValidRow) {
        if (categoryIdIndex >= 0) {
            Cell cell = row.getCell(categoryIdIndex);
            if (cell != null) {
                CellType cellType = cell.getCellTypeEnum();
                if (cellType.equals(CellType.NUMERIC)) {
                    double value = cell.getNumericCellValue();
                    data.setCategory((long) value);
                }
            } else {
                errors.add("Category  is required.");
                isValidRow = false;
            }
        } else {
            errors.add("Thiếu cột Category.");
            isValidRow = false;
        }
        return isValidRow;
    }

    private boolean validateProductName(int productNameIndex,
                                        Row row,
                                        ProductFileExcel data,
                                        List<String> errors,
                                        boolean isValidRow) {
        if (productNameIndex >= 0) {
            Cell cell = row.getCell(productNameIndex);
            if (cell != null) {
                CellType cellType = cell.getCellTypeEnum();
                if (cellType.equals(CellType.STRING)) {
                    String value = cell.getStringCellValue();
                    data.setProductName(value);
                } else {
                    isValidRow = false;
                    errors.add("Product Name phải là chuỗi.");
                }
            }
        } else {
            errors.add("Thiếu cột ProductName.");
            isValidRow = false;
        }
        return isValidRow;
    }

}
