package project.spring.boot.shop.online.gateway.api;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("project.spring.boot.shop.online.components.repository")
//các module phải có đường dẫn package giống nhau thì springBoot mới autowired được
@ComponentScan({
        "project.spring.boot.shop.online.*",
        "project.spring.boot.shop.online.components.config"
})
@EntityScan("project.spring.boot.shop.online.components.entity")
@SpringBootApplication()
public class GatewayApiStartUp extends SpringBootServletInitializer {

    public static void main(String[] args) throws Exception {
        GatewayBootstrap bootstrap = new GatewayBootstrap();
        bootstrap.start(GatewayApiStartUp.class);
    }

}
