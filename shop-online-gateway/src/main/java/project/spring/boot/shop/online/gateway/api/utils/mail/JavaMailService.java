package project.spring.boot.shop.online.gateway.api.utils.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.spring.boot.shop.online.gateway.api.constant.Constant;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

public class JavaMailService {
    private static final Logger logger = LoggerFactory.getLogger(JavaMailService.class);

    public static String sendEmail(String subject,
                                   String content,
                                   String mailTo) throws IOException {
        Properties prop = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        prop.load(classLoader.getResourceAsStream("mail_information.properties"));

        String doSendMail = doSendMail(
                prop.getProperty("account"),
                prop.getProperty("password"),
                mailTo, subject,
                content,
                prop.getProperty("host"),
                prop.getProperty("port"),
                prop.getProperty("timeOut"));

        return doSendMail.equals(Constant.EMPTY) ? Constant.EMPTY : doSendMail;
    }


    private static String doSendMail(String account, String password, String mailTo, String subject, String content,
                                     String host, String port, String string) {

        String result = Constant.EMPTY;

        Properties props = new Properties();
        props.put("mail.smtp.ssl.trust", host);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.ssl.enable", true);// start both ssl :587
        props.put("mail.smtp.starttls.enable", true); // start tls : 465 - dùng thằng nào thì enable 1 thằng thôi
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.timeout", string);
        props.put("mail.smtp.connectiontimeout", string);

        try {
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(account, password);
                }
            });
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(account));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo));
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);
        } catch (Exception e) {
            logger.error(Constant.EXCEPTION, e);
            result = e.toString();
        }
        return result;
    }

    // public static void main(String[] args) throws IOException {
    // JavaMailService.sendEmailJson("1", "1", "mrlytieudao@gmail.com");
    // }

}
